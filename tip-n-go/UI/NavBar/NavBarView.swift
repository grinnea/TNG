//
//  NavBarView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

import UIKit

@IBDesignable
class NavBarView: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var oraganizationView: UIView!
    @IBOutlet weak var nameOrganization: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var bellImage: UIImageView!
    
    var organizations = UserOrganizations()
    var way = true
    let baseVC = BaseVC()
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    // MARK: - Lifecycle & Init
    
    private func commonInit() {
        let bundle = Bundle(for: NavBarView.self)
        bundle.loadNibNamed("NavBarView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.setNeedsLayout()
        let userData = baseVC.getUserCurrent()

        if userData?.userOrganizations?.count == 0 || UIApplication.topViewController() is ProfileVC {
            userNameView.isHidden = false
            oraganizationView.isHidden = true
            reNewView()
        }else{
            userNameView.isHidden = true
            oraganizationView.isHidden = false
        }
        
        userNameLabel.text = (userData?.lastName ?? "") + " " + (userData?.firstName ?? "")
        userNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        
        let swipeToLeft = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        let swipeToRight = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        swipeToLeft.direction = .right
        swipeToRight.direction = .left
        contentView.addGestureRecognizer(swipeToLeft)
        contentView.addGestureRecognizer(swipeToRight)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reNewView), name: NSNotification.Name(rawValue:  "notificationCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reNewName), name: NSNotification.Name(rawValue:  "changeUserSettings"), object: nil)
    }
    
    @objc func reNewView() {
        bellImage.image = UserDefaults.notiEmpty ? R.image.bell() : R.image.bellRed()
    }
    
    @objc func reNewName() {
        let userData = baseVC.getUserCurrent()
        userNameLabel.text = (userData?.lastName ?? "") + " " + (userData?.firstName ?? "")
    }
    
    func preparePageControl() {
        organizations = (baseVC.getUserCurrent()?.userOrganizations)!.sorted(by: { $0.organization?.name == $1.organization?.name })
        pageControl.numberOfPages = organizations.count
        pageControl.isHidden = pageControl.numberOfPages == 1
        if UserDefaults.clientDefaultOrganizationHash == nil {
            pageControl.currentPage = 0
        }else{
            let index = organizations.firstIndex(where: {$0.organization?.hash == UserDefaults.clientDefaultOrganizationHash})
            pageControl.currentPage = index ?? 0
        }
        prepareButtonVC()
    }
    
    func prepareButtonVC() {
        if baseVC.getUserCurrent()?.userOrganizations?.count == 0 {
            nameOrganization.text = "qrcode_self_employed".localized()
        }else{
            nameOrganization.text = organizations[pageControl.currentPage].organization?.name
            UserDefaults.clientDefaultOrganizationHash = organizations[pageControl.currentPage].organization?.hash
        }
    }
    
    @objc func changePageOnSwipe(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .left && pageControl.currentPage < (organizations.count - 1) {
            pageControl.currentPage += 1
            way = false
            self.refreshSwipe()
        } else if gesture.direction == .right && pageControl.currentPage > 0{
            pageControl.currentPage -= 1
            way = true
            self.refreshSwipe()
        }
        
    }
    
    func refreshSwipe() {
        DispatchQueue.main.async {
            for view in self.nameOrganization.subviews {
                view.removeFromSuperview()
            }
            self.prepareButtonVC()
            self.transition(vc: self.nameOrganization)
            NotificationCenter.default.post(name: NSNotification.Name("organizationHash"), object: nil)
        }
    }
    
    func transition(vc: UIView) {
        self.transitionView(vc: vc, duration: 0.3, type: way ? .fromLeft : .fromRight)
    }
    
    func handleNotificationsResponse(success: Bool, response: Data?, error: Error?) {
        if success, let response = response {
            do {
                let notifications = try JSONDecoder().decode(NotificationMessages.self, from: response)
                if notifications.contains(where: {$0.isRead == false}) {
                    bellImage.image = R.image.bellRed()
                } else {
                    bellImage.image = R.image.bell()
                }
            } catch let error {
                baseVC.catchError(error: error)
            }
        } else if let response = response {
            baseVC.handleErrorResponse(data: response)
        }
    }
    
    @IBAction func openQRCode(_ sender: Any) {
        let vcQRCode = R.storyboard.main.qrCodeVC()!
        vcQRCode.userCurrent = baseVC.getUserCurrent()!
        if let vc = self.next(ofType: UIViewController.self) {
            vc.present(vcQRCode, animated: true, completion: nil)
        }
    }
    
    @IBAction func openNotifications(_ sender: Any) {
        let vcNotifications = R.storyboard.main.notificationsVC()!
        vcNotifications.userCurrent = baseVC.getUserCurrent()!
        vcNotifications.modalPresentationStyle = .fullScreen
        if let vc = self.next(ofType: UIViewController.self) {
            vc.present(vcNotifications, animated: true, completion: nil)
        }
    }
    
}

extension UIResponder {
    func next<T:UIResponder>(ofType: T.Type) -> T? {
        let r = self.next
        if let r = r as? T ?? r?.next(ofType: T.self) {
            return r
        } else {
            return nil
        }
    }
}
