//
//  UserSettingsView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 06.04.2022.
//

import UIKit
import SDWebImage
import TextFieldEffects

@IBDesignable
final class UserSettingsView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var changePhotoButton: UILabel!
    @IBOutlet weak var changeCard: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cardTableView: UITableView!
    
    @IBOutlet weak var lastNameView: TextFieldCustom!
    @IBOutlet weak var firstNameView: TextFieldCustom!
    @IBOutlet weak var phoneView: TextFieldCustom!
    @IBOutlet weak var emailView: TextFieldCustom!
    
    @IBOutlet weak var myCardTitle: UILabel!
    
    var delegate: UserSettingsVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        let user = delegate?.getUser()
        if profileImage.image == nil{
            profileImage.sd_setImage(with: URL(string: user?.avatar ?? ""), placeholderImage: R.image.profile())
        }
        lastNameView.delegate = self
        firstNameView.delegate = self
        phoneView.delegate = self
        emailView.delegate = self
        
        title.text = "profile_settings_and_notifications".localized()
        changePhotoButton.text = "profile_change_photo".localized()
        changeCard.setTitle("profile_add_card".localized(), for: .normal)
        saveButton.setTitle("save".localized(), for: .normal)
        myCardTitle.text = "profile_my_cards".localized()
        
        lastNameView.label.text = "employee_last_name".localized()
        lastNameView.textField.text = user?.lastName
        lastNameView.inputIsNotEmpty(empty: true)
        firstNameView.label.text = "employee_first_name".localized()
        firstNameView.textField.text = user?.firstName
        firstNameView.inputIsNotEmpty(empty: true)
        phoneView.label.text = "phone".localized()
        phoneView.phoneTextField.text = user?.phone
        phoneView.preparePhone(on: true)
        phoneView.phoneTextField.isEnabled = false
        phoneView.alpha = 0.3
        emailView.label.text = "email".localized()
        emailView.textField.text = user?.email
        emailView.inputIsNotEmpty(empty: true)
        emailView.textField.isEnabled = false
        emailView.alpha = 0.3
        
        checkTextFieldsIsNotEmpty()
    }
    
    func prepareTable() {
        cardTableView.dataSource = self
        cardTableView.delegate = self
        cardTableView?.register(R.nib.bankCardCell)
        cardTableView.tableFooterView = UIView()
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func changePhotoAction(_ sender: Any) {
        delegate?.changeUserPictureAction()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        delegate?.saveAction(phone: phoneView.phoneTextField.text ?? "", email: emailView.textField.text ?? "",
                             lastName: lastNameView.textField.text ?? "", firstName: firstNameView.textField.text ?? "")
    }
    
    @IBAction func changeCard(_ sender: Any) {
        delegate?.openPayments()
    }
}

// MARK: - TextFieldCustom

extension UserSettingsView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let lastName = self.lastNameView.textField.text, !lastName.isEmpty,
            let firstName = self.firstNameView.textField.text, !firstName.isEmpty,
            let email = self.emailView.textField.text,
            let phone = self.phoneView.phoneTextField.text,
            (!phone.isEmpty || !email.isEmpty)
        else
        {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.3
            return
        }
        self.saveButton.isEnabled = true
        self.saveButton.alpha = 1
    }
}

// MARK: - Table view data source

extension UserSettingsView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getCardsList()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.bankCardCell, for: indexPath)!
        let row = delegate?.getCardsList()?[indexPath.row]
        cell.cardMask.text = row?.cardNumberMasked ?? ""
        cell.cardDate.text = "\(row?.expMonth ?? 0)/\(row?.expYear ?? 0)"
        
        shadowView(addView: cell.borderView)
        return cell
    }
}
