//
//  UserSettingsVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 06.04.2022.
//

protocol UserSettingsVCDelegate {
    func backAction()
    func changeUserPictureAction()
    func openPayments()
    func getUser() -> UserCurrent?
    func saveAction(phone: String, email: String, lastName: String, firstName: String)
    func getCardsList() -> UserBankCards?
}
