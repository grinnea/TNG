//
//  UserSettingsVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 06.04.2022.
//

import UIKit
import PKHUD
import ecommpaySDK

class UserSettingsVC: BaseVC, UserSettingsVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: UserSettingsView!
    
    let imagePicker = UIImagePickerController()
    var ecommpayConfig = EcommpayConfig()
    var ecompaySDK = EcommpaySDK()
    var bankCards = UserBankCards()
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        root.prepareView()
        root.prepareTable()
        self.hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Api.financeBankCard(self.handleGetFinanceBankCard(success:response:error:))
    }

    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func saveAction(phone: String, email: String, lastName: String, firstName: String) {
        HUD.show(.progress)
        Api.userUpdateUser(lastName: lastName, firstName: firstName, phone: phone, email: email, self.handleUpdateUser(success:response:error:))
    }
    
    func getUser() -> UserCurrent? {
        return getUserCurrent()
    }
    
    func getCardsList() -> UserBankCards? {
        return bankCards
    }
    
    func changeUserPictureAction() {
        let actionSheet = UIAlertController(
            title: "profile_choose_how_to_update_photo".localized(),
            message: nil,
            preferredStyle: .actionSheet
        )
        let takePhotoAction = UIAlertAction(
            title: "profile_take_photo".localized(),
            style: .default,
            handler: { [weak self] _ in
                self?.presentCameraVC()
            })
        let chooseImageAction = UIAlertAction(
            title: "profile_choose_photo".localized(),
            style: .default,
            handler: { [weak self] _ in
                self?.presentImagePickerVC()
            })
        let cancelAction = UIAlertAction(
            title: "cancel".localized(),
            style: .cancel,
            handler: nil
        )
        
        actionSheet.addAction(takePhotoAction)
        actionSheet.addAction(chooseImageAction)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc private func presentImagePickerVC() {
        let currentVC = UIApplication.topViewController()
        
        imagePicker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = .photoLibrary
       
        currentVC?.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc private func presentCameraVC() {
        let currentVC = UIApplication.topViewController()
        
        imagePicker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            
            currentVC?.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(
                title: "entrance_no_camera_title".localized(),
                message: "entrance_no_camera_message".localized(),
                preferredStyle: .alert
            )
            alert.addAction(
                UIAlertAction(
                    title: "entrance_no_camera_proceed".localized(),
                    style: .destructive,
                    handler: nil
                )
            )
            currentVC?.present(alert, animated: true, completion: nil)
        }
    }
    
    func openPayments() {
        ecommpayConfig = EcommpayConfig()
        ecompaySDK = EcommpaySDK()
        Api.ecommpayGetEcommpayConfigJson(language: Locale.current.languageCode!, self.handleEcommpayConfig(success:response:error:))
    }
    
    func getPaymentInfoAllParams() -> PaymentInfo {
        return PaymentInfo(projectID: ecommpayConfig.project_id ?? 0,
                           paymentID: ecommpayConfig.payment_id ?? "",
                           paymentAmount: ecommpayConfig.payment_amount ?? 0,
                           paymentCurrency: ecommpayConfig.payment_currency ?? "",
                           paymentDescription: ecommpayConfig.payment_description ?? "",
                           customerID: ecommpayConfig.customer_id ?? "",
                           regionCode: ecommpayConfig.region_code ?? "")
    }
    
    func setAvatar(image: UIImage) {
        DispatchQueue.main.async {
            HUD.show(.progress)
            Api.clientsSetAvatar(picture: image, self.handlePhotoUpload(success:response:))
        }
    }
    
    // MARK: - Handle
    
    private func handlePhotoUpload(success: Bool, response: Data?) {
        if success {
            DispatchQueue.main.async {
                let toast = PeopleToastPanel()
                toast.title = "profile_avatar_updated".localized()
                Api.userGetCurrentUser(self.handleRenewClientCurrent(success:response:error:))
                toast.show()
            }
        } else if !success, let response = response {
            self.handleErrorResponse(data: response)
        }
        HUD.hide()
    }
   
    func handleUpdateUser(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            UserDefaults.currentUser = data
            NotificationCenter.default.post(name: NSNotification.Name("changeUserSettings"), object: nil)
            HUD.flash(.success, delay: 1.0)
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    
    // MARK: - Handle
    
    func handleEcommpayConfig(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                ecommpayConfig = try JSONDecoder().decode(EcommpayConfig.self, from: data)
                let paymentInfo = getPaymentInfoAllParams()
                paymentInfo.setSignature(value: ecommpayConfig.signature ?? "")
                paymentInfo.setAction(action: .Verify)
                ecompaySDK.presentPayment(at: self, paymentInfo: paymentInfo) { (result) in
                    print("ecommpaySDK finished with status \(result.status.rawValue)")
                    if let error = result.error { // if error occurred
                        print("Error: \(error.localizedDescription)")
                    }
                    if let token = result.token { // if tokenize action
                        print("Token: \(token)")
                    }
                }
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetFinanceBankCard(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    bankCards = try jsonDecoder.decode(UserBankCards.self, from: data)
                    root.cardTableView.reloadData()
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleRenewClientCurrent(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            UserDefaults.currentUser = data
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}

// MARK: - Image Picker & Camera Delegate

extension UserSettingsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.editedImage] as? UIImage{
            self.setAvatar(image: image)
            root.profileImage.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
