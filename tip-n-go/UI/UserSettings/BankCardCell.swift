//
//  BankCardCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

import UIKit

class BankCardCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var cardMask: UILabel!
    @IBOutlet weak var cardDate: UILabel!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
