//
//  OrganizationListVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

protocol OrganizationsListVCDelegate {
    func backAction()
    func addOrganizationAction()
    func openSelectOrganization()
    func getOrganizations() -> UserOrganizations?
}
