//
//  OrganizationsCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class OrganizationsCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameOrganization: UILabel!
    @IBOutlet weak var brandOrganization: UILabel!
    @IBOutlet weak var imageOrganization: UIImageView!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var rateLabel: UILabel!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

}
