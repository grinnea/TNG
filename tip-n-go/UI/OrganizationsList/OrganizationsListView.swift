//
//  OrganizationsListView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

import UIKit

@IBDesignable
final class OrganizationsListView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addOrganization: UIButton!
    @IBOutlet weak var organizationsTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    @IBOutlet weak var emptyAddOrganization: UIButton!
    
    var delegate: OrganizationsListVCDelegate?
    
    // MARK: - Methods
    
    func prepareTable() {
        organizationsTableView.dataSource = self
        organizationsTableView.delegate = self
        organizationsTableView?.register(R.nib.organizationsCell)
        organizationsTableView.tableFooterView = UIView()
    }
    
    func prepareView() {
        titleLabel.text = "organizations".localized()
        addOrganization.setTitle("qrcode_organization".localized(), for: .normal)
        emptyTitle.text = "organizations_empty_title".localized()
        emptyText.text = "organizations_empty_text".localized()
        emptyAddOrganization.setTitle("qrcode_organization".localized(), for: .normal)
        shadowView(addView: emptyView)
    }
    
    func prepareEmptyView() {
        if delegate?.getOrganizations()?.count == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
            emptyAddOrganization.isHidden = false
            addOrganization.isHidden = true
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
            emptyAddOrganization.isHidden = true
            addOrganization.isHidden = false
        }
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func addOrganizationAction(_ sender: Any) {
        delegate?.addOrganizationAction()
    }
    
}

// MARK: - Table view data source

extension OrganizationsListView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getOrganizations()?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.organizationsCell, for: indexPath)!
        let row = delegate?.getOrganizations()?[indexPath.row]
        cell.nameOrganization.text = row?.organization?.name
        cell.brandOrganization.text = row?.organization?.brand?.name
        cell.imageOrganization.sd_setImage(with: URL(string: row?.organization?.brand?.logo ?? ""), placeholderImage: R.image.organizations())
        cell.rateLabel.text = row?.organization?.rating ?? 0 == 0 ? "" : "\(Double((row?.organization?.rating) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
        cell.starImage.image = getStarRate(rate: row?.organization?.rating)
        shadowView(addView: cell.borderView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.clientDefaultOrganizationHash = delegate?.getOrganizations()?[indexPath.row].organization?.hash
        delegate?.openSelectOrganization()
    }

}
