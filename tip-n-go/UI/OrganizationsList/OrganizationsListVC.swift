//
//  OrganizationsListVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

import UIKit
import PKHUD

class OrganizationsListVC: BaseVC, OrganizationsListVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: OrganizationsListView!

    var userCurrent = UserCurrent()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareTable()
        root.prepareView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserOrganizations()
    }
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func addOrganizationAction() {
        self.openVC(vc: R.storyboard.main.addOrganizationVC())
    }
    
    func openSelectOrganization() {
        let vc = R.storyboard.main.tabBarVC()
        vc?.modalPresentationStyle = .fullScreen
        if let vc = vc { self.present(vc, animated: true, completion: nil) }
    }
    
    func getUserOrganizations() {
        Api.userGetCurrentUser(self.handleRenewClientCurrent(success:response:error:))
    }
    
    func getOrganizations() -> UserOrganizations? {
        return userCurrent.userOrganizations
    }
    
    func handleRenewClientCurrent(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                UserDefaults.currentUser = data
                self.userCurrent = try jsonDecoder.decode(UserCurrent.self, from: data)
                root.organizationsTableView.reloadData()
                root.prepareEmptyView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
