//
//  LoginView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit

@IBDesignable
final class LoginView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var loginSegmentedControl: UISegmentedControl!
    @IBOutlet private weak var recoveryPasswordButton: UIButton!
    @IBOutlet private weak var loginButton: UIButton!
    @IBOutlet weak var logitTitle: UILabel!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var registration: UIButton!
    
    @IBOutlet weak var downLoginButtonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loginView: TextFieldCustom!
    @IBOutlet weak var passwordView: TextFieldCustom!
    @IBOutlet weak var passwordViewConstraint: NSLayoutConstraint!
    
    var delegate: LoginVCDelegate?
    var statusRegistration = false
    var sendCodeToCheck = false
    
    // MARK: - Methods

    func setTitles() {
        logitTitle.font = UIFont.boldSystemFont(ofSize: 20)
        logitTitle.text = "login".localized()
        prepareButton(sendCode: statusRegistration)
        forgotButton.setTitle("login_forgot_password".localized(), for: .normal)
        registration.setTitle("login_registration".localized(), for: .normal)
        loginSegmentedControl.setTitle("login".localized(), forSegmentAt: 0)
        loginSegmentedControl.setTitle("login_registration".localized(), forSegmentAt: 1)
    }
    
    func prepareButton(sendCode: Bool) {
        sendCodeToCheck = sendCode
        checkTextFieldsIsNotEmpty()
        if !sendCode {
            loginButton.addViewBorder(borderColor: R.color.blue()!.cgColor, borderWith: 1, borderCornerRadius: 4)
            loginButton.setTitle("login".localized(), for: .normal)
            loginButton.setTitleColor(R.color.blue(), for: .normal)
            loginButton.backgroundColor = R.color.blueLite()
        }else{
            passwordView.textField.becomeFirstResponder()
            loginButton.addViewBorder(borderColor: R.color.blue()!.cgColor, borderWith: 0, borderCornerRadius: 4)
            loginButton.setTitle("login_confirm".localized(), for: .normal)
            loginButton.setTitleColor(R.color.white(), for: .normal)
            loginButton.backgroundColor = R.color.blue()
            passwordViewConstraint.constant = 60
            passwordView.isHidden = false
        }
    }
    
    func prepareInput() {
        loginView.delegate = self
        passwordView.delegate = self
        passwordView.textField.text = ""
        if loginSegmentedControl.selectedSegmentIndex == 0 {
            loginView.preparePhone(on: true)
            passwordView.label.text = "login_security_code".localized()
            passwordView.isHidden = true
            passwordViewConstraint.constant = 0
            passwordView.textField.isSecureTextEntry = false
            passwordView.textField.textContentType = .oneTimeCode
            passwordView.textField.keyboardType = .numberPad
        }else{
            loginView.preparePhone(on: false)
            loginView.label.text = "email".localized()
            passwordView.label.text = "login_password".localized()
            passwordView.isHidden = false
            passwordViewConstraint.constant = 60
            passwordView.textField.isSecureTextEntry = true
            passwordView.textField.textContentType = .password
            passwordView.textField.keyboardType = .default
        }
    }
    
    private func getLogin() -> (String?) {
        let _login = loginSegmentedControl.selectedSegmentIndex == 0 ? loginView.phoneTextField.text : loginView.textField.text
        if _login?.count == 0 {
            delegate?.showAlert(
                title: "login_empty_field",
                message: "login_empty_login")
            return nil
        }
        return _login
    }
    
    private func getPassword() -> (String?) {
        let _password = passwordView.textField.text
        if _password?.count == 0 {
            delegate?.showAlert(
                title: "login_empty_field",
                message: "login_empty_password")
            return nil
        }
        return _password
    }
    
    // MARK: - Actions
    
    @IBAction private func loginTapped(_ sender: Any) {
        if loginSegmentedControl.selectedSegmentIndex == 0 {
            if !statusRegistration {
                delegate?.accountCheckPhone(phone: loginView.phoneTextField.text ?? "")
            }else{
                if let phone = getLogin(), let code = getPassword() {
                    delegate?.loginToApp(phone: phone, code: code, email: "", password: "")
                } else {
                    return
                }
            }
        }else{
            if let email = getLogin(), let password = getPassword() {
                delegate?.loginToApp(phone: "", code: "", email: email, password: password)
            } else {
                return
            }
        }
        
    }
    
    @IBAction func segmentAction(_ sender: Any) {
        self.endEditing(true)
        prepareInput()
        prepareButton(sendCode: false)
        statusRegistration = false
    }
    
    @IBAction private func forgotTapped(_ sender: Any) {
        delegate?.openRecoverByEmail()
    }
    
    @IBAction func openWebRegister(_ sender: Any) {
        delegate?.openRegistration()
    }
}

extension LoginView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        let type = loginSegmentedControl.selectedSegmentIndex == 0 ? true : false
        if type {
            if !sendCodeToCheck {
                guard
                    let phone = self.loginView.phoneTextField.text, !phone.isEmpty
                else
                {
                    self.loginButton.isEnabled = false
                    self.loginButton.alpha = 0.3
                    return
                }
                self.loginButton.isEnabled = true
                self.loginButton.alpha = 1
            }else{
                guard
                    let pass = self.passwordView.textField.text, !pass.isEmpty
                else
                {
                    self.loginButton.isEnabled = false
                    self.loginButton.alpha = 0.3
                    return
                }
                self.loginButton.isEnabled = true
                self.loginButton.alpha = 1
            }
        }else{
            guard
                let login = self.loginView.textField.text, !login.isEmpty,
                let pass = self.passwordView.textField.text, !pass.isEmpty
            else
            {
                self.loginButton.isEnabled = false
                self.loginButton.alpha = 0.3
                return
            }
            self.loginButton.isEnabled = true
            self.loginButton.alpha = 1
        }
    }
}
