//
//  LoginDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

protocol LoginVCDelegate {
    func loginToApp(phone: String, code: String, email: String, password: String)
    func accountCheckPhone(phone: String)
    func openRecoverByEmail()
    func openRegistration()
    func showAlert(title: String, message: String)
}
