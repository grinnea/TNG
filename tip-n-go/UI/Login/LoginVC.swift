//
//  LoginViewController.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit
import PKHUD
import Localize_Swift

class LoginVC: BaseVC, LoginVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: LoginView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareInput()
        root.setTitles()

        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Business Logic
    
    func loginToApp(phone: String, code: String, email: String, password: String) {
        HUD.show(.systemActivity)
        Api.getJwtToken(phone: phone, code: code, email: email, password: password, completion: self.handleTokens(success:data:))
        UserDefaults.loginString = phone
        UserDefaults.flushFcmRoutingData()
    }

    // MARK: - Methods
    
    func accountCheckPhone(phone: String) {
        HUD.show(.progress)
        Api.accountCheckPhone(phone: phone, fcmToken: UserDefaults.fcmToken ?? "", self.handleAccountCheckPhone(success:response:error:))
    }
    
    func showAlert(title: String, message: String) {
        self.instantAlertShow(title.localized(), message: message.localized())
    }
    
    func openRecoverByEmail() {
        self.openVC(vc: R.storyboard.main.recoverByEmailVC())
    }
    
    func openRegistration() {
        self.openVC(vc: R.storyboard.main.registrationVC())
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        root.downLoginButtonConstraint.constant = keyboardSize.height + 10
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        root.downLoginButtonConstraint.constant = 30
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Handlers
    
    private func handleTokens(success: Bool, data: Data?) {
        if success, let response = data {
            do {
                let responseObject = try LocalStorage.fetchObject(from: response, to: JwtToken.self)
                UserDefaults.tokensReceivedTime = Date().timeIntervalSince1970
                UserDefaults.jwtAccessToken = responseObject?.accessToken
                UserDefaults.jwtAccessTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject?.accessTokenLifetime ?? 0)
                UserDefaults.jwtRefreshToken = responseObject?.refreshToken
                UserDefaults.jwtRefreshTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject?.refreshTokenLifetime ?? 0)
                Api.registerFcmToken(fcmToken: UserDefaults.fcmToken ?? "", completion: { _, _ in })
                Api.userGetCurrentUser(handleLoginClientCurrent(success:response:error:))
            } catch let error {
                self.instantAlertShow("error".localized(), message:error.localizedDescription)
            }
        } else if !success, let response = data {
            UserDefaults.loginString = ""
            self.handleErrorResponse(data: response)
        }
    }

    func handleAccountCheckPhone(success: Bool, response: Data?, error: Error?) {
        if success {
            self.root.statusRegistration = true
            self.root.prepareButton(sendCode: true)
            HUD.hide()
        } else if !success, let data = response {
            HUD.hide()
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleLoginClientCurrent(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            UserDefaults.currentUser = data
            HUD.hide()
            let vc = R.storyboard.main.tabBarVC()
            vc?.modalPresentationStyle = .fullScreen
            if let vc = vc { self.present(vc, animated: true, completion: nil) }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
