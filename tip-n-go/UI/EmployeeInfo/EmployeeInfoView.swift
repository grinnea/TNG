//
//  EmployeeInfoView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 07.04.2022.
//

import UIKit
import Highcharts

@IBDesignable
final class EmployeeInfoView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userRoleLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var employeeTableView: UITableView!
    @IBOutlet weak var userSegmentedControl: UISegmentedControl!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingStar: UIImageView!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyimage: UIImageView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    @IBOutlet weak var chartTipsView: UIView!
    @IBOutlet weak var chartTipSelectCollection: UICollectionView!
    @IBOutlet weak var chartTipViewBackground: UIView!
    
    @IBOutlet weak var chartReviewsView: UIView!
    @IBOutlet weak var chartReviewSelectCollection: UICollectionView!
    @IBOutlet weak var chartReviewPageControl: UIPageControl!
    @IBOutlet weak var chartReviewViewBackground: UIView!
    
    var delegate: EmployeeInfoVCDelegate?
    var chartViewLine: HIChartView!
    var chartViewPie: HIChartView!
    let listChartPeriod = ["day".localized(), "week".localized(), "month".localized(), "year".localized()]
    
    // MARK: - Methods
    
    func prepareView() {
        shadowView(addView: infoView)
        shadowView(addView: emptyView)
        shadowView(addView: chartTipsView)
        shadowView(addView: chartReviewsView)
        userSegmentedControl.setTitle("tips".localized(), forSegmentAt: 1)
        userSegmentedControl.setTitle("review_reviews".localized(), forSegmentAt: 2)
        let user = delegate?.getUserData()
        let userFullName = (user?.lastName ?? "") + " " + (user?.firstName ?? "")
        titleLabel.text = userFullName
        userNameLabel.text = userFullName
        userRoleLabel.text = user?.userOrganizations?.first?.organizationRole?.name
        userAvatar.sd_setImage(with: URL(string: user?.avatar ?? ""), placeholderImage: R.image.avatarDemo())
        phoneLabel.text = user?.phone
        emailLabel.text = user?.email
        ratingLabel.text = user?.rating ?? 0 == 0 ? "" : "\(Double((user?.rating) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
        ratingStar.image = getStarRate(rate: user?.rating)
    }
    
    func prepareTable() {
        employeeTableView.dataSource = self
        employeeTableView.delegate = self
        employeeTableView?.register(R.nib.profileCell)
        employeeTableView?.register(R.nib.tipsCell)
        employeeTableView?.register(R.nib.reviewCell)
        employeeTableView.tableFooterView = UIView()
    }
    
    func heightHeaderView(height: Int) {
        if let headerView = employeeTableView.tableHeaderView {
            var headerFrame = headerView.frame
            headerFrame.size.height = CGFloat(height)
            headerView.frame = headerFrame
            employeeTableView.tableHeaderView = headerView
        }
    }
    
    func prepareEmptyView() {
        let index = userSegmentedControl.selectedSegmentIndex
        emptyimage.image = index == 2 ? R.image.reviewEmpty() : R.image.tipEmpty()
        emptyTitle.text = index == 2 ? "employee_no_reviews_to_display_yet".localized() : "employee_no_tips_to_display_yet".localized()
        emptyText.text = index == 2 ? "employees_show_qr_code_to_customer_leave_reviews".localized() : "employee_show_qr_code_to_the_customer_leave_tip".localized()
        if (delegate?.getUserTips()?.count == 0 && index == 1)
            || (delegate?.getUserReviews()?.count == 0 && index == 2) {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
        }
    }
    
    func collectionViewSetup(viewCollection: UICollectionView) {
        viewCollection.showsHorizontalScrollIndicator = false
        viewCollection.bounces = false
        viewCollection.register(UINib(nibName: R.nib.periondChartCell.identifier, bundle: nil), forCellWithReuseIdentifier: R.nib.periondChartCell.identifier)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 70, height: 24)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        viewCollection.collectionViewLayout = flowLayout
        viewCollection.dataSource = self
        viewCollection.delegate = self
    }
    
    func preparePageControl() {
        chartReviewPageControl.numberOfPages = delegate?.getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
    }
    
    func prepareChartView() {
        for view in self.chartReviewViewBackground.subviews {
            view.removeFromSuperview()
        }
       prepareChartPie(metric: chartReviewPageControl.currentPage)
    }
    
    func transition(vc: UIView, way: Bool) {
        self.transitionView(vc: vc, duration: 0.3, type: way ? .fromLeft : .fromRight)
    }
    
    func prepareChartLine() {
        chartViewLine = HIChartView(frame: CGRect(x: chartTipViewBackground.bounds.origin.x,
                                              y: chartTipViewBackground.bounds.origin.y + 8,
                                              width: chartTipViewBackground.bounds.size.width - 8,
                                              height: chartTipViewBackground.bounds.size.height))
        let arrColumn = delegate?.getReportsData()?.filter({ $0.type == "column" })
        let currency = delegate?.getUserData()?.currency?.sign ?? ""
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrColumn?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrColumn?.first?.title?.text
        options.title = title
        
        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.category + '</b>: ' + this.point.y?.toFixed(2) + ' ' + '\(currency)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        
        let xAxis = HIXAxis()
        xAxis.categories = arrColumn?.first?.xAxis?.categories
        options.xAxis = [xAxis]
        
        let yAxis = HIYAxis()
        yAxis.title = HITitle()
        yAxis.title.text = arrColumn?.first?.yAxis?.title?.text
        options.yAxis = [yAxis]
        
        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        let series = HIColumn()
        series.data = arrColumn?.first?.series?.first!.dataLine
        series.name = arrColumn?.first?.series?.first?.name
        options.series = [series]
        
        chartViewLine.options = options
        chartTipViewBackground.addSubview(chartViewLine)
    }
    
    func prepareChartPie(metric: Int) {
        chartViewPie = HIChartView(frame: CGRect(x: chartReviewViewBackground.bounds.origin.x,
                                              y: chartReviewViewBackground.bounds.origin.y + 8,
                                              width: chartReviewViewBackground.bounds.size.width - 8,
                                              height: chartReviewViewBackground.bounds.size.height))
        chartViewPie.plugins = ["no-data-to-display"]
        let arrPie = delegate?.getReportsData()?.filter({ $0.type == "pie" })
        
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrPie?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrPie?.first?.title?.text
        options.title = title

        let subtitle = HISubtitle()
        subtitle.text = arrPie?.first?.series?[metric].name
        subtitle.style = HICSSObject()
        subtitle.style.fontSize = "16px"
        options.subtitle = subtitle

        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.name + '</b>: ' + this.point.y?.toFixed() + ' review(s) (' + this.point.percentage?.toFixed() + '%)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting

        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        var arrData = [Any]()
        arrPie?.first?.series?[metric].dataPie?.forEach({
            arrData.append(["name": $0.name ?? "", "y": $0.y ?? 0.0, "color": $0.color ?? ""])
        })
        
        let series = HIPie()
        series.data = arrData
        
        options.series = [series]
        
        chartViewPie.options = options
        chartReviewViewBackground.addSubview(chartViewPie)
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func editAction(_ sender: Any) {
        delegate?.openEditStaff()
    }
    
    @IBAction func infoUserSegment(_ sender: Any) {
        if userSegmentedControl.selectedSegmentIndex == 0 {
            infoView.isHidden = false
            chartTipsView.isHidden = false
            chartReviewsView.isHidden = false
            heightHeaderView(height: 775)
        } else{
            infoView.isHidden = true
            chartTipsView.isHidden = true
            chartReviewsView.isHidden = true
            heightHeaderView(height: 0)
        }
        employeeTableView.reloadData()
        prepareEmptyView()
    }
    
    @IBAction func chartPageACtion(_ sender: Any) {
        prepareChartView()
    }
}

// MARK: - Table view data source

extension EmployeeInfoView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if userSegmentedControl.selectedSegmentIndex == 0 {
            let row = indexPath.row
            if row == 2 {
                return 0 //67
            }else {
                return 92
            }
        } else if userSegmentedControl.selectedSegmentIndex == 1 {
            return 85
        }else{
            return 85
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userSegmentedControl.selectedSegmentIndex == 0 {
            return 3
        } else if userSegmentedControl.selectedSegmentIndex == 1 {
            return delegate?.getUserTips()?.count ?? 0
        }else{
            return delegate?.getUserReviews()?.count ?? 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        if userSegmentedControl.selectedSegmentIndex == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.profileCell, for: indexPath)
            else { return UITableViewCell() }
            if row == 0 {
                cell.menuTitle.text = "employee_employee_qr_code".localized()
                cell.menuInfo.text = "employee_create_a_qr_code_or_view_an_existing_one".localized()
                cell.menuTitle.textColor = .black
                cell.menuIcon.image = R.image.history()
            } else if row == 1 {
                cell.menuTitle.text = "employee_settings_and_notifications".localized()
                cell.menuInfo.text = "employee_profile_settings_and_notifications".localized()
                cell.menuTitle.textColor = .black
                cell.menuIcon.image = R.image.settings()
            } else if row == 2 {
                if delegate?.getUserData()?.hash == BaseVC().getUserCurrent()?.hash {
                    cell.menuTitle.alpha = 0.3
                    cell.menuIcon.alpha = 0.3
                    cell.isUserInteractionEnabled = false
                }else{
                    cell.menuTitle.alpha = 1
                    cell.menuIcon.alpha = 1
                    cell.isUserInteractionEnabled = true
                }
                cell.menuTitle.text = "employee_lay_off_an_employee".localized()
                cell.menuTitle.textColor = .red
                cell.menuInfo.text = ""
                cell.menuIcon.image = R.image.crossRed()
                cell.isHidden = true
            }
            shadowView(addView: cell.viewBackground)
            return cell
        } else if userSegmentedControl.selectedSegmentIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tipsCell, for: indexPath)!
            let row = delegate?.getUserTips()?[indexPath.row]
            cell.labelName.text = ""
            cell.labelDate.text = row?.date?.toFullFormatWithTime()
            cell.labelSum.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
            cell.tipRateLabel.text = "\(Double((row?.tipRate) ?? 0).convertedFromBackendFormat(afterPoint: 1))%"
            
            shadowView(addView: cell.background)
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reviewCell, for: indexPath)
            else { return UITableViewCell() }
            let row = delegate?.getUserReviews()?[indexPath.row]
            cell.labelName.text = ""
            cell.labelDate.text = row?.date?.toFullFormatWithTime()
            cell.labelSum.text = row?.rate ?? 0 == 0 ? "" : "\(Double((row?.rate) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
            cell.starImage.image = getStarRate(rate: row?.rate)
            
            shadowView(addView: cell.background)
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if userSegmentedControl.selectedSegmentIndex == 0 {
            if indexPath.row == 0 {
                delegate?.openQRCode()
            }else if indexPath.row == 1 {
                delegate?.openEditStaff()
            }else if indexPath.row == 2 && delegate?.getUserData()?.hash != BaseVC().getUserCurrent()?.hash {
                
            }
        }else if userSegmentedControl.selectedSegmentIndex == 2 {
            if delegate?.getUserReviews()?[indexPath.row] != nil {
                delegate?.openReviewInfo(review: (delegate?.getUserReviews()?[indexPath.row])!)
            }
        }
    }

}

extension UITableView {

    var autolayoutTableViewHeader: UIView? {
        set {
            self.tableHeaderView = newValue
            guard let header = newValue else { return }
            header.setNeedsLayout()
            header.layoutIfNeeded()
            header.frame.size =
            header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
            self.tableHeaderView = header
        }
        get {
            return self.tableHeaderView
        }
    }
}

// MARK: - Collection view data source

extension EmployeeInfoView: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listChartPeriod.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)!
        cell.title.text = listChartPeriod[indexPath.row]
        if cell.isSelected {
                cell.backgroundColor = R.color.blue()
            }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.getPeriodReport(period: listChartPeriod[indexPath.row], type: collectionView == chartTipSelectCollection)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = false
    }
    
}
