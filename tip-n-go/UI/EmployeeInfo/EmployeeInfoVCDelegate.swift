//
//  EmployeeInfoVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 07.04.2022.
//

protocol EmployeeInfoVCDelegate {
    func backAction()
    func getUserData() -> UserCurrent?
    func getUserTips() -> Tips?
    func getUserReviews() -> Reviews?
    func openReviewInfo(review: Review)
    func openQRCode()
    func openEditStaff()
    func getReportsData() -> ReportsData?
    func getReport(period: String)
    func getPeriodReport(period: String, type: Bool)
}
