//
//  EmployeeInfoVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 07.04.2022.
//

import UIKit
import PKHUD

class EmployeeInfoVC: BaseVC, EmployeeInfoVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: EmployeeInfoView!
    
    var userInfo = UserCurrent()
    var tips = Tips()
    var reviews = Reviews()
    var userHash = ""
    var reportsData = ReportsData()
    var way = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
        root.collectionViewSetup(viewCollection: root.chartTipSelectCollection)
        root.collectionViewSetup(viewCollection: root.chartReviewSelectCollection)
        
        NotificationCenter.default.addObserver(self, selector: #selector(editStaff), name: NSNotification.Name("userInfo"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiOrganizationUsers()
        getReport(period: "month".localized())
        self.root.chartTipSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        self.root.chartReviewSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        let swipeToLeft = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        let swipeToRight = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        swipeToLeft.direction = .right
        swipeToRight.direction = .left
        root.chartReviewsView.addGestureRecognizer(swipeToLeft)
        root.chartReviewsView.addGestureRecognizer(swipeToRight)
    }
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getApiOrganizationUsers() {
        HUD.show(.progress)
        Api.usersByHashAndOrganizationHash(userHash: userHash, organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserByHash(success:response:error:))
        Api.tipsByHashAndOrganizationHash(userHash: userHash, organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserTips(success:response:error:))
        Api.reviewsByHashAndOrganizationHash(userHash: userHash, organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserReviews(success:response:error:))
    }
    
    func getReportsData() -> ReportsData? {
        return reportsData
    }
    
    func getReport(period: String) {
        var charts = [[String:String]]()
        charts.append(["userHash": userInfo.hash ?? "", "organizationHash":  UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserTips"])
        charts.append(["userHash": userInfo.hash ?? "", "organizationHash":  UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserReviews"])
        Api.getReportsData(charts: charts, self.handleGetReportsData(success:response:error:))
    }
    
    @objc func editStaff(notification: NSNotification) {
        userInfo = notification.object as! UserCurrent
        root.prepareView()
    }
    
    func getPeriodReport(period: String, type: Bool) {
        var charts = [[String:String]]()
            if type {
                charts.append(["userHash": userInfo.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentTips"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataTip(success:response:error:))
            }else{
                charts.append(["userHash": userInfo.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentReviews"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataReview(success:response:error:))
            }
        }
    
    @objc func changePageOnSwipe(_ gesture: UISwipeGestureRecognizer) {
        let numPage = getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
        if gesture.direction == .left && root.chartReviewPageControl.currentPage < numPage - 1 {
            root.chartReviewPageControl.currentPage += 1
            way = false
            self.refreshSwipe()
        } else if gesture.direction == .right && root.chartReviewPageControl.currentPage > 0{
            root.chartReviewPageControl.currentPage -= 1
            way = true
            self.refreshSwipe()
        }
    }
    
    func refreshSwipe() {
        DispatchQueue.main.async {
            self.root.prepareChartView()
            self.root.transition(vc: self.root.chartReviewViewBackground, way: self.way)
        }
    }
    
    func getUserData() -> UserCurrent? {
        return userInfo
    }
    
    func getUserTips() -> Tips? {
        return tips
    }
    
    func getUserReviews() -> Reviews? {
        return reviews
    }
    
    func openReviewInfo(review: Review) {
        let vc = R.storyboard.main.reviewInfoVC()
        vc?.review = review
        openVC(vc: vc)
    }
    
    func openEditStaff() {
        let vc = R.storyboard.main.editStaffVC()
        vc?.userCurrent = userInfo
        openVC(vc: vc)
    }
    
    func openQRCode() {
        let vc = R.storyboard.main.qrCodeVC()
        vc?.userCurrent = userInfo
        openVC(vc: vc)
    }
    
    // MARK: - Handle
    
    func handleGetUserTips(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                tips = try jsonDecoder.decode(Tips.self, from: data)
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetUserReviews(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reviews = try jsonDecoder.decode(Reviews.self, from: data)
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetUserByHash(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                userInfo = try jsonDecoder.decode(UserCurrent.self, from: data)
                root.prepareView()
                HUD.hide()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsData(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
                root.prepareChartPie(metric: 0)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataTip(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataReview(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartPie(metric: root.chartReviewPageControl.currentPage)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
