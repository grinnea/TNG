//
//  RecoverByEmailVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 23.03.2022.
//

import UIKit
import PKHUD

class RecoverByEmailVC: BaseVC, RecoverByEmailVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: RecoverByEmailView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.checkTextFieldsIsNotEmpty()
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Methods
    
    func recoverByEmail(email: String) {
        Api.recoverPasswordByPhone(email: email, self.handleRecoverPasswordByPhone(success:response:error:))
    }
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        self.view.frame.origin.y = 0 - keyboardSize.height + 20
    }

    @objc func keyboardWillHide(notification: NSNotification) {
      self.view.frame.origin.y = 0
    }
    
    // MARK: - Handle
    
    func handleRecoverPasswordByPhone(success: Bool, response: Data?, error: Error?) {
        if success {
            self.cancelAction()
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
