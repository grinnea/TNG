//
//  RecoverByEmailView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 23.03.2022.
//

import UIKit

@IBDesignable
final class RecoverByEmailView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var emailView: TextFieldCustom!
    @IBOutlet weak var sendButton: UIButton!
    
    var delegate: RecoverByEmailVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        emailView.delegate = self
        title.text = "recovery_password_recovery".localized()
        emailView.label.text = "email".localized()
        sendButton.setTitle("recovery_send_recovery_link".localized(), for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.cancelAction()
    }
    
    @IBAction func sendLinkButton(_ sender: Any) {
        delegate?.recoverByEmail(email: emailView.textField.text ?? "")
    }
    
}

extension RecoverByEmailView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let email = self.emailView.textField.text, !email.isEmpty
        else
        {
            self.sendButton.isEnabled = false
            self.sendButton.alpha = 0.3
            return
        }
        self.sendButton.isEnabled = true
        self.sendButton.alpha = 1
    }
}
