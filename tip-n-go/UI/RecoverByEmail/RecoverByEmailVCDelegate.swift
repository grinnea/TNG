//
//  RecoverByEmailVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 23.03.2022.
//

protocol RecoverByEmailVCDelegate {
    func cancelAction()
    func recoverByEmail(email: String)
}
