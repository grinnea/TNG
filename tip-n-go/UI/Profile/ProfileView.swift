//
//  ProfileView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

import UIKit
import Highcharts

@IBDesignable
final class ProfileView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var headerView: NavBarView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceCount: UILabel!
    @IBOutlet weak var withdrawButton: UIButton!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var chartTipsView: UIView!
    @IBOutlet weak var chartTipSelectCollection: UICollectionView!
    @IBOutlet weak var chartTipViewBackground: UIView!
    
    @IBOutlet weak var chartReviewsView: UIView!
    @IBOutlet weak var chartReviewSelectCollection: UICollectionView!
    @IBOutlet weak var chartReviewPageControl: UIPageControl!
    @IBOutlet weak var chartReviewViewBackground: UIView!
    
    var delegate: ProfileVCDelegate?
    var chartViewLine: HIChartView!
    var chartViewPie: HIChartView!
    let listChartPeriod = ["day".localized(), "week".localized(), "month".localized(), "year".localized()]
    
    // MARK: - Methods
    
    func prepareView() {
        shadowView(addView: headerView)
        shadowView(addView: balanceView)
        shadowView(addView: chartTipsView)
        shadowView(addView: chartReviewsView)
        
        withdrawButton.setTitle("profile_withdraw".localized(), for: .normal)
        balanceTitle.text = "profile_balance".localized()
        
        if delegate?.getUser()?.userOrganizations?.count == 0 {
            chartTipsView.isHidden = true
            chartReviewsView.isHidden = true
        }
    }
    
    func prepareTable() {
        menuTableView.dataSource = self
        menuTableView.delegate = self
        menuTableView?.register(R.nib.profileCell)
        menuTableView.tableFooterView = UIView()
    }
    
    func prepareBalance(financeBalance: Balance) {
        withdrawButton.isHidden = financeBalance.balance == 0
        balanceCount.text = "\(Double((financeBalance.balance) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (financeBalance.currency?.sign ?? "")
    }
    
    func collectionViewSetup(viewCollection: UICollectionView) {
        viewCollection.showsHorizontalScrollIndicator = false
        viewCollection.bounces = false
        viewCollection.register(UINib(nibName: R.nib.periondChartCell.identifier, bundle: nil), forCellWithReuseIdentifier: R.nib.periondChartCell.identifier)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 70, height: 24)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        viewCollection.collectionViewLayout = flowLayout
        viewCollection.dataSource = self
        viewCollection.delegate = self
    }
    
    func preparePageControl() {
        chartReviewPageControl.numberOfPages = delegate?.getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
    }
    
    func prepareChartView() {
        for view in self.chartReviewViewBackground.subviews {
            view.removeFromSuperview()
        }
       prepareChartPie(metric: chartReviewPageControl.currentPage)
    }
    
    func transition(vc: UIView, way: Bool) {
        self.transitionView(vc: vc, duration: 0.3, type: way ? .fromLeft : .fromRight)
    }
    
    func prepareChartLine() {
        chartViewLine = HIChartView(frame: CGRect(x: chartTipViewBackground.bounds.origin.x,
                                              y: chartTipViewBackground.bounds.origin.y + 8,
                                              width: chartTipViewBackground.bounds.size.width - 8,
                                              height: chartTipViewBackground.bounds.size.height))
        let arrColumn = delegate?.getReportsData()?.filter({ $0.type == "column" })
        let currency = delegate?.getUser()?.currency?.sign ?? ""
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrColumn?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrColumn?.first?.title?.text
        options.title = title
        
        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.category + '</b>: ' + this.point.y?.toFixed(2) + ' ' + '\(currency)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        
        let xAxis = HIXAxis()
        xAxis.categories = arrColumn?.first?.xAxis?.categories
        options.xAxis = [xAxis]
        
        let yAxis = HIYAxis()
        yAxis.title = HITitle()
        yAxis.title.text = arrColumn?.first?.yAxis?.title?.text
        options.yAxis = [yAxis]
        
        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        let series = HIColumn()
        series.data = arrColumn?.first?.series?.first!.dataLine
        series.name = arrColumn?.first?.series?.first?.name
        options.series = [series]
        
        chartViewLine.options = options
        chartTipViewBackground.addSubview(chartViewLine)
    }
    
    func prepareChartPie(metric: Int) {
        chartViewPie = HIChartView(frame: CGRect(x: chartReviewViewBackground.bounds.origin.x,
                                              y: chartReviewViewBackground.bounds.origin.y + 8,
                                              width: chartReviewViewBackground.bounds.size.width - 8,
                                              height: chartReviewViewBackground.bounds.size.height))
        chartViewPie.plugins = ["no-data-to-display"]
        let arrPie = delegate?.getReportsData()?.filter({ $0.type == "pie" })
        
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrPie?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrPie?.first?.title?.text
        options.title = title

        let subtitle = HISubtitle()
        subtitle.text = arrPie?.first?.series?[metric].name
        subtitle.style = HICSSObject()
        subtitle.style.fontSize = "16px"
        options.subtitle = subtitle

        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.name + '</b>: ' + this.point.y?.toFixed() + ' review(s) (' + this.point.percentage?.toFixed() + '%)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting

        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        var arrData = [Any]()
        arrPie?.first?.series?[metric].dataPie?.forEach({
            arrData.append(["name": $0.name ?? "", "y": $0.y ?? 0.0, "color": $0.color ?? ""])
        })
        
        let series = HIPie()
        series.data = arrData
        
        options.series = [series]
        
        chartViewPie.options = options
        chartReviewViewBackground.addSubview(chartViewPie)
    }
    
    func sizeHeaderToFit() {
        let headerView = menuTableView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = delegate?.getUser()?.userOrganizations?.count == 0 ? balanceView.frame.origin.y + balanceView.frame.height + 8  : chartReviewsView.frame.origin.y + chartReviewsView.frame.height + 8
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        menuTableView.tableHeaderView = headerView
    }
    
    // MARK: - Actions
    
    @IBAction func paymentsAction(_ sender: Any) {
        delegate?.openWithdraw()
    }
    
    @IBAction func chartPageACtion(_ sender: Any) {
        prepareChartView()
    }
}

// MARK: - Table view data source

extension ProfileView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = indexPath.row
        if row == 3 {
            return 67
        }else {
            return 92
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.profileCell, for: indexPath)
        else { return UITableViewCell() }
        let row = indexPath.row
        if row == 0 {
            cell.menuTitle.text = "profile_history_of_withdrawals".localized()
            cell.menuInfo.text = "profile_the_history_of_all_withdrawals".localized()
            cell.menuIcon.image = R.image.history()
        } else if row == 1 {
            cell.menuTitle.text = "profile_organizations".localized()
            cell.menuInfo.text = "profile_list_of_your_organizations".localized()
            cell.menuIcon.image = R.image.tips()
        } else if row == 2 {
            cell.menuTitle.text = "profile_settings_and_notifications".localized()
            cell.menuInfo.text = "profile_profile_settings_and_notifications".localized()
            cell.menuIcon.image = R.image.settings()
        } else if row == 3 {
            cell.menuTitle.text = "profile_sign_out".localized()
            cell.menuTitle.textColor = .red
            cell.menuInfo.text = ""
            cell.menuIcon.image = R.image.out()
        }
        shadowView(addView: cell.viewBackground)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.open(section: indexPath.section, row: indexPath.row)
    }

}

// MARK: - Collection view data source

extension ProfileView: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listChartPeriod.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)!
        cell.title.text = listChartPeriod[indexPath.row]
        if cell.isSelected {
                cell.backgroundColor = R.color.blue()
            }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.getPeriodReport(period: listChartPeriod[indexPath.row], type: collectionView == chartTipSelectCollection)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = false
    }
    
}
