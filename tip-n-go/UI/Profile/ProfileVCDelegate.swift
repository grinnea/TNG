//
//  ProfileVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

protocol ProfileVCDelegate {
    func openOrganizationView()
    func openOrganizationsList()
    func openTips()
    func openStaff()
    func backAction()
    func openWithdraw()
    func getUserOrganizations() -> UserOrganizations?
    func open(section: Int, row: Int)
    func getUser() -> UserCurrent?
    func getRoleToShow() -> Bool
    func getReportsData() -> ReportsData?
    func getReport(period: String)
    func getPeriodReport(period: String, type: Bool)
}
