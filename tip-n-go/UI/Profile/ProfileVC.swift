//
//  ProfileVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 17.03.2022.
//

import UIKit
import PKHUD
import ecommpaySDK

class ProfileVC: BaseVC, ProfileVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: ProfileView!

    var userCurrent = UserCurrent()
    var financeBalance = Balance()
    var reportsData = ReportsData()
    var way = true
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
        root.collectionViewSetup(viewCollection: root.chartTipSelectCollection)
        root.collectionViewSetup(viewCollection: root.chartReviewSelectCollection)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Api.financeBalance(self.handleGetFinanceBalance(success:response:error:))
        getReport(period: "Month")
        self.root.chartTipSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        self.root.chartReviewSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        let swipeToLeft = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        let swipeToRight = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        swipeToLeft.direction = .right
        swipeToRight.direction = .left
        root.chartReviewsView.addGestureRecognizer(swipeToLeft)
        root.chartReviewsView.addGestureRecognizer(swipeToRight)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        root.sizeHeaderToFit()
    }
    
    // MARK: - Methods
  
    func getUser() -> UserCurrent? {
        return getUserCurrent()
    }
    
    func getUserOrganizations() -> UserOrganizations? {
        return getUserCurrent()?.userOrganizations
    }
    
    func getRoleToShow() -> Bool {
        if getUserCurrent()?.userOrganizations?.count == 0 {
            return true
        }else{
            return getUserCurrent()?.userOrganizations?.filter({ $0.organizationRole?.id == 3 }).count != 0
        }
    }
    
    func openWithdraw() {
        self.openVC(vc: R.storyboard.main.withdrawVC())
    }
    
    func openOrganizationView() {
        self.openVC(vc: R.storyboard.main.organizationVC())
    }
    
    func openOrganizationsList() {
        self.openVC(vc: R.storyboard.main.organizationsListVC())
    }
    
    func openStaff() {
        self.openVC(vc: R.storyboard.main.staffVC())
    }
    
    func openTips() {
        self.openVC(vc: R.storyboard.main.tipsVC())
    }
    
    func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getReportsData() -> ReportsData? {
        return reportsData
    }
    
    func getReport(period: String) {
        var charts = [[String:String]]()
        charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentTips"])
        charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentReviews"])
        Api.getReportsData(charts: charts, self.handleGetReportsData(success:response:error:))
    }
    
    func getPeriodReport(period: String, type: Bool) {
        var charts = [[String:String]]()
            if type {
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentTips"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataTip(success:response:error:))
            }else{
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentReviews"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataReview(success:response:error:))
            }
        }
    
    @objc func changePageOnSwipe(_ gesture: UISwipeGestureRecognizer) {
        let numPage = getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
        if gesture.direction == .left && root.chartReviewPageControl.currentPage < numPage - 1 {
            root.chartReviewPageControl.currentPage += 1
            way = false
            self.refreshSwipe()
        } else if gesture.direction == .right && root.chartReviewPageControl.currentPage > 0{
            root.chartReviewPageControl.currentPage -= 1
            way = true
            self.refreshSwipe()
        }
    }
    
    func refreshSwipe() {
        DispatchQueue.main.async {
            self.root.prepareChartView()
            self.root.transition(vc: self.root.chartReviewViewBackground, way: self.way)
        }
    }
    
    func open(section: Int, row: Int) {
        if row == 0 {
            self.performSegue(withIdentifier: R.segue.profileVC.historyWithdrawSegue, sender: self)
        } else if row == 1 {
            let vc = R.storyboard.main.organizationsListVC()!
            self.navigationController?.pushViewController(vc, animated: true)
        } else if row == 2 {
            self.performSegue(withIdentifier: R.segue.profileVC.userSettingsSegue, sender: self)
        } else if row == 3 {
            UserDefaults.clientDefaultOrganizationHash = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
            Api.removeFcmToken(completion: { success, data in
                if success {
                    UserDefaults.flushClientData()
                }
            })
            self.openVC(vc: R.storyboard.main.loginVC())
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = R.segue.profileVC.historyWithdrawSegue(segue: segue) {
            vc.destination.balance = financeBalance
        }
    }
    
    // MARK: - Handle
    
    func handleGetFinanceBalance(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    financeBalance = try jsonDecoder.decode(Balance.self, from: data)
                    root.prepareBalance(financeBalance: financeBalance)
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleGetReportsData(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
                root.prepareChartPie(metric: 0)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataTip(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataReview(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartPie(metric: root.chartReviewPageControl.currentPage)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}

