//
//  ProfileCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 06.04.2022.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var menuInfo: UILabel!
    @IBOutlet weak var menuIcon: UIImageView!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        menuTitle.font = UIFont.systemFont(ofSize: 20)
        menuInfo.font = UIFont.systemFont(ofSize: 14)
    }

}
