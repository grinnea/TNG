//
//  TipsCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class TipsCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSum: UILabel!
    @IBOutlet weak var tipRateLabel: UILabel!
    @IBOutlet weak var background: UIView!
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        tipRateLabel.layer.cornerRadius = tipRateLabel.frame.height/2
        tipRateLabel.layer.masksToBounds = true
    }
}
