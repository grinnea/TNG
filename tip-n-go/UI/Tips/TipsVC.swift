//
//  TipsVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

import UIKit
import PKHUD

class TipsVC: BaseVC, TipsVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: TipsView!

    var tips = Tips()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiTips()
        root.headerView.preparePageControl()
        NotificationCenter.default.addObserver(self, selector: #selector(reNewView), name: NSNotification.Name(rawValue:  "organizationHash"), object: nil)
    }
    
    // MARK: - Methods
    
    @objc func reNewView() {
        root.headerView.transition(vc: self.root.tipsTableView)
        root.headerView.transition(vc: self.root.emptyView)
        root.headerView.transition(vc: self.root.emptyTitle)
        root.headerView.transition(vc: self.root.emptyText)
        getApiTips()
        root.tipsTableView.reloadData()
        root.prepareEmptyView()
    }
    
    func getUserOrganizations() -> UserOrganizations? {
        return getUserCurrent()?.userOrganizations
    }
    
    func getUserTips() -> Tips? {
        return tips
    }
    
    func getRoleToShow() -> Bool {
        if getUserCurrent()?.userOrganizations?.count == 0 {
            return true
        }else{
            return getUserCurrent()?.userOrganizations?.filter({ $0.organizationRole?.id == 3 }).count != 0
        }
    }
    
    func getApiTips() {
        if getRoleToShow() {
            if getUserOrganizations()?.count == 0 {
                Api.userTips(self.handleGetUserTips(success:response:error:))
            }else{
                Api.tipsByHashAndOrganizationHash(userHash: BaseVC().getUserCurrent()?.hash ?? "", organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserTips(success:response:error:))
            }
        }else{
            Api.userTipsByOrganization(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserTips(success:response:error:))
        }
    }
    
    // MARK: - Handle
    
    func handleGetUserTips(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    tips = try jsonDecoder.decode(Tips.self, from: data)
                    self.root.tipsTableView.reloadData()
                    self.root.prepareEmptyView()
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
}

