//
//  TipsView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

import UIKit

@IBDesignable
final class TipsView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var headerView: NavBarView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tipsTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: TipsVCDelegate?
    
    
    // MARK: - Methods
    
    func prepareView() {
        title.text = "tips".localized()
        emptyTitle.text = "tips_empty_title".localized()
        emptyText.text = "tips_empty_text".localized()
        shadowView(addView: headerView)
        shadowView(addView: emptyView)
    }
    
    func prepareTable() {
        tipsTableView.dataSource = self
        tipsTableView.delegate = self
        tipsTableView?.register(R.nib.tipsCell)
        tipsTableView.tableFooterView = UIView()
    }
    
    func prepareEmptyView() {
        if delegate?.getUserTips()?.count == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
        }
    }
}

// MARK: - Table view data source

extension TipsView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getUserTips()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tipsCell, for: indexPath)!
        let row = delegate?.getUserTips()?[indexPath.row]
        cell.labelName.text = delegate?.getUserOrganizations()?.count == 0 ? "" : (row?.user?.lastName ?? "") + " " + (row?.user?.firstName ?? "")
        cell.labelDate.text = row?.date?.toFullFormatWithTime()
        cell.labelSum.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
        cell.tipRateLabel.text = "\(Double((row?.tipRate) ?? 0).convertedFromBackendFormat(afterPoint: 1))%"
        
        shadowView(addView: cell.background)
        return cell
    }
    
}
