//
//  TipsVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

protocol TipsVCDelegate {
    func getUserOrganizations() -> UserOrganizations?
    func getUserTips() -> Tips?
}
