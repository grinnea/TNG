//
//  SettingsHeaderView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

import UIKit

class SettingsHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Initialization & Lifecycle

    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Methods
    

}
