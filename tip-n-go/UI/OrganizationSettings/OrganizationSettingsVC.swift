//
//  OrganizationSettingsVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

import UIKit
import PKHUD

class OrganizationSettingsVC: BaseVC, OrganizationSettingsVCDelegate {
    
    // MARK: Properties
    
    @IBOutlet private weak var root: OrganizationSettingsView!
    
    var organizationSettings = OrganizationSettings()
    var editSetings = false
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiOrganizationUsers()
    }
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func saveAction(settings: [[String: Any]]) {
        editSetings = true
        HUD.show(.progress)
        Api.organizationPutSettings(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", settings: settings, self.handleGetOrganizationSettings(success:response:error:))
    }
    
    func getOrganizationSettings() -> OrganizationSettings? {
        return organizationSettings
    }
    
    func getApiOrganizationUsers() {
        editSetings = false
        Api.organizationGetSettings(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetOrganizationSettings(success:response:error:))
    }
    
    // MARK: - Handle
    
    func handleGetOrganizationSettings(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                organizationSettings = try jsonDecoder.decode(OrganizationSettings.self, from: data)
                self.root.tableView.reloadData()
                if editSetings { HUD.flash(.success, delay: 1.0) }
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
