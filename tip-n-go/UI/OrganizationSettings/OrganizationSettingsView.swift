//
//  OrganizationSettingsView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

import UIKit

@IBDesignable
final class OrganizationSettingsView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: OrganizationSettingsVCDelegate?
    var settings = [[String: Any]]()
    
    // MARK: - Methods
    
    func prepareView(){
        title.text = "profile_settings_and_notifications".localized()
        saveButton.setTitle("save".localized(), for: .normal)
    }
    
    func prepareTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView?.register(R.nib.organizationSettingsCell)
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        delegate?.saveAction(settings: settings)
    }
}

// MARK: - Table view data source

extension OrganizationSettingsView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return delegate?.getOrganizationSettings()?.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed(R.nib.settingsHeaderView.name,
                                                  owner: self, options: nil)?.last as! SettingsHeaderView
        headerView.titleLabel.text = delegate?.getOrganizationSettings()?[section].name
        headerView.descriptionLabel.text = delegate?.getOrganizationSettings()?[section].description
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getOrganizationSettings()?[section].options?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.organizationSettingsCell, for: indexPath)!
        let row = delegate?.getOrganizationSettings()?[indexPath.section].options?[indexPath.row]
        cell.titleLabel.text = row?.name
        cell.infoLabel.text = row?.description
        if row?.checked ?? false {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        shadowView(addView: cell.borderView)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = delegate?.getOrganizationSettings()?[indexPath.section]
        let row = delegate?.getOrganizationSettings()?[indexPath.section].options?[indexPath.row]
        settings.removeAll()
        settings = [["key": section?.key ?? "", "selectedOptionId": [row?.id ?? 0]]]
    }

}

