//
//  OrganizationSettingsCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

import UIKit

class OrganizationSettingsCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var boxImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.boxImage.image = selected ? R.image.boxOn() : R.image.boxOff()
    }
}
