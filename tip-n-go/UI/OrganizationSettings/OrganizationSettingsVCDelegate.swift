//
//  OrganizationSettingsVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 27.04.2022.
//

protocol OrganizationSettingsVCDelegate {
    func backAction()
    func getOrganizationSettings() -> OrganizationSettings?
    func saveAction(settings: [[String: Any]]) 
}
