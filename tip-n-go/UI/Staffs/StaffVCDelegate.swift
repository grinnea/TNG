//
//  StaffVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

protocol StaffVCDelegate {
    func backAction()
    func goToPrifile()
    func goToAddStaff()
    func openSelectUser(userHash: String)
    func getStaff() -> User2Organizations?
    func getOrganizationName() -> String?
}
