//
//  StaffView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit
import SDWebImage

@IBDesignable
final class StaffView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addStaff: UIButton!
    @IBOutlet weak var staffTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    @IBOutlet weak var emptyAddStaff: UIButton!
    
    var delegate: StaffVCDelegate?
    
    // MARK: - Methods
    
    func prepareTable() {
        staffTableView.dataSource = self
        staffTableView.delegate = self
        staffTableView?.register(R.nib.staffCell)
        staffTableView.tableFooterView = UIView()
    }
    
    func prepareView() {
        titleLabel.text = "organizations_staff".localized()
        addStaff.setTitle("organizations_staff".localized(), for: .normal)
        emptyTitle.text = "staff_empty_title".localized()
        emptyText.text = "staff_empty_text".localized()
        emptyAddStaff.setTitle("organizations_staff".localized(), for: .normal)
        shadowView(addView: emptyView)
    }
    
    func prepareEmptyView() {
        if delegate?.getStaff()?.count == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
            emptyAddStaff.isHidden = false
            addStaff.isHidden = true
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
            emptyAddStaff.isHidden = true
            addStaff.isHidden = false
        }
    }
    
    // MARK: - Actions
    
    @IBAction func addStaffAction(_ sender: Any) {
        delegate?.goToAddStaff()
    }
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
}

// MARK: - Table view data source

extension StaffView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getStaff()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.staffCell, for: indexPath)!
        let row = delegate?.getStaff()?[indexPath.row]
        cell.nameUser.text = (row?.lastName ?? "") + " " + (row?.firstName ?? "")
        cell.userPosition.text = row?.position ?? ""
        cell.avatar.sd_setImage(with: URL(string: row?.avatar ?? ""), placeholderImage: R.image.avatarDemo())
        cell.rateLabel.text = row?.rating ?? 0 == 0 ? "" : "\(Double((row?.rating) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
        cell.starImage.image = getStarRate(rate: row?.rating)
        
        shadowView(addView: cell.borderView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.openSelectUser(userHash: delegate?.getStaff()?[indexPath.row].hash ?? "")
    }
}
