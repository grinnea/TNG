//
//  StaffVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit
import PKHUD

class StaffVC: BaseVC, StaffVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: StaffView!

    var user2Organizations = User2Organizations()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        getApiOrganizationUsers()
        root.prepareView()
    }
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getApiOrganizationUsers() {
        Api.organizationUsersByOrganization(organizationHash: getHashOrganization() ?? "", self.handleGetUserEmployees(success:response:error:))
    }
    
    func goToAddStaff() {
        self.openVC(vc: R.storyboard.main.addStaffVC())
    }
    
    func goToPrifile() {
        self.openVC(vc: R.storyboard.main.profileVC())
    }
    
    func getHashOrganization() -> String? {
        if UserDefaults.clientDefaultOrganizationHash == nil {
            return getUserCurrent()?.userOrganizations?.first?.organization?.hash
        }else{
            return UserDefaults.clientDefaultOrganizationHash
        }
    }
    
    func getOrganizationName() -> String? {
        if UserDefaults.clientDefaultOrganizationHash == nil {
            return getUserCurrent()?.userOrganizations?.first?.organization?.name
        }else{
            return getUserCurrent()?.userOrganizations?.first(where: {$0.organization?.hash == UserDefaults.clientDefaultOrganizationHash })?.organization?.name
        }
    }
    
    func openSelectUser(userHash: String) {
        let vc = R.storyboard.main.employeeInfoVC()
        vc?.userHash = userHash
        self.openVC(vc: vc)
    }
    
    func getStaff() -> User2Organizations? {
        return user2Organizations
    }
    
    // MARK: - Handle
    
    func handleGetUserEmployees(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                user2Organizations = try jsonDecoder.decode(User2Organizations.self, from: data)
                self.root.staffTableView.reloadData()
                self.root.prepareEmptyView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
}
