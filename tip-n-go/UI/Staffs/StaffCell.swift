//
//  StaffCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class StaffCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var userPosition: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
