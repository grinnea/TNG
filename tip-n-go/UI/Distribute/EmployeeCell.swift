//
//  EmployeeCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class EmployeeCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var userPosition: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var boxImage: UIImageView!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.boxImage.image = selected ? R.image.boxOn() : R.image.boxOff()
    }
}
