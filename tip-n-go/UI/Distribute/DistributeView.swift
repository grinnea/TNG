//
//  DistributeView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 21.04.2022.
//

import UIKit
import SDWebImage

@IBDesignable
final class DistributeView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var titleTable: UILabel!
    @IBOutlet weak var tipsTableView: UITableView!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var distributeButton: UIButton!
    @IBOutlet weak var selectAllTitle: UILabel!
    @IBOutlet weak var selectAllBox: UIImageView!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonBlue: UIButton!
    @IBOutlet weak var finishDistributeButton: UIButton!
    @IBOutlet weak var selectAllView: UIView!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    //    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: DistributeVCDelegate?
    var arrTips = [String]()
    var arrStaffs = [String]()
    var distributeCount = Int()
    
    // MARK: - Methods
    
    func prepareView() {
        shadowView(addView: emptyView)
        title.text = "distribution_tip_distribution".localized()
        selectAllTitle.text = "select_all".localized()
        backButtonBlue.setTitle("distribution_back".localized(), for: .normal)
        finishDistributeButton.setTitle("distribution_distribute".localized(), for: .normal)
        distributeButton.setTitle("distribution_distribute".localized(), for: .normal)
        titleTable.text = "distribution_select_tips_to_share".localized()
        selectAllBox.image = R.image.boxOff()
        distributeButton.setTitle("distribution_distribute".localized(), for: .normal)
        distributeButton.isHidden = false
        buttonStackView.isHidden = true
    }
    
    func prepareTable() {
        tipsTableView.dataSource = self
        tipsTableView.delegate = self
        tipsTableView?.register(R.nib.tipsToShareCell)
        tipsTableView?.register(R.nib.employeeCell)
        tipsTableView.tableFooterView = UIView()
    }
    
    func prepareOk() {
        backButton.isHidden = true
        selectAllView.isHidden = true
        titleTable.isHidden = true
        tipsTableView.isHidden = true
        emptyView.isHidden = false
        emptyImage.image = R.image.ok()
        emptyTitle.isHidden = false
        emptyTitle.text = "distribution_tips_were_successfully_distributed_to_staff_accounts".localized()
        distributeButton.setTitle("Ok", for: .normal)
        distributeButton.isHidden = false
        buttonStackView.isHidden = true
    }
    
    func prepareEmptyView() {
        //        if delegate?.getUserTips()?.count == 0
        //            emptyView.isHidden = false
        //            emptyTitle.isHidden = false
        //            emptyText.isHidden = false
        //        }else{
        //            emptyView.isHidden = true
        //            emptyTitle.isHidden = true
        //            emptyText.isHidden = true
        //        }
    }
    
    func addSelectTip(row: Int) {
        let tip = delegate?.getUserTips()?[row]
        arrTips.append(tip?.hash ?? "")
        distributeCount = distributeCount + (tip?.amount ?? 0)
        if distributeCount == 0 {
            distributeButton.setTitle("distribution_distribute".localized(), for: .normal)
        }else{
            distributeButton.setTitle("distribution_distribute".localized() + " \(Double((distributeCount) ).convertedFromBackendFormat(afterPoint: 2)) " + (tip?.currency?.sign ?? ""), for: .normal)
        }
    }
    
    func removeSelectTip(row: Int) {
        let tip = delegate?.getUserTips()?[row]
        arrTips = arrTips.filter({ $0 != tip?.hash ?? "" })
        distributeCount = distributeCount - (tip?.amount ?? 0)
        if distributeCount == 0 {
            distributeButton.setTitle("distribution_distribute".localized(), for: .normal)
        }else{
            distributeButton.setTitle("distribution_distribute".localized() + " \(Double((distributeCount) ).convertedFromBackendFormat(afterPoint: 2)) " + (tip?.currency?.sign ?? ""), for: .normal)
        }
    }
    
    func addSelectStaff(row: Int) {
        let user = delegate?.getStaff()?[row]
        arrStaffs.append(user?.hash ?? "")
    }
    
    func removeSelectStaff(row: Int) {
        let user = delegate?.getStaff()?[row]
        arrStaffs = arrStaffs.filter({ $0 != user?.hash ?? "" })
    }
    
    func selectAllRows() {
        if selectAllBox.image == R.image.boxOff() {
            if titleTable.text == "distribution_select_tips_to_share".localized() {
                arrTips.removeAll()
                distributeCount = 0
            }else{
                arrStaffs.removeAll()
            }
            let totalRows = tipsTableView.numberOfRows(inSection: 0)
            for row in 0..<totalRows {
                tipsTableView.selectRow(at: IndexPath(row: row, section: 0), animated: true, scrollPosition: .none)
                if titleTable.text == "distribution_select_tips_to_share".localized() {
                    addSelectTip(row: row)
                }else{
                    addSelectStaff(row: row)
                }
            }
            selectAllBox.image = R.image.boxOn()
        }else{
            let totalRows = tipsTableView.numberOfRows(inSection: 0)
            for row in 0..<totalRows {
                tipsTableView.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
                if titleTable.text == "distribution_select_tips_to_share".localized() {
                    removeSelectTip(row: row)
                }else{
                    removeSelectStaff(row: row)
                }
            }
            selectAllBox.image = R.image.boxOff()
        }
    }
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func selectAllAction(_ sender: Any) {
        selectAllRows()
    }
    
    @IBAction func distributeFirstAction(_ sender: Any) {
        if titleTable.isHidden == false {
            titleTable.text = "distribution_select_employees".localized()
            distributeButton.isHidden = true
            buttonStackView.isHidden = false
            tipsTableView.reloadData()
        }else{
            delegate?.backAction()
        }
    }
    
    @IBAction func backToTipsAction(_ sender: Any) {
        titleTable.text = "distribution_select_tips_to_share".localized()
        distributeButton.isHidden = false
        buttonStackView.isHidden = true
        arrStaffs.removeAll() // clean select
        tipsTableView.reloadData()
    }
    
    @IBAction func distributeFinishAction(_ sender: Any) {
        delegate?.getApiFinanceDistributeTips(tipHashes: arrTips, userHashes: arrStaffs, amount: distributeCount)
    }
    
}

// MARK: - Table view data source

extension DistributeView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return titleTable.text == "distribution_select_tips_to_share".localized() ? 85 : 88
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleTable.text == "distribution_select_tips_to_share".localized() ? delegate?.getUserTips()?.count ?? 0 : delegate?.getStaff()?.count ?? 0
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if titleTable.text == "distribution_select_tips_to_share".localized() {
            selectAllBox.image = arrTips.count == delegate?.getUserTips()?.count ?? 0 ? R.image.boxOn() : R.image.boxOff()
        }else{
            selectAllBox.image = arrStaffs.count == delegate?.getStaff()?.count ?? 0 ? R.image.boxOn() : R.image.boxOff()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if titleTable.text == "distribution_select_tips_to_share".localized() {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tipsToShareCell, for: indexPath)!
            let row = delegate?.getUserTips()?[indexPath.row]
            if arrTips.contains(row?.hash ?? "") {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
            cell.labelName.text = (row?.user?.lastName ?? "") + " " + (row?.user?.firstName ?? "")
            cell.labelDate.text = row?.date?.toFullFormatWithTime()
            cell.labelSum.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
            cell.tipRateLabel.text = "\(Double((row?.tipRate) ?? 0).convertedFromBackendFormat(afterPoint: 1))%"
            
            shadowView(addView: cell.background)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.employeeCell, for: indexPath)!
            let row = delegate?.getStaff()?[indexPath.row]
            if arrStaffs.contains(row?.hash ?? "") {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
            cell.nameUser.text = (row?.lastName ?? "") + " " + (row?.firstName ?? "")
            cell.userPosition.text = row?.position
            cell.avatar.sd_setImage(with: URL(string: row?.avatar ?? ""), placeholderImage: R.image.avatarDemo())
            
            shadowView(addView: cell.borderView)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if titleTable.text == "distribution_select_tips_to_share".localized() {
            addSelectTip(row: indexPath.row)
        }else{
            addSelectStaff(row: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if titleTable.text == "distribution_select_tips_to_share".localized() {
            removeSelectTip(row: indexPath.row)
        }else{
            removeSelectStaff(row: indexPath.row)
        }
        selectAllBox.image = R.image.boxOff()
    }
}
