//
//  DistributeVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 21.04.2022.
//

import UIKit
import PKHUD

class DistributeVC: BaseVC, DistributeVCDelegate {

    // MARK: Properties
    
   @IBOutlet private weak var root: DistributeView!
    
    var tips = Tips()
    var user2Organizations = User2Organizations()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiTips()
        getApiOrganizationUsers()
    }
    
    // MARK: - Methods
        
    func getUserOrganizations() -> UserOrganizations? {
        return getUserCurrent()?.userOrganizations
    }
    
    func getUserTips() -> Tips? {
        return tips
    }
    
    func getStaff() -> User2Organizations? {
        return user2Organizations
    }
    
    func getHashOrganization() -> String? {
        if UserDefaults.clientDefaultOrganizationHash == nil {
            return getUserCurrent()?.userOrganizations?.first?.organization?.hash
        }else{
            return UserDefaults.clientDefaultOrganizationHash
        }
    }
    
    func getApiTips() {
        Api.financeOrganizationUndistributedTips(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserTips(success:response:error:))
    }
    
    func getApiOrganizationUsers() {
        Api.organizationUsersByOrganization(organizationHash: getHashOrganization() ?? "", self.handleGetUserEmployees(success:response:error:))
    }
    
    func getApiFinanceDistributeTips(tipHashes: [String], userHashes: [String], amount: Int) {
        HUD.show(.progress)
        Api.financeDistributeTips(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", tipHashes: tipHashes, userHashes: userHashes, amount: amount, self.handleFinanceDistributeTips(success:response:error:))
    }
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Handle
    
    func handleGetUserTips(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    tips = try jsonDecoder.decode(Tips.self, from: data)
                    self.root.tipsTableView.reloadData()
//                    self.root.prepareEmptyView()
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleGetUserEmployees(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    user2Organizations = try jsonDecoder.decode(User2Organizations.self, from: data)
                    self.root.tipsTableView.reloadData()
//                    self.root.prepareEmptyView()
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleFinanceDistributeTips(success: Bool, response: Data?, error: Error?) {
        if success {
            root.prepareOk()
            HUD.hide()
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
