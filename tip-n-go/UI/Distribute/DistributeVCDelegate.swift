//
//  DistributeVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 21.04.2022.
//

protocol DistributeVCDelegate {
    func backAction()
    func getUserTips() -> Tips?
    func getStaff() -> User2Organizations?
    func getApiFinanceDistributeTips(tipHashes: [String], userHashes: [String], amount: Int)
}
