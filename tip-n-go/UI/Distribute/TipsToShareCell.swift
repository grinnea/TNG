//
//  TipsToShareCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class TipsToShareCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSum: UILabel!
    @IBOutlet weak var tipRateLabel: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var checkBoxImage: UIImageView!
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        tipRateLabel.layer.cornerRadius = tipRateLabel.frame.height/2
        tipRateLabel.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.checkBoxImage.image = selected ? R.image.boxOn() : R.image.boxOff()
    }
}
