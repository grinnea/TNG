//
//  QRCodeView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

import UIKit

@IBDesignable
final class QRCodeView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var organizationsView: TextFieldCustom!
    @IBOutlet weak var employeesView: TextFieldCustom!
    @IBOutlet weak var tableView: TextFieldCustom!
    @IBOutlet weak var orderTotalView: TextFieldCustom!
    @IBOutlet weak var generateButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageQRCode: UIImageView!
    @IBOutlet weak var tableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableUpConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    var delegate: QRCodeVCDelegate?

    // MARK: - Methods
    
    func prepareView() {
        titleLabel.text = "qrcode_generate_qr_code".localized()
        generateButton.setTitle("qrcode_generate".localized(), for: .normal)
        var listPicker = [String]()
        delegate?.getOrganizations()?.forEach({
            listPicker.append($0.organization?.name ?? "")
        })
        organizationsView.preparePicker(pickerType: true, pickerData: listPicker)
        organizationsView.textField.resignFirstResponder()
        organizationsView.label.text = "qrcode_organization".localized()
        if UserDefaults.clientDefaultOrganizationHash == nil || UserDefaults.clientDefaultOrganizationHash == "" {
            organizationsView.textField.text = "qrcode_self_employed".localized()
            organizationsView.textField.isEnabled = false
            organizationsView.alpha = 0.3
            tableViewConstraint.constant = 0
            tableUpConstraint.constant = 0
        }else{
            let nameOrganization = delegate?.getOrganizations()?.filter({ $0.organization?.hash == UserDefaults.clientDefaultOrganizationHash }).first?.organization?.name ?? ""
            organizationsView.textField.text = nameOrganization
            if let firstNameIndex = listPicker.firstIndex(of: nameOrganization) {
                organizationsView.pickerCustom.selectRow(firstNameIndex, inComponent: 0, animated: true)
            }
        }
        organizationsView.inputIsNotEmpty(empty: true)
        employeesView.label.text = "qrcode_employee".localized()
        employeesView.textField.text = (delegate?.getUserData()?.lastName ?? "") + " " + (delegate?.getUserData()?.firstName ?? "")
        employeesView.inputIsNotEmpty(empty: true)
        employeesView.textField.isEnabled = false
        employeesView.alpha = 0.3
        tableView.label.text = "qrcode_table_no".localized()
        tableView.textField.keyboardType = .numberPad
        tableView.isHidden = UserDefaults.clientDefaultOrganizationHash == nil
        orderTotalView.label.text = "qrcode_invoice_total".localized()
        orderTotalView.textField.keyboardType = .numberPad
        
        organizationsView.delegate = self
        employeesView.delegate = self
        tableView.delegate = self
        orderTotalView.delegate = self
    }
    
    // MARK: - Actions
    
    @IBAction func backAction(_ sender: Any) {
        viewInput.isHidden = false
        backButton.isHidden = true
        imageQRCode.isHidden = true
        generateButton.isHidden = false
        titleLabel.text = "qrcode_generate_qr_code".localized()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        delegate?.closeAction()
    }
    
    @IBAction func generateAction(_ sender: Any) {
        viewInput.isHidden = true
        backButton.isHidden = false
        imageQRCode.isHidden = false
        generateButton.isHidden = true
        titleLabel.text = employeesView.textField.text
        
        let hashOrganization = UserDefaults.clientDefaultOrganizationHash == nil ? "" : delegate?.getOrganizations()?.filter({ $0.organization?.name == organizationsView.textField.text }).first?.organization?.hash ?? ""
        let hashEmployee = delegate?.getUserData()?.hash ?? ""
        
        delegate?.generateAction(employeeHash: hashEmployee, organizationHash: hashOrganization, tableNo: Int(tableView.textField.text!) ?? 0, invoiceTotal: (Int(orderTotalView.textField.text!) ?? 0) * 100)
    }
    
}

extension QRCodeView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let organization = self.organizationsView.textField.text, !organization.isEmpty,
            let employee = self.employeesView.textField.text, !employee.isEmpty,
            let order = self.orderTotalView.textField.text, !order.isEmpty
        else
        {
            self.generateButton.isEnabled = false
            self.generateButton.alpha = 0.3
            return
        }
        self.generateButton.isEnabled = true
        self.generateButton.alpha = 1
    }
}
