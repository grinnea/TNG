//
//  QRCodeVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

protocol QRCodeVCDelegate {
    func closeAction()
    func getOrganizations() -> UserOrganizations?
    func getUserData() -> UserCurrent?
    func generateAction(employeeHash: String, organizationHash: String, tableNo: Int, invoiceTotal: Int)
}
