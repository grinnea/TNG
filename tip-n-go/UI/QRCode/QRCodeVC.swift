//
//  QRCodeVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

import UIKit
import PKHUD

class QRCodeVC: BaseVC, QRCodeVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: QRCodeView!

    var userCurrent = UserCurrent()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.checkTextFieldsIsNotEmpty()
        self.hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // MARK: - Methods
    
    func closeAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getOrganizations() -> UserOrganizations? {
        return getUserCurrent()?.userOrganizations
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        view.endEditing(true)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func getUserData() -> UserCurrent? {
        return userCurrent
    }
    
    func generateAction(employeeHash: String, organizationHash: String, tableNo: Int, invoiceTotal: Int) {
        Api.getQRCode(employeeHash: employeeHash, organizationHash: organizationHash, tableNo: tableNo, invoiceTotal: invoiceTotal, self.handleQRCode(success:response:error:))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        root.buttonConstraint.constant = keyboardSize.height + 10
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        root.buttonConstraint.constant = 30
        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Handle
    
    func handleQRCode(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    let qrData = try jsonDecoder.decode(QRCode.self, from: data).data ?? ""
                    self.root.imageQRCode.image = generateQRCode(from: qrData)
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
}
