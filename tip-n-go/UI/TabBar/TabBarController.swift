//
//  TabBarViewController.swift
//  tip-n-go
//
//  Created by developer03 on 12.10.2020.
//

import PKHUD
import UIKit

class TabBarController: UITabBarController, UIGestureRecognizerDelegate, UITabBarControllerDelegate {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.selectedIndex = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareView()
        getNotifications()
    }
    
    func prepareView() {
        tabBar.items![0].title = "dashboard".localized()
        tabBar.items![1].title = "tips".localized()
        tabBar.items![2].title = "review_reviews".localized()
        tabBar.items![3].title = "profile".localized()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        getNotifications()
    }
    
    func getNotifications() {
        Api.getNotifications(self.handleNotificationsResponse(success:response:error:))
    }

    func handleNotificationsResponse(success: Bool, response: Data?, error: Error?) {
        if success, let response = response {
            do {
                let notifications = try JSONDecoder().decode(NotificationMessages.self, from: response)
                var notificationsUnReadCount = 0
                notifications.forEach({
                    if $0.isRead == false {
                        notificationsUnReadCount += 1
                    }
                })
                self.tabBar.items![3].badgeValue = notificationsUnReadCount == 0 ? nil : "\(notificationsUnReadCount)"
                UserDefaults.notiEmpty = notificationsUnReadCount == 0
                NotificationCenter.default.post(name: NSNotification.Name("notificationCount"), object: nil)
                UIApplication.shared.applicationIconBadgeNumber = notificationsUnReadCount
            } catch let error {
                BaseVC().catchError(error: error)
            }
        } else if let response = response {
            BaseVC().handleErrorResponse(data: response)
        }
    }
}
