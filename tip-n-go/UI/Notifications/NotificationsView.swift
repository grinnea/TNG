//
//  NotificationsView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.05.2022.
//

import UIKit

@IBDesignable
final class NotificationsView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyImage: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: NotificationsVCDelegate?

    // MARK: - Methods
    
    func prepareTitle() {
        title.text = "notifications".localized()
        emptyTitle.text = "notifications_empty_title".localized()
        emptyText.text = "notifications_empty_text".localized()
        shadowView(addView: emptyImage)
    }
    
    func prepareTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(R.nib.notificationsCell)
    }
    
    func prepareEmpty(count: Int) {
        emptyImage.isHidden = count == 0 ? false : true
        emptyTitle.isHidden = count == 0 ? false : true
        emptyText.isHidden = count == 0 ? false : true
    }
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
}

// MARK: - Table View

extension NotificationsView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getNotificationsList()?.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notificationsCell, for: indexPath) else { return UITableViewCell() }
        let row = delegate?.getNotificationsList()?[indexPath.row]
        cell.dateLabel.text = row?.date?.toFullFormatWithTime()
        cell.titleLabel.text = row?.text
        if row?.isRead == false {
            cell.background.backgroundColor = R.color.pink()
        }else{
            cell.background.backgroundColor = R.color.white()
        }
        
        shadowView(addView: cell.background)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.notificationMarkAsRead(notificationId: delegate?.getNotificationsList()?[indexPath.row].id ?? 0)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "delete".localized()) {  (contextualAction, view, boolValue) in
            self.delegate?.notificationDelete(notificationId: self.delegate?.getNotificationsList()?[indexPath.row].id ?? 0)
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [deleteAction])
        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
