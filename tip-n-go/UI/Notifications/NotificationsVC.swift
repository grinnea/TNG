//
//  NotificationsVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.05.2022.
//

import UIKit

class NotificationsVC: BaseVC, NotificationsVCDelegate {

    // MARK: - Properties
    
    @IBOutlet weak var root: NotificationsView!
    
    var notifications = NotificationMessages()
    var userCurrent = UserCurrent()
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareTitle()
        root.prepareTable()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNotifications()
    }
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func getNotifications() {
        Api.getNotifications(self.handleNotificationsResponse(success:response:error:))
    }
    
    func getNotificationsList() -> NotificationMessages? {
        return notifications.sorted(by: {$0.date ?? "" > $1.date ?? ""})
    }
    
    func notificationMarkAsRead(notificationId: Int) {
        Api.notificationMarkAsRead(notificationId: notificationId, self.handleNotificationMarkAsRead(success:response:error:))
    }
    
    func notificationDelete(notificationId: Int) {
        Api.notificationDelete(notificationId: notificationId, self.handleNotificationMarkAsRead(success:response:error:))
    }
    
    // MARK: - Handlers
    
    func handleNotificationMarkAsRead(success: Bool, response: Data?, error: Error?) {
        if success {
            getNotifications()
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleNotificationsResponse(success: Bool, response: Data?, error: Error?) {
        if success, let response = response {
            do {
                self.notifications = try JSONDecoder().decode(NotificationMessages.self, from: response)
                self.root.prepareEmpty(count: notifications.count)
                self.root.tableView.reloadData()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let response = response {
            self.handleErrorResponse(data: response)
        }
    }
    
}
