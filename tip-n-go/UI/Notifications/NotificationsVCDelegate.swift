//
//  NotificationsVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.05.2022.
//

protocol NotificationsVCDelegate {
    func backAction()
    func getNotificationsList() -> NotificationMessages?
    func notificationMarkAsRead(notificationId: Int)
    func notificationDelete(notificationId: Int)
}
