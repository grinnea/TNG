//
//  NotificationsCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.05.2022.
//

import UIKit

class NotificationsCell: UITableViewCell {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        dateLabel.font = UIFont.systemFont(ofSize: 12)
    }
}
