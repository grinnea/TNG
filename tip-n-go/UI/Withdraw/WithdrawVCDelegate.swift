//
//  WithdrawVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.04.2022.
//

protocol WithdrawVCDelegate {
    func cancelAction()
    func getCardsList() -> UserBankCards?
    func withdraw(amount: Int, bankCardHash: String)
}
