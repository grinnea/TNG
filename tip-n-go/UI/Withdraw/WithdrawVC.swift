//
//  WithdrawVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.04.2022.
//

import UIKit
import PKHUD

class WithdrawVC: BaseVC, WithdrawVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: WithdrawView!
    
    var tip = Tip()
    var bankCards = UserBankCards()
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Api.financeBalance(self.handleGetFinanceBalance(success:response:error:))
        Api.financeBankCard(self.handleGetFinanceBankCard(success:response:error:))
    }
    
    // MARK: - Methods
    
    func cancelAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getCardsList() -> UserBankCards? {
        return bankCards
    }
   
    func withdraw(amount: Int, bankCardHash: String) {
        Api.financePayOut(amount: amount, bankCardHash: bankCardHash, self.handleFinancePayOut(success:response:error:))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        self.view.frame.origin.y = 0 - keyboardSize.height + 20
    }

    @objc func keyboardWillHide(notification: NSNotification) {
      self.view.frame.origin.y = 0
    }
    
    // MARK: - Handle
    
    func handleGetFinanceBalance(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    let financeBalance = try jsonDecoder.decode(Balance.self, from: data)
                    self.root.balanceLabel.text = "\(Double((financeBalance.balance) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (financeBalance.currency?.sign ?? "")
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleGetFinanceBankCard(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    bankCards = try jsonDecoder.decode(UserBankCards.self, from: data)
                    root.prepareFields()
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
    func handleFinancePayOut(success: Bool, response: Data?, error: Error?) {
            if success, let data = response {
                let jsonDecoder = JSONDecoder()
                do {
                    let financeBalance = try jsonDecoder.decode(Balance.self, from: data)
                    self.root.balanceLabel.text = "\(Double((financeBalance.balance) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (financeBalance.currency?.sign ?? "")
                    HUD.flash(.success, delay: 1.0)
                    root.amountView.textField.text = ""
                } catch let error {
                    self.catchError(error: error)
                }
            } else if let data = response {
                self.handleErrorResponse(data: data)
            }
    }
    
}

