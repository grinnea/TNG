//
//  WithdrawView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.04.2022.
//

import UIKit
import TextFieldEffects

@IBDesignable
final class WithdrawView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var cardView: TextFieldCustom!
    @IBOutlet weak var amountView: TextFieldCustom!
    @IBOutlet weak var withdrawButton: UIButton!
    
    var delegate: WithdrawVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        shadowView(addView: balanceView)
        title.text = "profile_withdraw".localized()
        balanceTitle.text = "profile_my_balance".localized()
        withdrawButton.setTitle("profile_withdraw".localized(), for: .normal)
        checkTextFieldsIsNotEmpty()
    }
    
    func prepareFields() {
        var listPicker = [String]()
        delegate?.getCardsList()?.forEach({
            listPicker.append($0.cardNumberMasked ?? "")
        })
        cardView.delegate = self
        amountView.delegate = self
        
        cardView.preparePicker(pickerType: true, pickerData: listPicker)
        cardView.textField.resignFirstResponder()
        cardView.label.text = "profile_card".localized()
        cardView.textField.text = delegate?.getCardsList()?.first?.cardNumberMasked
        cardView.inputIsNotEmpty(empty: true)
        amountView.label.text = "profile_amount".localized()
        amountView.textField.keyboardType = .numberPad
    }
    
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: Any) {
        delegate?.cancelAction()
    }
    
    @IBAction func withdrawAction(_ sender: Any) {
        let bankCardHash = delegate?.getCardsList()?.filter({ $0.cardNumberMasked == cardView.textField.text }).first?.hash ?? ""
        let amount = Int((Double(self.amountView.textField.text ?? "") ?? 0.0) * 100)
        delegate?.withdraw(amount: amount, bankCardHash: bankCardHash)
    }
}

// MARK: - Text Fields

extension WithdrawView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let card = self.cardView.textField.text, !card.isEmpty,
            let amount = self.amountView.textField.text, !amount.isEmpty
        else
        {
            self.withdrawButton.isEnabled = false
            self.withdrawButton.alpha = 0.3
            return
        }
        self.withdrawButton.isEnabled = true
        self.withdrawButton.alpha = 1
    }
}

