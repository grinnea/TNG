//
//  LaunchView.swift
//  tip-n-go
//
//  Created by developer02 on 06.07.2021.
//

import UIKit

@IBDesignable
final class LaunchView: XibView {
    
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    
    // MARK: - Methods
    
    func prepareView() {
        introLabel.text = "launch_cashless_tipping".localized()
        introLabel.font = UIFont.systemFont(ofSize: 20)
        versionLabel.text = EnvironmentHelper.version
        versionLabel.font = UIFont.systemFont(ofSize: 12)
    }
}
