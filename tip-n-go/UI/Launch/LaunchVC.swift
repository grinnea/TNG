//
//  LaunchViewController.swift
//  tip-n-go
//
//  Created by developer03 on 10.02.2021.
//

import UIKit

class LaunchVC: BaseVC {
    
    // MARK: Properties

    @IBOutlet var root: LaunchView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.navController = navigationController
        self.root.prepareView()
        self.checkTokens()
    }
    
    // MARK: - Methods
    

    
}
