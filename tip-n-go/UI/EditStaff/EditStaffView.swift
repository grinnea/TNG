//
//  EditStaffView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

import UIKit

@IBDesignable
final class EditStaffView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var roleView: TextFieldCustom!
    @IBOutlet weak var positionView: TextFieldCustom!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: EditStaffVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        title.text = "employee_edit_employee".localized()
        saveButton.setTitle("save".localized(), for: .normal)
        var listPicker = [String]()
        delegate?.getOrganizationRoles()?.forEach({
            listPicker.append($0.name ?? "")
        })
        roleView.preparePicker(pickerType: true, pickerData: listPicker)
        roleView.textField.resignFirstResponder()
        
        let isMe = delegate?.getUserData()?.hash == BaseVC().getUserCurrent()?.hash
        
        roleView.label.text = "employee_role".localized()
        let roleOrganization = delegate?.getUserData()?.userOrganizations?.first?.organizationRole?.name
        roleView.textField.text = roleOrganization
        if let firstNameIndex = listPicker.firstIndex(of: roleOrganization ?? "") {
            roleView.pickerCustom.selectRow(firstNameIndex, inComponent: 0, animated: true)
        }
        if isMe {
            roleView.textField.isEnabled = false
            roleView.alpha = 0.3
        }else{
            roleView.textField.isEnabled = true
            roleView.alpha = 1.0
        }
        
        roleView.inputIsNotEmpty(empty: true)
       
        positionView.label.text = "employee_position".localized()
        positionView.textField.text = delegate?.getUserData()?.userOrganizations?.first?.position
        positionView.inputIsNotEmpty(empty: true)
        
        roleView.delegate = self
        positionView.delegate = self
    }
    
    
    // MARK: - Actions
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.cancelAction()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        let roleId = delegate?.getOrganizationRoles()?.filter({
            $0.name == roleView.pickerData[roleView.pickerCustom.selectedRow(inComponent: 0)]
        }).first?.id ?? 0
        delegate?.updateUser(userHash: delegate?.getUserData()?.hash ?? "", organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", roleId: roleId, position: positionView.textField.text ?? "")
    }
    
}

extension EditStaffView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let position = self.positionView.textField.text, !position.isEmpty
        else
        {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.3
            return
        }
        self.saveButton.isEnabled = true
        self.saveButton.alpha = 1
    }
}
