//
//  EditStaffVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

protocol EditStaffVCDelegate {
    func cancelAction()
    func getUserData() -> UserCurrent?
    func getOrganizationRoles() -> Roles?
    func updateUser(userHash: String, organizationHash: String, roleId: Int, position: String)
}
