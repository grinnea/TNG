//
//  EditStaffVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

import UIKit
import PKHUD

class EditStaffVC: BaseVC, EditStaffVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: EditStaffView!
    
    var review = Review()
    var userCurrent = UserCurrent()
    var organizationRoles = Roles()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        self.hideKeyboardWhenTappedAround()
        Api.organizationAvailableRoles(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleOrganizationRoles(success:response:error:))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Methods
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func getOrganizationRoles() -> Roles? {
        return organizationRoles
    }
    
    func getUserData() -> UserCurrent? {
        return userCurrent
    }
    
    func updateUser(userHash: String, organizationHash: String, roleId: Int, position: String) {
        Api.organizationUpdateUser(userHash: userHash, organizationHash: organizationHash, roleId: roleId, position: position, self.handleGetUserByHash(success:response:error:))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        self.view.frame.origin.y = 0 - keyboardSize.height + 20
    }

    @objc func keyboardWillHide(notification: NSNotification) {
      self.view.frame.origin.y = 0
    }
    
    // MARK: - Handle
    
    func handleOrganizationRoles(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                organizationRoles = try JSONDecoder().decode(Roles.self, from: data)
                root.prepareView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetUserByHash(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                NotificationCenter.default.post(name: NSNotification.Name("userInfo"), object: try jsonDecoder.decode(UserCurrent.self, from: data))
                HUD.flash(.success, delay: 1.0)
                self.cancelAction()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
