//
//  DistributedInfoVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 25.04.2022.
//

import UIKit
import PKHUD

class DistributedInfoVC: BaseVC, DistributedInfoVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: DistributedInfoView!

    var tipDistribution = TipDistribution()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    
    override func viewWillLayoutSubviews() {
        root.prepareHeight()
        super.updateViewConstraints()
    }
    
    // MARK: - Methods
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func getTipInfo() -> TipDistribution? {
        return tipDistribution
    }
    
}
