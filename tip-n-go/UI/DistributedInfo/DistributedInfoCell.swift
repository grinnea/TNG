//
//  DistributedInfoCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 25.04.2022.
//

import UIKit

class DistributedInfoCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
