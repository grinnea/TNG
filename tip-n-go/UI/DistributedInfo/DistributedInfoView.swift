//
//  DistributedInfoView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 25.04.2022.
//

import UIKit

@IBDesignable
final class DistributedInfoView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tipsTableView: UITableView!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var dateDistributed: UILabel!
    
    @IBOutlet weak var tableheightConstraint: NSLayoutConstraint!
    
    var delegate: DistributedInfoVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        let tip = delegate?.getTipInfo()
        titleLabel.text = (tip?.user?.lastName ?? "") + " " + (tip?.user?.firstName ?? "")
        amount.text = "\(Double((tip?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (tip?.currency?.sign ?? "")
        dateDistributed.text = tip?.date?.toFullFormatWithTime()
    }
    
    func prepareTable() {
        tipsTableView.dataSource = self
        tipsTableView.delegate = self
        tipsTableView?.register(R.nib.distributedInfoCell)
        tipsTableView.tableFooterView = UIView()
    }
    
    func prepareHeight() {
        DispatchQueue.main.async {
            if self.tipsTableView.contentSize.height < 420 {
                self.tableheightConstraint?.constant = self.tipsTableView.contentSize.height
            }else{
                self.tableheightConstraint?.constant = 420
            }
        }
    }
    
    // MARK: - Action
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.cancelAction()
    }
    
}

// MARK: - Table view data source

extension DistributedInfoView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getTipInfo()?.destinations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.distributedInfoCell, for: indexPath)!
        let row = delegate?.getTipInfo()?.destinations?[indexPath.row]
        cell.nameLabel.text = (row?.user?.lastName ?? "") + " " + (row?.user?.firstName ?? "")
        cell.avatarUser.sd_setImage(with: URL(string: row?.user?.avatar ?? ""), placeholderImage: R.image.avatarDemo())
        cell.positionLabel.text = row?.user?.position
        cell.amount.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
        
        shadowView(addView: cell.background)
        return cell
    }
    
}
