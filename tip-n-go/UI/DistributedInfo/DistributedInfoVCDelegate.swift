//
//  DistributedInfoVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 25.04.2022.
//

protocol DistributedInfoVCDelegate {
    func cancelAction()
    func getTipInfo() -> TipDistribution?
}
