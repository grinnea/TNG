//
//  ReviewInfoVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 08.04.2022.
//

protocol ReviewInfoVCDelegate {
    func cancelAction()
    func getReview() -> Review?
}
