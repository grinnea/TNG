//
//  ReviewInfoVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 08.04.2022.
//

import UIKit
import PKHUD

class ReviewInfoVC: BaseVC, ReviewInfoVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: ReviewInfoView!
    
    var review = Review()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        root.prepareHeight()
        super.updateViewConstraints()
    }
    // MARK: - Methods
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func getReview() -> Review? {
        return review
    }
    
    // MARK: - Handle
    
}

