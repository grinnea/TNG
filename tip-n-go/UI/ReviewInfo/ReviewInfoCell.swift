//
//  ReviewInfoCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 14.04.2022.
//

import UIKit

class ReviewInfoCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var labelRate: UILabel!
    @IBOutlet weak var stars: UIView!
    
    var imageViews = [UIImageView]()
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func viewStars(rating: Int) {
        imageViews = (0..<5).map { i in
            UIImageView(image: i < rating ? R.image.star5() : R.image.star0())
        }
        
        
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution  = UIStackView.Distribution.equalSpacing
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing = 8.0

        for view in imageViews {
            stackView.addArrangedSubview(view)
            view.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
            view.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
        }

        stackView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        stars.addSubview(stackView)
    }
}
