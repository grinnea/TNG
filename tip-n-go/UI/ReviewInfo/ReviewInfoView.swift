//
//  ReviewInfoView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 08.04.2022.
//

import UIKit

@IBDesignable
final class ReviewInfoView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateReview: UILabel!
    @IBOutlet weak var countTip: UILabel!
    @IBOutlet weak var tipRate: UILabel!
    @IBOutlet weak var reviewRate: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var ratingTableVIew: UITableView!
    @IBOutlet weak var ratingTableConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedbackTextConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedbackTextView: UITextView!
    
    var delegate: ReviewInfoVCDelegate?
    
    // MARK: - Methods
    
    func prepareView() {
        let review = delegate?.getReview()
        titleLabel.text = review?.user == nil ? "review_review_details".localized() : (review?.user?.lastName ?? "") + " " + (review?.user?.firstName ?? "")
        feedbackLabel.text = "review_feedback".localized()
        ratingLabel.text = "review_rating".localized()
        tipRate.layer.cornerRadius = tipRate.frame.height/2
        tipRate.layer.masksToBounds = true
        dateReview.text = review?.date?.toFullFormatWithTime()
        countTip.text = "\(Double((review?.tip?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (review?.tip?.currency?.sign ?? "")
        tipRate.text = "\(Double((review?.tip?.tipRate) ?? 0).convertedFromBackendFormat(afterPoint: 1))%"
        reviewRate.text = review?.rate ?? 0 == 0 ? "" : "\(Double((review?.rate) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
        starImage.image = getStarRate(rate: review?.rate)
        feedbackTextView.text = review?.comments
        feedbackTextView.textContainer.lineFragmentPadding = 0
        feedbackTextView.textContainerInset = .zero
        if feedbackTextView.text == "" {
            feedbackLabel.isHidden = true
        }
    }
    
    func prepareTable() {
        ratingTableVIew.dataSource = self
        ratingTableVIew.delegate = self
        ratingTableVIew?.register(R.nib.reviewInfoCell)
        ratingTableVIew.tableFooterView = UIView()
    }
    
    func prepareHeight() {
        DispatchQueue.main.async {
            self.ratingTableConstraint?.constant = self.ratingTableVIew.contentSize.height
            self.feedbackTextConstraint?.constant = self.feedbackTextView.contentSize.height
        }
    }
    
    // MARK: - Actions
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.cancelAction()
    }
    
}


// MARK: - Table view data source

extension ReviewInfoView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getReview()?.metricRates?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reviewInfoCell, for: indexPath)!
        let row = delegate?.getReview()?.metricRates?[indexPath.row]
        cell.labelRate.text = row?.name
        cell.viewStars(rating: row?.rate ?? 0)
        return cell
    }

}
