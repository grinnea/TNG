//
//  XibView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit

class XibView: UIView {
    
    // MARK: Properties
    
    var content: UIView?
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: - Methods

    private func setup() {
        guard let nibView = loadViewFromNib() else { return }

        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(nibView)
        content = nibView
    }
    
    private func loadViewFromNib() -> UIView? {
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: Bundle(for: XibView.self))
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func shadowView(addView: UIView) {
        addView.layer.shadowColor = UIColor.black.cgColor
        addView.layer.shadowOffset = CGSize(width: 3, height: 3)
        addView.layer.shadowOpacity = 0.1
        addView.layer.shadowRadius = 4.0
    }
    
    func getStarRate(rate: Int?) -> UIImage? {
        switch (Double(rate ?? 0) / 100).rounded(.toNearestOrAwayFromZero) {
            case 0: return R.image.star0()
            case 1: return R.image.star1()
            case 2: return R.image.star2()
            case 3: return R.image.star3()
            case 4: return R.image.star4()
            case 5: return R.image.star5()
        default:
            return R.image.star0()
        }
    }
}
