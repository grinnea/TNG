//
//  RegistrationVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 28.03.2022.
//

import UIKit
import PKHUD

class RegistrationVC: BaseVC, RegistrationVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: RegistrationView!
    
    var countriesList = Countries()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.setTitles()
        self.hideKeyboardWhenTappedAround()
        Api.settingsGetCountries(self.handleSettingsGetCountries(success:response:error:))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        root.prepareFields()
    }
    
    // MARK: - Methods
    
    func back() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getCountriesList() -> Countries {
        return countriesList
    }
    
    func registrationCheckPhone(phone: String) {
        Api.registrationCheckPhone(phone: phone, self.handleRegistrationCheck(success:response:error:))
    }
    
    func registrationCheckEmail(email: String) {
        Api.registrationCheckEmail(email: email, self.handleRegistrationCheck(success:response:error:))
    }
    
    func registrationUser(phone: String, email: String, firstName: String, countryId: Int, code: String, password: String) {
        Api.registrationUser(phone: phone, email: email, firstName: firstName, countryId: countryId, code: code, password: password, self.handleRegistrationUser(success:response:error:))
    }
    
    
    // MARK: - Handle
    
    func handleRegistrationCheck(success: Bool, response: Data?, error: Error?) {
        if success {
                self.root.statusRegistration = true
                self.root.prepareButton(sendCode: true)
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleRegistrationUser(success: Bool, response: Data?, error: Error?) {
        if success, let response = response {
            do {
                let responseObject = try LocalStorage.fetchObject(from: response, to: JwtToken.self)
                UserDefaults.tokensReceivedTime = Date().timeIntervalSince1970
                UserDefaults.jwtAccessToken = responseObject?.accessToken
                UserDefaults.jwtAccessTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject?.accessTokenLifetime ?? 0)
                UserDefaults.jwtRefreshToken = responseObject?.refreshToken
                UserDefaults.jwtRefreshTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject?.refreshTokenLifetime ?? 0)
                Api.userGetCurrentUser(handleClientCurrent(success:response:error:))
            } catch let error {
                self.instantAlertShow("error".localized(), message:error.localizedDescription)
            }
        } else if !success, let response = response {
            UserDefaults.loginString = ""
            self.handleErrorResponse(data: response)
        }
    }
    
    func handleSettingsGetCountries(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                countriesList = try JSONDecoder().decode(Countries.self, from: data)
                root.prepareFields()
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
}
