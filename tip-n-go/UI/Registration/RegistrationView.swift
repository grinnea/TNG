//
//  RegistrationView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 28.03.2022.
//

import UIKit
import PhoneNumberKit

@IBDesignable
final class RegistrationView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var registrationButton: UIButton!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var countriesView: TextFieldCustom!
    @IBOutlet weak var firstNameView: TextFieldCustom!
    @IBOutlet weak var emailView: TextFieldCustom!
    @IBOutlet weak var passwordView: TextFieldCustom!
    @IBOutlet weak var securityEmail: TextFieldCustom!
    
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    var delegate: RegistrationVCDelegate?
    var statusRegistration = false
    var sendCodeToCheck = false
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.back()
    }
    
    @IBAction func registrationAction(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            let validCharacters = CharacterSet(charactersIn: "0123456789")
            let phone = emailView.phoneTextField.text?.components(separatedBy: validCharacters.inverted).joined()
            if !statusRegistration {
                delegate?.registrationCheckPhone(phone: phone ?? "")
            }else{
                let countryId = delegate?.getCountriesList().filter({ $0.name == countriesView.textField.text }).first?.id ?? 0
                delegate?.registrationUser(phone: phone ?? "", email: "", firstName: firstNameView.textField.text ?? "",
                                           countryId: countryId, code: passwordView.textField.text ?? "", password: "")
            }
        }else{
            if !statusRegistration {
                delegate?.registrationCheckEmail(email: emailView.textField.text ?? "")
            }else{
                let countryId = delegate?.getCountriesList().filter({ $0.name == countriesView.textField.text }).first?.id ?? 0
                delegate?.registrationUser(phone: "", email: emailView.textField.text ?? "", firstName: firstNameView.textField.text ?? "",
                                           countryId: countryId, code: securityEmail.textField.text ?? "", password: passwordView.textField.text ?? "")
            }
        }
    }
    
    @IBAction func segmentedAction(_ sender: Any) {
        prepareFields()
        prepareButton(sendCode: false)
        statusRegistration = false
    }
    
    // MARK: - Methods

    func prepareFields() {
        var listPicker = [String]()
        delegate?.getCountriesList().forEach({
            listPicker.append($0.name ?? "")
        })
        countriesView.preparePicker(pickerType: true, pickerData: listPicker)
        countriesView.textField.resignFirstResponder()
        countriesView.label.text = "registration_country".localized()
        firstNameView.label.text = "registration_first_name".localized()
        
        countriesView.delegate = self
        firstNameView.delegate = self
        emailView.delegate = self
        passwordView.delegate = self
        securityEmail.delegate = self
        
        if segmentedControl.selectedSegmentIndex == 0 {
            emailView.preparePhone(on: true)
            passwordView.label.text = "registration_security_code".localized()
            passwordView.isHidden = true
            securityEmail.isHidden = true
        }else{
            emailView.isHidden = false
            emailView.preparePhone(on: false)
            emailView.label.text = "email".localized()
            passwordView.label.text = "registration_password".localized()
            passwordView.isHidden = false
            securityEmail.label.text = "registration_security_code".localized()
            securityEmail.isHidden = true
            passwordView.textField.isSecureTextEntry = true
        }
        
    }
    
    func setTitles() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        passwordView.isHidden = true
        prepareButton(sendCode: statusRegistration)
        titleLabel.text = "login_registration".localized()
        segmentedControl.setTitle("phone".localized(), forSegmentAt: 0)
        segmentedControl.setTitle("email".localized(), forSegmentAt: 1)
    }
    
    func prepareButton(sendCode: Bool) {
        sendCodeToCheck = sendCode
        checkTextFieldsIsNotEmpty()
        if !sendCode {
            registrationButton.addViewBorder(borderColor: R.color.blue()!.cgColor, borderWith: 1, borderCornerRadius: 4)
            registrationButton.setTitle("login_registration".localized(), for: .normal)
            registrationButton.setTitleColor(R.color.blue(), for: .normal)
            registrationButton.backgroundColor = R.color.blueLite()
            self.countriesView.textField.isEnabled = true
            self.countriesView.alpha = 1
            self.firstNameView.textField.isEnabled = true
            self.firstNameView.alpha = 1
            if segmentedControl.selectedSegmentIndex == 0 {
                self.emailView.phoneTextField.isEnabled = true
                self.emailView.alpha = 1
                self.emailView.textField.keyboardType = .numberPad
                self.passwordView.textField.isEnabled = true
                self.passwordView.alpha = 1
                passwordView.textField.text = ""
                passwordView.isHidden = true
            }else{
                self.emailView.textField.isEnabled = true
                self.emailView.alpha = 1
                self.emailView.textField.keyboardType = .emailAddress
                self.passwordView.textField.isEnabled = true
                self.passwordView.alpha = 1
                passwordView.textField.text = ""
                securityEmail.isHidden = true
                self.securityEmail.textField.keyboardType = .numberPad
            }
        }else{
            registrationButton.addViewBorder(borderColor: R.color.blue()!.cgColor, borderWith: 0, borderCornerRadius: 4)
            registrationButton.setTitle("login_confirm".localized(), for: .normal)
            registrationButton.setTitleColor(R.color.white(), for: .normal)
            registrationButton.backgroundColor = R.color.blue()
            self.countriesView.textField.isEnabled = false
            self.countriesView.alpha = 0.3
            self.firstNameView.textField.isEnabled = false
            self.firstNameView.alpha = 0.3
            if segmentedControl.selectedSegmentIndex == 0 {
                self.emailView.phoneTextField.isEnabled = false
                self.emailView.alpha = 0.3
                passwordView.isHidden = false
            }else{
                self.emailView.textField.isEnabled = false
                self.emailView.alpha = 0.3
                self.passwordView.textField.isEnabled = true
                self.passwordView.alpha = 0.3
                securityEmail.isHidden = false
            }
        }
    }

}

extension RegistrationView: TextFieldCustomDelegate {
    func flagAction() {
        let phoneCountryCode = delegate?.getCountriesList().filter({ $0.name == countriesView.textField.text }).first?.phoneCountryCode
        emailView.selectFlag(phoneCountryCode: phoneCountryCode)
        emailView.phoneCountryCode = phoneCountryCode ?? 0
    }
    
    func checkTextFieldsIsNotEmpty() {
        let type = segmentedControl.selectedSegmentIndex == 0 ? true : false
        if type {
            if !sendCodeToCheck {
                guard
                    let role = self.countriesView.textField.text, !role.isEmpty,
                    let lastName = self.firstNameView.textField.text, !lastName.isEmpty,
                    let firstName = self.emailView.phoneTextField.text, !firstName.isEmpty
                else
                {
                    self.registrationButton.isEnabled = false
                    self.registrationButton.alpha = 0.3
                    return
                }
                self.registrationButton.isEnabled = true
                self.registrationButton.alpha = 1
            }else{
                guard
                    let pass = self.passwordView.textField.text, !pass.isEmpty
                else
                {
                    self.registrationButton.isEnabled = false
                    self.registrationButton.alpha = 0.3
                    return
                }
                self.registrationButton.isEnabled = true
                self.registrationButton.alpha = 1
            }
        }else{
            if !sendCodeToCheck {
                guard
                    let role = self.countriesView.textField.text, !role.isEmpty,
                    let lastName = self.firstNameView.textField.text, !lastName.isEmpty,
                    let firstName = self.emailView.textField.text, !firstName.isEmpty,
                    let pass = self.passwordView.textField.text, !pass.isEmpty
                else
                {
                    self.registrationButton.isEnabled = false
                    self.registrationButton.alpha = 0.3
                    return
                }
                self.registrationButton.isEnabled = true
                self.registrationButton.alpha = 1
            }else{
                guard
                    let code = self.securityEmail.textField.text, !code.isEmpty
                else
                {
                    self.registrationButton.isEnabled = false
                    self.registrationButton.alpha = 0.3
                    return
                }
                self.registrationButton.isEnabled = true
                self.registrationButton.alpha = 1
            }
        }
    }
}
