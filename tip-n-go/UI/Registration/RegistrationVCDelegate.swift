//
//  RegistrationVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 28.03.2022.
//

protocol RegistrationVCDelegate {
    func back()
    func getCountriesList() -> Countries
    func registrationCheckPhone(phone: String)
    func registrationCheckEmail(email: String)
    func registrationUser(phone: String, email: String, firstName: String, countryId: Int, code: String, password: String)
}
