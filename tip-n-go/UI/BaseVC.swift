//
//  BaseViewController.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit
import PKHUD
import Alamofire

class BaseVC: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Properties

    var navController: UINavigationController?

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PKHUD.sharedHUD.gracePeriod = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerToNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Notifications
    
    func registerToNotifications() {
        self.registerForNotification(
            name: UIApplication.willEnterForegroundNotification.rawValue,
            selector: #selector(self.applicationDidLaunch)
        )
        self.registerForNotification(
            name: Notification.Name("appDidRecieveNotificationInForeground").rawValue,
            selector: #selector(self.checkIfRouteIsPending)
        )
    }
    
    @objc func checkIfRouteIsPending() {
        openVCFullScreen(vc: R.storyboard.main.notificationsVC())
    }
    
    func registerForNotification(name: String, selector: Selector) {
        NotificationCenter.default.addObserver(
            self,
            selector: selector,
            name: NSNotification.Name(name),
            object: nil
        )
    }
    
    func presentStartVC() {
        let vc = R.storyboard.main.loginVC()!
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .fullScreen
        navController?.pushViewController(vc, animated: true)
    }
    
    func catchError(error: Error) {
        self.instantAlertShow(
            "error".localized(),
            message: error.localizedDescription
        )
    }

    func getUserCurrent() -> UserCurrent? {
        let jsonDecoder = JSONDecoder()
        do {
            let responseObject = try jsonDecoder.decode(UserCurrent.self, from: UserDefaults.currentUser!)
            return responseObject
        } catch let error {
            self.catchError(error: error)
        }
        return nil
    }
    
    func handleErrorResponse(data: Data) {
        let stringError = String(data: data, encoding: String.Encoding.utf8)
        if stringError!.firstIndex(of: "{") != nil || stringError!.lastIndex(of: "}") != nil ||
            stringError!.firstIndex(of: "[") != nil || stringError!.lastIndex(of: "]") != nil {
            do {
                let startCurve = stringError!.firstIndex(of: "{")
                let endCurve = stringError!.lastIndex(of: "}")
                let startSquare = stringError!.firstIndex(of: "[")
                let endSquare = stringError!.lastIndex(of: "]")
                let startIndex = min(startCurve ?? stringError!.endIndex, startSquare ?? stringError!.endIndex)
                let endIndex = max(endCurve ?? stringError!.startIndex, endSquare ?? stringError!.startIndex)
                let newString = String(stringError![startIndex...endIndex])
                let newData = Data(newString.utf8)
                let responseObject = try JSONDecoder().decode(ServerError.self, from: newData)
                self.instantAlertShow("Error", message: responseObject.error?.localizedError ?? responseObject.text)
            } catch let error {
                self.catchError(error: error)
            }
        }else{
            let alert = UIAlertController(
                title: "error".localized(),
                message: "service_unavailable".localized(), preferredStyle: .alert)
            let okAction = UIAlertAction(
                title: "start_exit".localized(),
                style: .destructive,
                handler: {_ in exit(0)}
            )
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        HUD.hide()
    }

    func appBackgroundOut() {
        if UserDefaults.fcmNotificationTapAction == "openNotification" && (!(UIApplication.topViewController() is LaunchVC) || !(UIApplication.topViewController() is LoginVC)) {
            UserDefaults.fcmNotificationTapAction = ""
            checkIfRouteIsPending()
        }
    }
    
    // MARK: - Entering Foreground & Tokens LC
    
    @objc func applicationDidLaunch() {
        self.checkTokens()
    }
    
    func checkTokens() {
        let nowTimeStamp = Date().timeIntervalSince1970
        
        let jwtAccessTokenTimeToLife = UserDefaults.jwtAccessTokenTimeToLife
        let jwtAccessTokenAlive = jwtAccessTokenTimeToLife ?? 0.0 >= nowTimeStamp
        
        let jwtRefreshTokenTimeToLife = UserDefaults.jwtRefreshTokenTimeToLife
        let jwtRefreshTokenAlive = jwtRefreshTokenTimeToLife ?? 0.0 >= nowTimeStamp
        
        if !jwtAccessTokenAlive && !jwtRefreshTokenAlive && UIApplication.topViewController() is LaunchVC {
            UserDefaults.clientDefaultOrganizationHash = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
            Api.removeFcmToken(completion: { success, data in
                if success {
                    UserDefaults.flushClientData()
                }
            })
            self.openVC(vc: R.storyboard.main.loginVC())
        } else {
            HUD.show(.progress)
            Api.userGetCurrentUser(self.handleClientCurrent(success:response:error:))
        }
    }
    
    func handleClientCurrent(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            UserDefaults.currentUser = data
            HUD.hide()
            if UIApplication.topViewController() is LaunchVC {
                let vc = R.storyboard.main.tabBarVC()
                vc?.modalPresentationStyle = .fullScreen
                if let vc = vc { self.present(vc, animated: true, completion: nil) }
            }else{
                self.appBackgroundOut()
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func openOrganization() {
        openVC(vc: R.storyboard.main.organizationVC())
    }
    
    func openVC(vc: UIViewController?) {
        if let vc = vc { self.present(vc, animated: true, completion: nil) }
    }
    
    func openVCFullScreen(vc: UIViewController?) {
        vc?.modalPresentationStyle = .fullScreen
        if let vc = vc { self.present(vc, animated: true, completion: nil) }
    }
}
