//
//  TextFieldCustom.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

import UIKit
import PhoneNumberKit

@objc protocol TextFieldCustomDelegate: AnyObject {
    @objc optional func flagAction()
    @objc optional func checkTextFieldsIsNotEmpty()
}

@IBDesignable
class TextFieldCustom: UIView {
    
    weak var delegate: TextFieldCustomDelegate?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    @IBOutlet weak var labelYConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldYConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldLeftConstraint: NSLayoutConstraint!
    
    var pickerType = false
    var isNoEmpty = false
    var pickerData = [String]()
    var phoneCountryCode = Int()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
        
    // MARK: - Lifecycle & Init
    
    private func commonInit() {
        let bundle = Bundle(for: TextFieldCustom.self)
        bundle.loadNibNamed("TextFieldCustom", owner: self, options: nil)
        addSubview(contentView)
        phoneTextField.isHidden = true
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.textField.delegate = self
        self.phoneTextField.delegate = self
        self.phoneTextField.withFlag = true
        self.phoneTextField.withPrefix = true
        self.phoneTextField.withDefaultPickerUI = false
        self.phoneTextField.withExamplePlaceholder = true
        self.phoneTextField.maxDigits = 12
        setBorderView()
        phoneTextField.addTarget(self, action: #selector(phoneTextFieldsIsNotEmpty), for: .editingChanged)
        textField.addTarget(self, action: #selector(textFieldsIsNotEmpty), for: .editingChanged)
    }
    
    @objc func phoneTextFieldsIsNotEmpty(sender: UITextField) {
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        delegate?.checkTextFieldsIsNotEmpty?()
    }
    
    @objc func textFieldsIsNotEmpty(sender: UITextField) {
        delegate?.checkTextFieldsIsNotEmpty?()
    }
    
    func preparePhone(on: Bool) {
        delegate?.flagAction?()
        if on {
            phoneTextField.isHidden = false
            textField.isHidden = true
            label.isHidden = true
        }else{
            phoneTextField.isHidden = true
            textField.isHidden = false
            label.isHidden = false
        }
    }
    
    func preparePicker(pickerType: Bool, pickerData: [String]) {
        if pickerType { self.pickerCustom.delegate = self
            self.pickerCustom.dataSource = self
            self.textField.inputView = self.pickerCustom
            self.textField.inputAccessoryView = self.pickerToolbar
            self.pickerType = pickerType
            self.pickerData = pickerData
        }else{
            self.textField.inputView = nil
            self.textField.inputAccessoryView = nil
            self.textField.reloadInputViews()
        }
    }
    
    func setBorderView() {
        background.layer.borderWidth = 1.0
        background.layer.cornerRadius = 4
        background.layer.borderColor = R.color.blue()?.cgColor
        background.layer.masksToBounds = true
    }
    
    func inputIsNotEmpty(empty: Bool) {
        if empty {
            labelYConstraint.constant = -15
            textFieldYConstraint.constant = 10
            leftLabelConstraint.constant = 2
            performAnimation(transform: CGAffineTransform(scaleX: 0.8, y: 0.8))
        }else{
            labelYConstraint.constant = 0
            textFieldYConstraint.constant = 0
            leftLabelConstraint.constant = 10
            performAnimation(transform: CGAffineTransform(scaleX: 1, y: 1))
        }
    }
    
    func selectFlag(phoneCountryCode: Int?) {
        phoneTextField.withExamplePlaceholder = false
        phoneTextField.withFlag = false
        phoneTextField.text = "+" + String(phoneCountryCode ?? 0)
        phoneTextField.withFlag = true
        phoneTextField.withExamplePlaceholder = true
        phoneTextField.text = ""
    }
    
    // MARK: - Pickers
    
    var pickerCustom: UIPickerView = { return $0 }(UIPickerView())
    private let pickerToolbar: UIToolbar = {
        let cancelButton = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: nil,
            action: #selector(otherDataPickerDoneButtonPressed)
        )
        let flexibleSpace = UIBarButtonItem(
            barButtonSystemItem: .flexibleSpace,
            target: nil,
            action: nil
        )
        
        $0.setItems([flexibleSpace, cancelButton], animated: true)
        $0.barStyle = .default
        $0.isTranslucent = true
        $0.sizeToFit()
        
        return $0
    }(UIToolbar())
    
    @objc private func otherDataPickerDoneButtonPressed() {
        if self.textField.isFirstResponder {
            self.textField.resignFirstResponder()
        }
    }
}
    
// MARK: - Text Fields

extension TextFieldCustom: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if pickerType && textField.text == "" {
            textField.text = pickerData.first
            delegate?.flagAction?()
        }else if textField == self.phoneTextField && self.phoneTextField.text == "" {
            if phoneCountryCode != 0 {
                phoneTextField.text = "+" + String(phoneCountryCode)
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        labelYConstraint.constant = -15
        textFieldYConstraint.constant = 10
        leftLabelConstraint.constant = 2
        performAnimation(transform: CGAffineTransform(scaleX: 0.8, y: 0.8))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty && textField != self.phoneTextField {
            labelYConstraint.constant = 0
            textFieldYConstraint.constant = 0
            leftLabelConstraint.constant = 10
            performAnimation(transform: CGAffineTransform(scaleX: 1, y: 1))
        }else if textField == self.phoneTextField && self.phoneTextField.text == "" {
            selectFlag(phoneCountryCode: phoneCountryCode)
        }
    }
    
    fileprivate func performAnimation(transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.label.transform = transform
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
}


// MARK: - Picker View

extension TextFieldCustom: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType && textField.isFirstResponder {
            textField.text = pickerData[row]
            delegate?.flagAction?()
            delegate?.checkTextFieldsIsNotEmpty?()
        }
    }
}

extension TextFieldCustom: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}
