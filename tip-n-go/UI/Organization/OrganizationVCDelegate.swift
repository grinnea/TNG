//
//  OrganizationVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

protocol OrganizationVCDelegate {
    func goToPrifile()
    func openWithdraw()
    func openSettings()
    func openTipsToShareVC()
    func open(section: Int, row: Int)
    func getUser() -> UserCurrent?
    func getOrganization() -> Organization?
    func getReportsData() -> ReportsData?
    func getRoleToShow() -> Bool
    func getReports(period: String)
    func getPeriodReport(period: String, type: Bool)
}
