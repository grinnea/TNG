//
//  OrganizationVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit
import PKHUD
import SwiftyJSON

class OrganizationVC: BaseVC, OrganizationVCDelegate {
    
    // MARK: Properties
    
    @IBOutlet private weak var root: OrganizationView!
    
    var userCurrent = UserCurrent()
    var reportsData = ReportsData()
    var way = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareShadow()
        root.prepareTable()
        root.collectionViewSetup(viewCollection: root.chartTipSelectCollection)
        root.collectionViewSetup(viewCollection: root.chartReviewSelectCollection)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        getUserOrganizations()
        self.root.chartTipSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        self.root.chartReviewSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        let swipeToLeft = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        let swipeToRight = UISwipeGestureRecognizer(target: self, action: #selector(changePageOnSwipe(_:)))
        swipeToLeft.direction = .right
        swipeToRight.direction = .left
        root.chartReviewsView.addGestureRecognizer(swipeToLeft)
        root.chartReviewsView.addGestureRecognizer(swipeToRight)
        appBackgroundOut()
        NotificationCenter.default.addObserver(self, selector: #selector(reNewView), name: NSNotification.Name(rawValue:  "organizationHash"), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        root.sizeHeaderToFit()
    }
    
    // MARK: - Methods
    
    func getOrganization() -> Organization? {
        if UserDefaults.clientDefaultOrganizationHash == nil {
            return self.userCurrent.userOrganizations?.first?.organization
        }else{
            return userCurrent.userOrganizations?.first(where: {$0.organization?.hash == UserDefaults.clientDefaultOrganizationHash })?.organization
        }
    }
    
    func getRoleToShow() -> Bool {
        if getUserCurrent()?.userOrganizations?.count == 0 {
            return true
        }else{
            return getUserCurrent()?.userOrganizations?.filter({ $0.organizationRole?.id == 3 }).count != 0
        }
    }
    
    func getApiBalanceByOrganization() {
        if getUserCurrent()?.userOrganizations?.count == 0 {
            Api.financeBalance(self.handleGetFinanceBalance(success:response:error:))
        }else{
            Api.financeBalanceByOrganization(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetFinanceBalance(success:response:error:))
        }
    }
    
    func getUserOrganizations() {
        Api.userGetCurrentUser(self.handleRenewClientCurrent(success:response:error:))
    }
    
    func getReportsData() -> ReportsData? {
        return reportsData
    }
    
    func getReports(period: String) {
        var charts = [[String:String]]()
        if getRoleToShow() {
            if getUserCurrent()?.userOrganizations?.count == 0 {
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentTips"])
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentReviews"])
            }else{
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserTips"])
                charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserReviews"])
            }
        }else{
            charts.append(["userHash": "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "OrganizationTips"])
            charts.append(["userHash": "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "OrganizationReviews"])
        }
        Api.getReportsData(charts: charts, self.handleGetReportsData(success:response:error:))
    }
    
    func getPeriodReport(period: String, type: Bool) {
        var charts = [[String:String]]()
        if getRoleToShow() {
            if getUserCurrent()?.userOrganizations?.count == 0 {
                if type {
                    charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentTips"])
                    Api.getReportsData(charts: charts, self.handleGetReportsDataTip(success:response:error:))
                }else{
                    charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": "", "period": period, "chartType": "CurrentReviews"])
                    Api.getReportsData(charts: charts, self.handleGetReportsDataReview(success:response:error:))
                }
            }else{
                if type {
                    charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserTips"])
                    Api.getReportsData(charts: charts, self.handleGetReportsDataTip(success:response:error:))
                }else{
                    charts.append(["userHash": userCurrent.hash ?? "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "UserReviews"])
                    Api.getReportsData(charts: charts, self.handleGetReportsDataReview(success:response:error:))
                }
            }
        }else{
            if type {
                charts.append(["userHash": "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "OrganizationTips"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataTip(success:response:error:))
            }else{
                charts.append(["userHash": "", "organizationHash": UserDefaults.clientDefaultOrganizationHash ?? "", "period": period, "chartType": "OrganizationReviews"])
                Api.getReportsData(charts: charts, self.handleGetReportsDataReview(success:response:error:))
            }
        }
    }
    
    func getUser() -> UserCurrent? {
        return userCurrent
    }
    
    func goToPrifile() {
        self.openVC(vc: R.storyboard.main.profileVC())
    }
    
    func openWithdraw() {
        self.openVC(vc: R.storyboard.main.withdrawVC())
    }
    
    func openTipsToShareVC() {
        self.openVC(vc: R.storyboard.main.distributeVC())
    }
    
    func openSettings() {
        let vc = R.storyboard.main.addOrganizationVC()
        vc?.editOrganization = true
        self.openVC(vc: vc)
    }
    
    @objc func reNewView() {
        root.headerView.transition(vc: self.root.tableView)
        getApiBalanceByOrganization()
        getReports(period: "month".localized())
        self.root.chartTipSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        self.root.chartReviewSelectCollection.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        root.prepareView()
    }

    func open(section: Int, row: Int) {
        if row == 0 {
            let vc = R.storyboard.main.historyDistributedVC()!
            self.navigationController?.pushViewController(vc, animated: true)
        } else if row == 1 {
            let vc = R.storyboard.main.staffVC()!
            self.navigationController?.pushViewController(vc, animated: true)
        } else if row == 2 {
            let vc = R.storyboard.main.organizationSettingsVC()!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func changePageOnSwipe(_ gesture: UISwipeGestureRecognizer) {
        let numPage = getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
        if gesture.direction == .left && root.chartReviewPageControl.currentPage < numPage - 1 {
            root.chartReviewPageControl.currentPage += 1
            way = false
            self.refreshSwipe()
        } else if gesture.direction == .right && root.chartReviewPageControl.currentPage > 0{
            root.chartReviewPageControl.currentPage -= 1
            way = true
            self.refreshSwipe()
        }
    }
    
    func refreshSwipe() {
        DispatchQueue.main.async {
            self.root.prepareChartView()
            self.root.transition(vc: self.root.chartReviewViewBackground, way: self.way)
        }
    }
    
    // MARK: - Handle
    
    func handleGetFinanceBalance(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                let balance = try jsonDecoder.decode(Balance.self, from: data)
                root.prepareBalance(balance: balance)
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleRenewClientCurrent(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                UserDefaults.currentUser = data
                self.userCurrent = try jsonDecoder.decode(UserCurrent.self, from: data)
                if UserDefaults.clientDefaultOrganizationHash == nil {
                    UserDefaults.clientDefaultOrganizationHash = self.userCurrent.userOrganizations?.first?.organization?.hash ?? ""
                }
                getApiBalanceByOrganization()
                getReports(period: "month".localized())
                root.prepareView()
                root.tableView.reloadData()
                root.headerView.preparePageControl()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsData(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
                root.prepareChartPie(metric: 0)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataTip(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartTipViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartLine()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetReportsDataReview(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                reportsData = try jsonDecoder.decode(ReportsData.self, from: data)
                for view in self.root.chartReviewViewBackground.subviews {
                    view.removeFromSuperview()
                }
                root.prepareChartPie(metric: root.chartReviewPageControl.currentPage)
                root.preparePageControl()
                root.prepareChartView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
}
