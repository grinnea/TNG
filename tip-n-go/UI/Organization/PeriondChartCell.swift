//
//  PeriondChartCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 26.04.2022.
//

import UIKit

class PeriondChartCell: UICollectionViewCell {

    @IBOutlet weak var background: UIView!
    @IBOutlet weak var title: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                UIView.animate(withDuration: 0.3, animations: {
                    self.title.textColor = R.color.white()
                    self.layer.backgroundColor = R.color.blue()?.cgColor
                    self.layer.borderWidth = 0
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.title.textColor = R.color.gray()
                    self.layer.backgroundColor = R.color.white()?.cgColor
                    self.layer.borderWidth = 1
                })
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = R.color.gray()?.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 12
    }
    
}
