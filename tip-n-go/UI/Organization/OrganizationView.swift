//
//  OrganizationView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit
import SDWebImage
import Highcharts

@IBDesignable
final class OrganizationView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var chartTipsView: UIView!
    @IBOutlet weak var chartReviewsView: UIView!
    @IBOutlet weak var headerView: NavBarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chartTipSelectCollection: UICollectionView!
    @IBOutlet weak var chartTipViewBackground: UIView!
    @IBOutlet weak var chartReviewSelectCollection: UICollectionView!
    @IBOutlet weak var chartReviewPageControl: UIPageControl!
    @IBOutlet weak var chartReviewViewBackground: UIView!
    
    
    // MARK: - Organization View
    @IBOutlet var organizationInfoView: UIView!
    @IBOutlet weak var organizationNameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var avatarOrganization: UIImageView!
    @IBOutlet weak var phone_title: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var site_title: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    @IBOutlet weak var address_title: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var email_title: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var heightOrganizationConstraint: NSLayoutConstraint!
    @IBOutlet weak var topOrganizationConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightBalanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var topBalanceConctraint: NSLayoutConstraint!
    
    // MARK: - Organization Balence
    @IBOutlet weak var balance_title: UILabel!
    @IBOutlet weak var balanceOrganizationLabel: UILabel!
    @IBOutlet weak var withdraw: UIButton!
    
    var delegate: OrganizationVCDelegate?
    var chartViewLine: HIChartView!
    var chartViewPie: HIChartView!
    let listChartPeriod = ["day".localized(), "week".localized(), "month".localized(), "year".localized()]
    
    // MARK: - Methods
    
    func prepareView() {
        balance_title.text = "organizations_balance".localized()
        phone_title.text = "phone".localized()
        site_title.text = "website".localized()
        address_title.text = "address".localized()
        email_title.text = "email".localized()
        
        if delegate?.getRoleToShow() ?? true {
            if BaseVC().getUserCurrent()?.userOrganizations?.count == 0 {
                organizationInfoView.isHidden = true
                heightOrganizationConstraint.constant = 0
                topOrganizationConstraint.constant = 0
                withdraw.setTitle("profile_withdraw".localized(), for: .normal)
            }else{
                organizationInfoView.isHidden = true
                balanceView.isHidden = true
                heightOrganizationConstraint.constant = 0
                topOrganizationConstraint.constant = 0
                heightBalanceConstraint.constant = 0
                topBalanceConctraint.constant = 0
            }
            
        } else {
            let userInfo = delegate?.getOrganization()
            organizationNameLabel.text = userInfo?.name
            brandLabel.text = userInfo?.brand?.name
            avatarOrganization.sd_setImage(with: URL(string: userInfo?.brand?.logo ?? ""), placeholderImage: R.image.avatarNoImage())
            phoneLabel.text = userInfo?.phone
            siteLabel.text = userInfo?.website
            adressLabel.text = userInfo?.address
            emailLabel.text = userInfo?.email
            withdraw.setTitle("organizations_distribute".localized(), for: .normal)
        }
    }
    
    func prepareBalance(balance: Balance?) {
        let userInfo = delegate?.getOrganization()
        if balance?.currency?.sign == "" {
            balanceOrganizationLabel.text = "0"
            balanceOrganizationLabel.textColor = .red
        }else{
            balanceOrganizationLabel.textColor = .black
            balanceOrganizationLabel.text = "\(Double((balance?.balance) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (balance?.currency?.sign ?? "")
        }
        withdraw.isHidden = balance?.balance == 0
        withdraw.isEnabled = true
        withdraw.setTitleColor(R.color.blue(), for: .normal)
        if userInfo?.tipDistributionMethodId == 1 {
            withdraw.isHidden = true
        }else if userInfo?.tipDistributionMethodId == 2 && balance?.balance == 0 {
            withdraw.isHidden = false
            withdraw.isEnabled = false
            withdraw.setTitleColor(R.color.gray(), for: .normal)
        }
    }
    
    func prepareTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView?.register(R.nib.profileCell)
        tableView.tableFooterView = UIView()
    }
    
    func prepareShadow() {
        shadowView(addView: organizationInfoView)
        shadowView(addView: balanceView)
        shadowView(addView: chartTipsView)
        shadowView(addView: chartReviewsView)
        shadowView(addView: headerView)
    }
    
    func collectionViewSetup(viewCollection: UICollectionView) {
        viewCollection.showsHorizontalScrollIndicator = false
        viewCollection.bounces = false
        viewCollection.register(UINib(nibName: R.nib.periondChartCell.identifier, bundle: nil), forCellWithReuseIdentifier: R.nib.periondChartCell.identifier)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 70, height: 24)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        viewCollection.collectionViewLayout = flowLayout
        viewCollection.dataSource = self
        viewCollection.delegate = self
    }
    
    func preparePageControl() {
        chartReviewPageControl.numberOfPages = delegate?.getReportsData()?.filter({ $0.type == "pie" }).first?.series?.count ?? 0
    }
    
    func prepareChartView() {
        for view in self.chartReviewViewBackground.subviews {
            view.removeFromSuperview()
        }
       prepareChartPie(metric: chartReviewPageControl.currentPage)
    }
    
    func transition(vc: UIView, way: Bool) {
        self.transitionView(vc: vc, duration: 0.3, type: way ? .fromLeft : .fromRight)
    }
    
    func prepareChartLine() {
        chartViewLine = HIChartView(frame: CGRect(x: chartTipViewBackground.bounds.origin.x,
                                              y: chartTipViewBackground.bounds.origin.y + 8,
                                              width: chartTipViewBackground.bounds.size.width - 8,
                                              height: chartTipViewBackground.bounds.size.height))
        let arrColumn = delegate?.getReportsData()?.filter({ $0.type == "column" })
        let currency = delegate?.getOrganization()?.currency?.sign ?? ""
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrColumn?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrColumn?.first?.title?.text
        options.title = title
        
        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.category + '</b>: ' + this.point.y?.toFixed(2) + ' ' + '\(currency)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        
        let xAxis = HIXAxis()
        xAxis.categories = arrColumn?.first?.xAxis?.categories
        options.xAxis = [xAxis]
        
        let yAxis = HIYAxis()
        yAxis.title = HITitle()
        yAxis.title.text = arrColumn?.first?.yAxis?.title?.text
        options.yAxis = [yAxis]
        
        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        let series = HIColumn()
        series.data = arrColumn?.first?.series?.first!.dataLine
        series.name = arrColumn?.first?.series?.first?.name
        options.series = [series]
        
        chartViewLine.options = options
        chartTipViewBackground.addSubview(chartViewLine)
    }
    
    func prepareChartPie(metric: Int) {
        chartViewPie = HIChartView(frame: CGRect(x: chartReviewViewBackground.bounds.origin.x,
                                              y: chartReviewViewBackground.bounds.origin.y + 8,
                                              width: chartReviewViewBackground.bounds.size.width - 8,
                                              height: chartReviewViewBackground.bounds.size.height))
        chartViewPie.plugins = ["no-data-to-display"]
        let arrPie = delegate?.getReportsData()?.filter({ $0.type == "pie" })
        
        let options = HIOptions()
        
        let chart = HIChart()
        chart.type = arrPie?.first?.type
        options.chart = chart
        
        let title = HITitle()
        title.text = arrPie?.first?.title?.text
        options.title = title

        let subtitle = HISubtitle()
        subtitle.text = arrPie?.first?.series?[metric].name
        subtitle.style = HICSSObject()
        subtitle.style.fontSize = "16px"
        options.subtitle = subtitle

        let tooltip = HITooltip.init()
        let fcStr = "function () { return '<b>' + this.point.name + '</b>: ' + this.point.y?.toFixed() + ' review(s) (' + this.point.percentage?.toFixed() + '%)';}"
        tooltip.formatter = HIFunction.init(jsFunction: fcStr)
        options.tooltip = tooltip
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting

        let credits = HICredits()
        credits.enabled = false
        options.credits = credits
        
        var arrData = [Any]()
        arrPie?.first?.series?[metric].dataPie?.forEach({
            arrData.append(["name": $0.name ?? "", "y": $0.y ?? 0.0, "color": $0.color ?? ""])
        })
        
        let series = HIPie()
        series.data = arrData
        
        options.series = [series]
        
        chartViewPie.options = options
        chartReviewViewBackground.addSubview(chartViewPie)
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = chartReviewsView.frame.origin.y + chartReviewsView.frame.height + 16
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        tableView.tableHeaderView = headerView
    }
    
    // MARK: - Actions
    @IBAction func withdrawAction(_ sender: Any) {
        if delegate?.getRoleToShow() ?? true {
            delegate?.openWithdraw()
        }else{
            delegate?.openTipsToShareVC()
        }
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        delegate?.openSettings()
    }
    
    @IBAction func chartPageACtion(_ sender: Any) {
        prepareChartView()
    }
    
}

// MARK: - Table view data source

extension OrganizationView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeader section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let userInfo = delegate?.getOrganization()
        if delegate?.getRoleToShow() ?? true {
            return 0
        } else if indexPath.row == 0 && userInfo?.tipDistributionMethodId == 1 {
            return 0
        } else {
            return 92
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.profileCell, for: indexPath)
        else { return UITableViewCell() }
        let userInfo = delegate?.getOrganization()
        let row = indexPath.row
        if row == 0 {
            cell.menuTitle.text = "organizations_distributed_tips".localized()
            cell.menuInfo.text = "organizations_the_history_of_tips_distributed_to_staff".localized()
            cell.menuIcon.image = R.image.history()
            cell.isHidden = userInfo?.tipDistributionMethodId == 1
        }  else if row == 1 {
            cell.menuTitle.text = "organizations_staff".localized()
            cell.menuInfo.text = "employee_list_of_employees".localized()
            cell.menuIcon.image = R.image.staffs()
        } else if row == 2 {
            cell.menuTitle.text = "employee_settings_and_notifications".localized()
            cell.menuInfo.text = "employee_profile_settings_and_notifications".localized()
            cell.menuIcon.image = R.image.settings()
        }
        shadowView(addView: cell.viewBackground)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.open(section: indexPath.section, row: indexPath.row)
    }

}

// MARK: - Collection view data source

extension OrganizationView: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listChartPeriod.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)!
        cell.title.text = listChartPeriod[indexPath.row]
        if cell.isSelected {
                cell.backgroundColor = R.color.blue()
            }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.getPeriodReport(period: listChartPeriod[indexPath.row], type: collectionView == chartTipSelectCollection)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.periondChartCell, for: indexPath)
        cell?.isSelected = false
    }
    
}
