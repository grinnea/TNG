//
//  ReviewsVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

protocol ReviewsVCDelegate {
    func getUserOrganizations() -> UserOrganizations?
    func getUserReviews() -> Reviews?
    func openReviewInfo(review: Review)
}
