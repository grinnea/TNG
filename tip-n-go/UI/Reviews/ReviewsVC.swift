//
//  ReviewsVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

import UIKit
import PKHUD

class ReviewsVC: BaseVC, ReviewsVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: ReviewsView!

    var reviews = Reviews()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiReviews()
        root.headerView.preparePageControl()
        NotificationCenter.default.addObserver(self, selector: #selector(reNewView), name: NSNotification.Name(rawValue:  "organizationHash"), object: nil)
    }
    
    
    // MARK: - Methods
    
    @objc func reNewView() {
        root.headerView.transition(vc: self.root.reviewsTableView)
        root.headerView.transition(vc: self.root.emptyView)
        root.headerView.transition(vc: self.root.emptyTitle)
        root.headerView.transition(vc: self.root.emptyText)
        getApiReviews()
        root.reviewsTableView.reloadData()
        root.prepareEmptyView()
    }
    
    func getUserOrganizations() -> UserOrganizations? {
        return getUserCurrent()?.userOrganizations
    }
    
    func getUserReviews() -> Reviews? {
        return reviews
    }
    
    func openReviewInfo(review: Review) {
        let vc = R.storyboard.main.reviewInfoVC()
        vc?.review = review
        openVC(vc: vc)
    }
    
    func getRoleToShow() -> Bool {
        if getUserCurrent()?.userOrganizations?.count == 0 {
            return true
        }else{
            return getUserCurrent()?.userOrganizations?.filter({ $0.organizationRole?.id == 3 }).count != 0
        }
    }
    
    func getApiReviews() {
        if getRoleToShow() {
            if getUserOrganizations()?.count == 0 {
                Api.userReviews(self.handleGetUserReviews(success:response:error:))
            }else{
                Api.reviewsByHashAndOrganizationHash(userHash: BaseVC().getUserCurrent()?.hash ?? "", organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserReviews(success:response:error:))
            }
        }else{
            Api.userReviewsByOrganization(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserReviews(success:response:error:))
        }
    }
        // MARK: - Handle
        
        func handleGetUserReviews(success: Bool, response: Data?, error: Error?) {
                if success, let data = response {
                    let jsonDecoder = JSONDecoder()
                    do {
                        reviews = try jsonDecoder.decode(Reviews.self, from: data)
                        self.root.reviewsTableView.reloadData()
                        self.root.prepareEmptyView()
                    } catch let error {
                        self.catchError(error: error)
                    }
                } else if let data = response {
                    self.handleErrorResponse(data: data)
                }
        }
}
