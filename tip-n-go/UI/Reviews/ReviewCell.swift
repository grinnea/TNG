//
//  ReviewCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelSum: UILabel!
    @IBOutlet weak var reviewInfoLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var background: UIView!
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reviewInfoLabel.text = "review_read_the_review".localized()
    }
}
