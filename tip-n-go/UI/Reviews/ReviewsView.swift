//
//  ReviewsView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 29.03.2022.
//

import UIKit

@IBDesignable
final class ReviewsView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var headerView: NavBarView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var reviewsTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: ReviewsVCDelegate?
    
    
    // MARK: - Methods
    
    func prepareView() {
        title.text = "review_reviews".localized()
        emptyTitle.text = "review_empty_title".localized()
        emptyText.text = "review_empty_text".localized()
        shadowView(addView: headerView)
        shadowView(addView: emptyView)
    }
    
    func prepareTable() {
        reviewsTableView.dataSource = self
        reviewsTableView.delegate = self
        reviewsTableView?.register(R.nib.reviewCell)
        reviewsTableView.tableFooterView = UIView()
    }
    
    func prepareEmptyView() {
        if delegate?.getUserReviews()?.count == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
        }
    }
}

// MARK: - Table view data source

extension ReviewsView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getUserReviews()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reviewCell, for: indexPath)!
        let row = delegate?.getUserReviews()?[indexPath.row]
        cell.labelName.text = delegate?.getUserOrganizations()?.count == 0 ? "" : (row?.user?.lastName ?? "") + " " + (row?.user?.firstName ?? "")
        cell.labelDate.text = row?.date?.toFullFormatWithTime()
        cell.labelSum.text = row?.rate ?? 0 == 0 ? "" : "\(Double((row?.rate) ?? 0).convertedFromBackendFormat(afterPoint: 1))"
        cell.starImage.image = getStarRate(rate: row?.rate)
        
        shadowView(addView: cell.background)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if delegate?.getUserReviews()?[indexPath.row] != nil {
            delegate?.openReviewInfo(review: (delegate?.getUserReviews()?[indexPath.row])!)
        }
    }
}
