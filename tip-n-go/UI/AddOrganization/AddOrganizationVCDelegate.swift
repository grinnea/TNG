//
//  AddOrganizationVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 19.04.2022.
//

protocol AddOrganizationVCDelegate {
    func cancelAction()
    func getTypeView() -> Bool
    func getUserSelectOrganizatino() -> Organization?
    func getCountriesList() -> Countries?
    func getBrandsList() -> Brands?
    func changeBrandLogoAction()
    func editFriendship(brandHash: String, brandName: String)
    func saveOrganizationAction(brandHash: String, name: String, legalName: String, phone: String, website: String, email: String, address: String, countryId: Int)
    func saveBrandAndOrganizationAction(brandName: String, name: String, legalName: String, phone: String, website: String, email: String, address: String, countryId: Int)
    func updateOrganizationAction(hash: String, brandHash: String, name: String, legalName: String, phone: String, website: String, email: String, address: String)
}
