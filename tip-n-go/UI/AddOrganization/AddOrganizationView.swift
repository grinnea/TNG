//
//  AddOrganizationView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 19.04.2022.
//

import UIKit
import SDWebImage

@IBDesignable
final class AddOrganizationView: XibView {
    
    // MARK: Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var brandLogo: UIImageView!
    @IBOutlet weak var uploadLogo: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var brandView: TextFieldCustom!
    @IBOutlet weak var brandNameView: TextFieldCustom!
    @IBOutlet weak var nameOrgView: TextFieldCustom!
    @IBOutlet weak var legalNameOrgView: TextFieldCustom!
    @IBOutlet weak var countryView: TextFieldCustom!
    @IBOutlet weak var emailView: TextFieldCustom!
    @IBOutlet weak var phoneView: TextFieldCustom!
    @IBOutlet weak var websiteView: TextFieldCustom!
    @IBOutlet weak var adressView: TextFieldCustom!
    
    @IBOutlet weak var newBrandUpConstraint: NSLayoutConstraint!
    @IBOutlet weak var newBrandHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewToScrollHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate: AddOrganizationVCDelegate?
    var listBrandsPicker = [String]()
    var listCountryPicker = [String]()
    var brandSelectIndex = 0
    
    // MARK: - Methods
    
    func prepareView() {
        brandView.delegate = self
        brandNameView.delegate = self
        nameOrgView.delegate = self
        legalNameOrgView.delegate = self
        countryView.delegate = self
        emailView.delegate = self
        phoneView.delegate = self
        websiteView.delegate = self
        adressView.delegate = self
        
        if delegate?.getTypeView() == true {
            let organization = delegate?.getUserSelectOrganizatino()
            titleLabel.text = "organizations_edit_organization".localized()
            brandView.textField.text = organization?.brand?.name
            brandView.inputIsNotEmpty(empty: true)
            brandNameView.textField.text = ""
            brandNameView.inputIsNotEmpty(empty: true)
            nameOrgView.textField.text = organization?.name
            nameOrgView.inputIsNotEmpty(empty: true)
            legalNameOrgView.textField.text = organization?.legalName
            legalNameOrgView.inputIsNotEmpty(empty: true)
            countryView.textField.text = organization?.country?.name
            countryView.inputIsNotEmpty(empty: true)
            countryView.textField.isEnabled = false
            countryView.alpha = 0.3
            emailView.textField.text = organization?.email
            emailView.inputIsNotEmpty(empty: true)
            phoneView.textField.text = organization?.phone
            phoneView.inputIsNotEmpty(empty: true)
            websiteView.textField.text = organization?.website
            websiteView.inputIsNotEmpty(empty: true)
            adressView.textField.text = organization?.address
            adressView.inputIsNotEmpty(empty: true)
        }else{
            titleLabel.text = "organizations_add_new_organization".localized()
            brandView.textField.text = listBrandsPicker.count == 1 ? listBrandsPicker.first : listBrandsPicker[brandSelectIndex]
            brandView.inputIsNotEmpty(empty: true)
            brandView.pickerCustom.selectRow(brandSelectIndex, inComponent: 0, animated: true)
            countryView.textField.text = listCountryPicker.count == 1 ? listCountryPicker.first : nil
            countryView.inputIsNotEmpty(empty: listCountryPicker.count == 1)
        }
        
        saveButton.setTitle("save".localized(), for: .normal)
    
        brandView.label.text = "organizations_brand".localized()
        brandView.tintColor = .clear

        editButton.isHidden = (brandView.textField.text == "organizations_new_brand".localized() || brandView.textField.text == "")
        brandNameView.label.text = "organizations_new_brand_name".localized()
        nameOrgView.label.text = "organizations_organization_name".localized()
        legalNameOrgView.label.text = "organizations_legal_entity_name".localized()
        countryView.preparePicker(pickerType: true, pickerData: listCountryPicker)
        countryView.textField.resignFirstResponder()
        countryView.tintColor = .clear
        
        countryView.label.text = "country".localized()
        emailView.label.text = "email".localized()
        phoneView.preparePhone(on: true)
        websiteView.label.text = "website".localized()
        adressView.label.text = "address".localized()
        
        checkTextFieldsIsNotEmpty()
    }
    
    // MARK: - Actions
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.cancelAction()
    }
    
    @IBAction func uploadLogoAction(_ sender: Any) {
        self.endEditing(true)
        delegate?.changeBrandLogoAction()
    }
    
    @IBAction func editAction(_ sender: Any?) {
        self.endEditing(true)
        let brandHash = delegate?.getBrandsList()?.filter({ $0.name == brandView.textField.text ?? "" }).first?.hash ?? ""
        delegate?.editFriendship(brandHash: brandHash, brandName: brandView.textField.text ?? "")
    }
    
    @IBAction func saveAction(_ sender: Any?) {
        let brandName = brandNameView.textField.text ?? ""
        let nameOrg = nameOrgView.textField.text ?? ""
        let legalNameOrg = legalNameOrgView.textField.text ?? ""
        let email = emailView.textField.text ?? ""
        let textPhone = phoneView.textField.text ?? ""
        let website = websiteView.textField.text ?? ""
        let address = adressView.textField.text ?? ""
        
        let countryId = delegate?.getCountriesList()?.filter({ $0.name == countryView.textField.text ?? "" }).first?.id ?? 0
        let validCharacters = CharacterSet(charactersIn: "0123456789")
        let phone = textPhone.components(separatedBy: validCharacters.inverted).joined()

        if brandView.textField.text != "organizations_new_brand".localized() {
            let brandHash = delegate?.getBrandsList()?.filter({ $0.name == brandView.textField.text ?? "" }).first?.hash ?? ""
            if delegate?.getTypeView() == true {
                delegate?.updateOrganizationAction(hash: UserDefaults.clientDefaultOrganizationHash ?? "", brandHash: brandHash, name: nameOrg, legalName: legalNameOrg, phone: textPhone, website: website, email: email, address: address)
            }else{
                delegate?.saveOrganizationAction(brandHash: brandHash, name: nameOrg, legalName: legalNameOrg, phone: phone, website: website, email: email, address: address, countryId: countryId)
            }
        } else {
            delegate?.saveBrandAndOrganizationAction(brandName: brandName, name: nameOrg, legalName: legalNameOrg, phone: phone, website: website, email: email, address: address, countryId: countryId)
        }
    }
}

extension AddOrganizationView: TextFieldCustomDelegate {
    func flagAction() {

        brandSelectIndex = listBrandsPicker.firstIndex(where: { $0 == brandView.textField.text ?? "" }) ?? 0
        
        UIView.animate(withDuration: 0.4) {
            let brandImage = self.delegate?.getBrandsList()?.filter({ $0.name == self.brandView.textField.text ?? "" }).first?.logo ?? ""
            self.brandLogo.sd_setImage(with: URL(string: brandImage), placeholderImage: R.image.brandEmpty())
            self.uploadLogo.setTitle(brandImage == "" ? "organizations_upload_logo".localized() : "organizations_сhange_logo".localized(), for: .normal)
            self.editButton.isHidden = (self.brandView.textField.text == "organizations_new_brand".localized() || self.brandView.textField.text == "")
            self.newBrandUpConstraint.constant = self.brandView.textField.text == "organizations_new_brand".localized() ? 8 : 0
            self.newBrandHeightConstraint.constant = self.brandView.textField.text == "organizations_new_brand".localized() ? 60 : 0
            self.viewToScrollHeightConstraint.constant = self.brandView.textField.text == "organizations_new_brand".localized() ? 820 : 820 - 68
            self.layoutIfNeeded()
        }
        
        let phoneCountryCode = delegate?.getCountriesList()?.filter({ $0.name == countryView.textField.text }).first?.phoneCountryCode
        phoneView.selectFlag(phoneCountryCode: phoneCountryCode)
        phoneView.phoneCountryCode = phoneCountryCode ?? 0
    }
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let brand = self.brandView.textField.text, !brand.isEmpty,
            let name = self.nameOrgView.textField.text, !name.isEmpty,
            let legalName = self.legalNameOrgView.textField.text, !legalName.isEmpty,
            let country = self.countryView.textField.text, !country.isEmpty,
            let adress = self.adressView.textField.text, !adress.isEmpty
        else
        {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.3
            return
        }
        self.saveButton.isEnabled = true
        self.saveButton.alpha = 1
    }
}
