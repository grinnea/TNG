//
//  AddOrganizationVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 19.04.2022.
//

import UIKit
import PKHUD

class AddOrganizationVC: BaseVC, AddOrganizationVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: AddOrganizationView!
    
    let imagePicker = UIImagePickerController()
    var countriesList = Countries()
    var brandsList = Brands()
    var editOrganization = false
    var imageSelected = UIImage()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.hideKeyboardWhenTappedAround()
        let serialQueue = DispatchQueue(label: "Serial_Queue", qos: .userInitiated)
        serialQueue.async { Api.getBrands(self.handleGetBrands(success:response:error:)) }
        serialQueue.async { Api.settingsGetCountries(self.handleSettingsGetCountries(success:response:error:)) }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Methods
    
    func cancelAction() {
        dismiss(animated: true, completion: nil)
    }
    
    func getTypeView() -> Bool {
        return editOrganization
    }
    
    func getUserSelectOrganizatino() -> Organization? {
        return getUserCurrent()?.userOrganizations?.filter({ $0.organization?.hash == UserDefaults.clientDefaultOrganizationHash }).first?.organization
    }
    
    func getCountriesList() -> Countries? {
        return countriesList
    }
    
    func getBrandsList() -> Brands? {
        return brandsList
    }
    
    func editFriendship(brandHash: String, brandName: String) {
        let alert = UIAlertController(
            title: "Rename brand",
            message: "Renaming your brand will affect all organizations connected to this brand", preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.text = brandName
            textField.placeholder = "Brand name" }
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
        )
        
        let okAction = UIAlertAction(
            title: "save".localized(),
            style: .destructive,
            handler: { _ in
                guard let textField =  alert.textFields?.first else { return }
                Api.updateBrand(brandHash: brandHash, brandName: textField.text ?? "", ({ success, data, _ in
                    if success, let data = data {
                        do {
                            let brand = try JSONDecoder().decode(Brand.self, from: data)
                            self.root.brandView.textField.text = brand.name
                            Api.getBrands(self.handleGetBrands(success:response:error:))
                        } catch let error {
                            self.catchError(error: error)
                        }
                    } else if !success, let response = data {
                        self.handleErrorResponse(data: response)
                    }
                }))
            })
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveBrandAndOrganizationAction(brandName: String, name: String, legalName: String, phone: String,
                                        website: String, email: String, address: String, countryId: Int) {
        HUD.show(.progress)
        Api.createBrand(brandName: brandName, ({ success, data, _ in
            if success, let response = data {
                do {
                    let brandHash = try JSONDecoder().decode(Brand.self, from: response).hash ?? ""
                    Api.addOrganization(brandHash: brandHash, name: name, legalName: legalName,
                                        phone: phone, website: website, email: email, address: address, countryId: countryId, self.handleOrganization(success:response:error:))
                    if self.imageSelected != UIImage() {
                        Api.brandsUploadLogo(brandHash: brandHash, picture: self.imageSelected, self.handlePhotoUpload(success:response:))
                    }
                } catch let error {
                    self.catchError(error: error)
                }
            } else if !success, let response = data {
                self.handleErrorResponse(data: response)
            }
        })
        )
    }
    
    func changeBrandLogoAction() {
        let actionSheet = UIAlertController(
            title: "profile_choose_how_to_update_photo".localized(),
            message: nil,
            preferredStyle: .actionSheet
        )
        let takePhotoAction = UIAlertAction(
            title: "profile_take_photo".localized(),
            style: .default,
            handler: { [weak self] _ in
                self?.presentCameraVC()
            })
        let chooseImageAction = UIAlertAction(
            title: "profile_choose_photo".localized(),
            style: .default,
            handler: { [weak self] _ in
                self?.presentImagePickerVC()
            })
        let cancelAction = UIAlertAction(
            title: "cancel".localized(),
            style: .cancel,
            handler: nil
        )
        
        actionSheet.addAction(takePhotoAction)
        actionSheet.addAction(chooseImageAction)
        actionSheet.addAction(cancelAction)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc private func presentImagePickerVC() {
        let currentVC = UIApplication.topViewController()
        
        imagePicker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = .photoLibrary
       
        currentVC?.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc private func presentCameraVC() {
        let currentVC = UIApplication.topViewController()
        
        imagePicker.delegate = currentVC as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            
            currentVC?.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(
                title: "entrance_no_camera_title".localized(),
                message: "entrance_no_camera_message".localized(),
                preferredStyle: .alert
            )
            alert.addAction(
                UIAlertAction(
                    title: "entrance_no_camera_proceed".localized(),
                    style: .destructive,
                    handler: nil
                )
            )
            currentVC?.present(alert, animated: true, completion: nil)
        }
    }
    
    func saveOrganizationAction(brandHash: String, name: String, legalName: String, phone: String,
                                website: String, email: String, address: String, countryId: Int) {
        HUD.show(.progress)
        Api.addOrganization(brandHash: brandHash, name: name, legalName: legalName,
                            phone: phone, website: website, email: email, address: address, countryId: countryId, self.handleOrganization(success:response:error:))
    }
    
    func updateOrganizationAction(hash: String, brandHash: String, name: String, legalName: String, phone: String,
                                website: String, email: String, address: String) {
        HUD.show(.progress)
        Api.updateOrganization(hash: hash, brandHash: brandHash, name: name, legalName: legalName, phone: phone,
                            website: website, email: email, address: address, self.handleOrganization(success:response:error:))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height + 50, right: 0.0)
        root.scrollView.contentInset = contentInsets
        root.scrollView.scrollIndicatorInsets = contentInsets
      }

      @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
          root.scrollView.contentInset = contentInsets
          root.scrollView.scrollIndicatorInsets = contentInsets
      }
    
    // MARK: - Handle
    
    func handleSettingsGetCountries(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                root.listCountryPicker.removeAll()
                countriesList = try JSONDecoder().decode(Countries.self, from: data)
                countriesList.forEach({ root.listCountryPicker.append($0.name ?? "") })
                root.prepareView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleGetBrands(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                root.listBrandsPicker.removeAll()
                brandsList = try JSONDecoder().decode(Brands.self, from: data)
                brandsList.forEach({ root.listBrandsPicker.append($0.name ?? "") })
                root.listBrandsPicker.append("organizations_new_brand".localized())
                root.brandView.preparePicker(pickerType: true, pickerData: root.listBrandsPicker)
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleOrganization(success: Bool, response: Data?, error: Error?) {
        if success {
                HUD.flash(.success, delay: 1.0)
                self.cancelAction()
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    private func handlePhotoUpload(success: Bool, response: Data?) {
        if success, let data = response {
            do {
                let brandImage = try JSONDecoder().decode(Brand.self, from: data).logo ?? ""
                self.root.brandLogo.sd_setImage(with: URL(string: brandImage), placeholderImage: R.image.brandEmpty())
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let response = response {
            self.handleErrorResponse(data: response)
        }
        HUD.hide()
    }
}

// MARK: - Image Picker & Camera Delegate

extension AddOrganizationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.editedImage] as? UIImage{
            self.root.brandLogo.image = image
            imageSelected = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
