//
//  HistoryWithdrawVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

import UIKit
import PKHUD

class HistoryWithdrawVC: BaseVC, HistoryWithdrawVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: HistoryWithdrawView!

    var payouts = Payouts()
    var balance = Balance()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiFinancePayouts()
    }
    
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getApiFinancePayouts() {
        Api.financePayouts(self.handleGetUserTips(success:response:error:))
    }
    
    func openWithdraw() {
        self.openVC(vc: R.storyboard.main.withdrawVC())
    }
    
    func getPayouts() -> Payouts? {
        return payouts
    }
    
    func getBalance() -> Balance? {
        return balance
    }
    
    // MARK: - Handle
    
    func handleGetUserTips(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                payouts = try jsonDecoder.decode(Payouts.self, from: data)
                self.root.HistoryWithdrawTableView.reloadData()
                root.prepareEmptyView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
}

