//
//  HistoryWithdrawCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

import UIKit

class HistoryWithdrawCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var balanceLeft: UILabel!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
}

