//
//  HistoryWithdrawView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

import UIKit

@IBDesignable
final class HistoryWithdrawView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var HistoryWithdrawTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: HistoryWithdrawVCDelegate?
    
    
    // MARK: - Methods
    
    func prepareView() {
        titleLabel.text = "history_history_of_withdrawals".localized()
        emptyTitle.text = "history_you_have_not_made_any_payouts_yet".localized()
        emptyText.text = "history_the_history_will_be_available_after_withdraw".localized()
        shadowView(addView: emptyView)
    }
    
    func prepareTable() {
        HistoryWithdrawTableView.dataSource = self
        HistoryWithdrawTableView.delegate = self
        HistoryWithdrawTableView?.register(R.nib.historyWithdrawCell)
        HistoryWithdrawTableView.tableFooterView = UIView()
    }
    
    func prepareEmptyView() {
        if delegate?.getPayouts()?.count == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
        }
    }
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
}

// MARK: - Table view data source

extension HistoryWithdrawView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getPayouts()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.historyWithdrawCell, for: indexPath)!
        let row = delegate?.getPayouts()?[indexPath.row]
        cell.cardNumber.text = row?.userBankCard?.cardNumberMasked
        cell.amount.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
        cell.date.text = row?.date?.toFullFormatWithTime()
        shadowView(addView: cell.background)
        return cell
    }

}

