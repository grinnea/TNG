//
//  HistoryWithdrawVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

protocol HistoryWithdrawVCDelegate {
    func backAction()
    func getPayouts() -> Payouts?
    func getBalance() -> Balance?
}
