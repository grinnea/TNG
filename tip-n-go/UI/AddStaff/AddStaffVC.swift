//
//  AddStaffVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 01.04.2022.
//

import UIKit
import PKHUD

class AddStaffVC: BaseVC, AddStaffVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: AddStaffView!
    
    var organizationRoles = Roles()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        self.hideKeyboardWhenTappedAround()
        Api.organizationAvailableRoles(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleOrganizationRoles(success:response:error:))
    }
    
    // MARK: - Methods
    
    func getOrganizationRoles() -> Roles? {
        return organizationRoles
    }
    
    func saveEmployee(phone: String, email: String, lastName: String, firstName: String, position: String, roleId: Int) {
        Api.registrationUserToOrganization(phone: phone, email: email, lastName: lastName, firstName: firstName,
                                            organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", position: position,
                                           roleId: roleId, self.handleRegistrationUserToOrganization(success:response:error:))
    }
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Handle
    
    func handleOrganizationRoles(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            do {
                organizationRoles = try JSONDecoder().decode(Roles.self, from: data)
                root.prepareFields()
            } catch let error {
                self.catchError(error: error)
            }
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
    func handleRegistrationUserToOrganization(success: Bool, response: Data?, error: Error?) {
        if success {
            HUD.flash(.success, delay: 1.0)
            self.backAction()
        } else if !success, let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
}
