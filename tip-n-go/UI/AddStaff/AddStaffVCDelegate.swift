//
//  AddStaffVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 01.04.2022.
//

protocol AddStaffVCDelegate {
    func backAction()
    func getOrganizationRoles() -> Roles?
    func saveEmployee(phone: String, email: String, lastName: String, firstName: String, position: String, roleId: Int)
}
