//
//  AddStaffView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 01.04.2022.
//

import UIKit

@IBDesignable
final class AddStaffView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var roleView: TextFieldCustom!
    @IBOutlet weak var lastNameView: TextFieldCustom!
    @IBOutlet weak var firstNameView: TextFieldCustom!
    @IBOutlet weak var emailView: TextFieldCustom!
    @IBOutlet weak var phoneView: TextFieldCustom!
    @IBOutlet weak var positionView: TextFieldCustom!
    
    var delegate: AddStaffVCDelegate?
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func saveEmployeeAction(_ sender: Any) {
        let roleId = delegate?.getOrganizationRoles()?.filter({ $0.name == roleView.textField.text ?? "" }).first?.id
        let validCharacters = CharacterSet(charactersIn: "0123456789")
        let phone = phoneView.phoneTextField.text?.components(separatedBy: validCharacters.inverted).joined()
        delegate?.saveEmployee(phone: phone ?? "", email: emailView.textField.text ?? "",
                               lastName: lastNameView.textField.text ?? "", firstName: firstNameView.textField.text ?? "",
                               position: positionView.textField.text ?? "", roleId: roleId ?? 0)
    }
    
    // MARK: - Methods
    
    func prepareFields() {
        title.text = "employee_add_an_employee".localized()
        saveButton.setTitle("save".localized(), for: .normal)
        var listPicker = [String]()
        delegate?.getOrganizationRoles()?.forEach({
            listPicker.append($0.name ?? "")
        })
        
        roleView.preparePicker(pickerType: true, pickerData: listPicker)
        roleView.textField.resignFirstResponder()
        roleView.delegate = self
        lastNameView.delegate = self
        firstNameView.delegate = self
        emailView.delegate = self
        phoneView.delegate = self
        positionView.delegate = self
        
        roleView.label.text = "employee_role".localized()
        lastNameView.label.text = "employee_last_name".localized()
        firstNameView.label.text = "employee_first_name".localized()
        emailView.label.text = "email".localized()
        phoneView.preparePhone(on: true)
        positionView.label.text = "employee_position".localized()
        
        checkTextFieldsIsNotEmpty()
    }

}


// MARK: - TextFieldCustom

extension AddStaffView: TextFieldCustomDelegate {
    
    func checkTextFieldsIsNotEmpty() {
        guard
            let role = self.roleView.textField.text, !role.isEmpty,
            let lastName = self.lastNameView.textField.text, !lastName.isEmpty,
            let firstName = self.firstNameView.textField.text, !firstName.isEmpty,
            let email = self.emailView.textField.text,
            let phone = self.phoneView.phoneTextField.text,
            (!phone.isEmpty || !email.isEmpty)
        else
        {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.3
            return
        }
        self.saveButton.isEnabled = true
        self.saveButton.alpha = 1
    }
}
