//
//  HistoryDistributedVC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.04.2022.
//

import UIKit
import PKHUD

class HistoryDistributedVC: BaseVC, HistoryDistributedVCDelegate {

    // MARK: Properties
    
    @IBOutlet private weak var root: HistoryDistributedView!
    
    var tipDistributions = TipDistributions()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        root.delegate = self
        root.prepareView()
        root.prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getApiFinanceTipDistributions()
        root.prepareEmptyView()
    }
    
    
    // MARK: - Methods
    
    func backAction() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func openDistributed() {
        openVC(vc: R.storyboard.main.distributeVC())
    }
   
    func getTipDistributions() -> TipDistributions? {
        return tipDistributions
    }
    
    func getApiFinanceTipDistributions() {
        Api.financeTipDistributions(organizationHash: UserDefaults.clientDefaultOrganizationHash ?? "", self.handleGetUserTips(success:response:error:))
    }
    
    func openInfo(sender: TipDistribution?) {
        let vc = R.storyboard.main.distributedInfoVC()
        vc?.tipDistribution = sender!
        openVC(vc: vc)
    }
    
    // MARK: - Handle
    
    func handleGetUserTips(success: Bool, response: Data?, error: Error?) {
        if success, let data = response {
            let jsonDecoder = JSONDecoder()
            do {
                tipDistributions = try jsonDecoder.decode(TipDistributions.self, from: data)
                self.root.historyDistributedTableView.reloadData()
                root.prepareEmptyView()
            } catch let error {
                self.catchError(error: error)
            }
        } else if let data = response {
            self.handleErrorResponse(data: data)
        }
    }
    
}
