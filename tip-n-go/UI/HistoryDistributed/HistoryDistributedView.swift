//
//  HistoryDistributedView.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.04.2022.
//

import UIKit

@IBDesignable
final class HistoryDistributedView: XibView {
    
    // MARK: Properties
    @IBOutlet weak var historyDistributedTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyTitle: UILabel!
    @IBOutlet weak var emptyText: UILabel!
    
    var delegate: HistoryDistributedVCDelegate?
    
    
    // MARK: - Methods
    
    func prepareView() {
        shadowView(addView: emptyView)
        titleLabel.text = "history_of_tips_distributed".localized()
        emptyTitle.text = "history_distributed_empty_title".localized()
        emptyText.text = "history_the_history_will_be_available_after_distribute_tips".localized()
    }
    
    func prepareTable() {
        historyDistributedTableView.dataSource = self
        historyDistributedTableView.delegate = self
        historyDistributedTableView?.register(R.nib.historyDistributedCell)
        historyDistributedTableView.tableFooterView = UIView()
    }
    
    func prepareEmptyView() {
        if historyDistributedTableView.numberOfRows(inSection: 0) == 0 {
            emptyView.isHidden = false
            emptyTitle.isHidden = false
            emptyText.isHidden = false
        }else{
            emptyView.isHidden = true
            emptyTitle.isHidden = true
            emptyText.isHidden = true
        }
    }
    
    // MARK: - Action
    
    @IBAction func backAction(_ sender: Any) {
        delegate?.backAction()
    }
    
    @IBAction func bistributedAction(_ sender: Any) {
        delegate?.openDistributed()
    }
}

// MARK: - Table view data source

extension HistoryDistributedView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.getTipDistributions()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.historyDistributedCell, for: indexPath)!
        let row = delegate?.getTipDistributions()?[indexPath.row]
        cell.name.text = (row?.user?.lastName ?? "") + " " + (row?.user?.firstName ?? "")
        cell.amount.text = "\(Double((row?.amount) ?? 0).convertedFromBackendFormat(afterPoint: 2)) " + (row?.currency?.sign ?? "")
        cell.date.text = row?.date?.toFullFormatWithTime()
        if row?.done == true {
            cell.personCount.text = "\(row?.destinations?.count ?? 0) " + "history_num_employees".localized()
            cell.isUserInteractionEnabled = true
        } else {
            cell.personCount.text = "history_in_progress".localized()
            cell.isUserInteractionEnabled = false
        }
       
        shadowView(addView: cell.background)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = delegate?.getTipDistributions()?[indexPath.row]
        if row?.done == true {
            delegate?.openInfo(sender: delegate?.getTipDistributions()?[indexPath.row])
        }
    }
}
