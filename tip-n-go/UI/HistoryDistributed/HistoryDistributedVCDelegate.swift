//
//  HistoryDistributedVCDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.04.2022.
//

protocol HistoryDistributedVCDelegate {
    func backAction()
    func openDistributed()
    func getTipDistributions() -> TipDistributions?
    func openInfo(sender: TipDistribution?)
}
