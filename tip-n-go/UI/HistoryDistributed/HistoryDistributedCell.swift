//
//  HistoryDistributedCell.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.04.2022.
//

import UIKit

class HistoryDistributedCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var personCount: UILabel!
    
    
    // MARK: - Lifecycle & Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
}
