//
//  UserDefaults+Variables.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

extension UserDefaults {
    
    // MARK: - Variables
    
    // Client Data
   
    static var currentUser: Data? {
        get { return UserDefaults.standard.data(forKey: "CURRENT_USER") }
        set { UserDefaults.standard.set(newValue, forKey: "CURRENT_USER") }
    }
    
    static var clientDefaultOrganizationHash: String? {
        get { return UserDefaults.standard.string(forKey: "CLIENT_DEFAULT_ORGANIZATION_HASH") }
        set { UserDefaults.standard.set(newValue, forKey: "CLIENT_DEFAULT_ORGANIZATION_HASH") }
    }
    
    static var clientUserPic: String? {
        get { return UserDefaults.standard.string(forKey: "CLIENT_USER_PIC") }
        set { UserDefaults.standard.set(newValue, forKey: "CLIENT_USER_PIC") }
    }
    
    static var clientUserBankCard: String? {
        get { return UserDefaults.standard.string(forKey: "CLIENT_USER_BANK_CARD") }
        set { UserDefaults.standard.set(newValue, forKey: "CLIENT_USER_BANK_CARD") }
    }
    
    static var hasAccessToSubInfo: Bool? {
        get { return UserDefaults.standard.bool(forKey: "INFO_BY_SUB_ACCESS") }
        set { UserDefaults.standard.set(newValue, forKey: "INFO_BY_SUB_ACCESS") }
    }
    
    static var selfieUploadDelay: Double? {
        get { return UserDefaults.standard.double(forKey: "SELFIE_UPLOAD_DELAY") }
        set { UserDefaults.standard.set(newValue, forKey: "SELFIE_UPLOAD_DELAY") }
    }
    
    static var selfieAccepted: Bool? {
        get { UserDefaults.standard.bool(forKey: "SELFIE_ACCEPTED") }
        set { UserDefaults.standard.set(newValue, forKey: "SELFIE_ACCEPTED") }
    }
    
    static var networkClubsWithZones: Data? {
        get { return UserDefaults.standard.data(forKey: "NETWORK_CLUBS_WITH_ZONES") }
        set { UserDefaults.standard.set(newValue, forKey: "NETWORK_CLUBS_WITH_ZONES") }
    }
    
    static var networkClubLoad: Data? {
        get { return UserDefaults.standard.data(forKey: "NETWORK_CLUBS") }
        set { UserDefaults.standard.set(newValue, forKey: "NETWORK_CLUBS") }
    }
    
    static var clubTimeSheet: Data? { // remove (?)
        get { return UserDefaults.standard.data(forKey: "CLUB_TIME_SHEET") }
        set { UserDefaults.standard.set(newValue, forKey: "CLUB_TIME_SHEET") }
    }
    
    static var networkId: String? {
        get { return UserDefaults.standard.string(forKey: "NETWORK_ID") }
        set { UserDefaults.standard.setValue(newValue, forKey: "NETWORK_ID") }
    }
    
    static var clubIds: [String: String] {
        get { return UserDefaults.standard.dictionary(forKey: "CLUB_IDS") as? [String: String] ?? [:] }
        set { UserDefaults.standard.setValue(newValue, forKey: "CLUB_IDS") }
    }
    
    static var lockerPinCode: Int? {
        get { return UserDefaults.standard.integer(forKey: "Locker_PinCode") }
        set { UserDefaults.standard.setValue(newValue, forKey: "Locker_PinCode") }
    }
    
    static var scheduleUpcomingTrainings: Bool? {
        get { return UserDefaults.standard.bool(forKey: "Schedule_Upcoming_Trainings") }
        set { UserDefaults.standard.setValue(newValue, forKey: "Schedule_Upcoming_Trainings") }
    }
    
    static var hasTrainerContract: Bool? {
        get { return UserDefaults.standard.bool(forKey: "HAS_TRAINER_CONTRACT") }
        set { UserDefaults.standard.setValue(newValue, forKey: "HAS_TRAINER_CONTRACT") }
    }
    
    static var availableLanguages: [String: String] {
        get { return UserDefaults.standard.dictionary(forKey: "AVAILABLE_LANGUAGES") as? [String: String] ?? [:] }
        set { UserDefaults.standard.set(newValue, forKey: "AVAILABLE_LANGUAGES") }
    }
    
    static var clientHash: String? {
        get { return UserDefaults.standard.string(forKey: "CLIENT_HASH") }
        set { UserDefaults.standard.setValue(newValue, forKey: "CLIENT_HASH") }
    }
    
    static var loginString: String {
        get { return UserDefaults.standard.string(forKey: "LOGIN_STRING") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "LOGIN_STRING") }
    }
    
    static var notiEmpty: Bool {
            get { return UserDefaults.standard.bool(forKey: "NOTI_EMPTY") }
            set { UserDefaults.standard.set(newValue, forKey: "NOTI_EMPTY") }
        }

    // JWT Tokens
    
    static var tokensReceivedTime: Double? {
        get { return UserDefaults.standard.double(forKey: "ACCESS_TOKENS_DELIVERY_TTMP") }
        set { UserDefaults.standard.set(newValue, forKey: "ACCESS_TOKENS_DELIVERY_TTMP") }
    }
    
    static var jwtAccessToken: String? {
        get { return UserDefaults.standard.string(forKey: "JWT_ACCESS_TOKEN") }
        set { UserDefaults.standard.set(newValue, forKey: "JWT_ACCESS_TOKEN") }
    }
    static var jwtAccessTokenTimeToLife: Double? {
        get { return UserDefaults.standard.double(forKey: "JWT_ACCESS_TOKEN_TTL") }
        set { UserDefaults.standard.set(newValue, forKey: "JWT_ACCESS_TOKEN_TTL") }
    }
    
    static var jwtRefreshToken: String? {
        get { return UserDefaults.standard.string(forKey: "JWT_REFRESH_TOKEN") }
        set { UserDefaults.standard.set(newValue, forKey: "JWT_REFRESH_TOKEN") }
    }
    static var jwtRefreshTokenTimeToLife: Double? {
        get { return UserDefaults.standard.double(forKey: "JWT_ACCESS_REFERSH_TTL") }
        set { UserDefaults.standard.set(newValue, forKey: "JWT_ACCESS_REFERSH_TTL") }
    }
    
    // Firebase
    
    static var fcmToken: String? {
        get { return UserDefaults.standard.string(forKey: "FCM_TOKEN") }
        set { UserDefaults.standard.set(newValue, forKey: "FCM_TOKEN") }
    }
    
    static var fcmNotificationTapAction: String? {
        get { return UserDefaults.standard.string(forKey: "FCM_TAP_ACTION") }
        set { UserDefaults.standard.set(newValue, forKey: "FCM_TAP_ACTION") }
    }
    
    static var fcmNotificationTapActionId: Int? {
        get { return UserDefaults.standard.integer(forKey: "FCM_TAP_ACTION_ID") }
        set { UserDefaults.standard.set(newValue, forKey: "FCM_TAP_ACTION_ID") }
    }
    
    // Debug
    
    static var debugInfo: [String: String]? {
        get { return UserDefaults.standard.object(forKey: "DEBUG_INFO") as! [String: String]? }
        set { UserDefaults.standard.object(forKey: "DEBUG_INFO") }
    }

    // MARK: - Methods
    
    class func flushClientData() {

        UserDefaults.tokensReceivedTime = nil
        UserDefaults.jwtRefreshToken?.removeAll()
        UserDefaults.jwtAccessToken?.removeAll()
        UserDefaults.jwtAccessTokenTimeToLife = nil
        UserDefaults.jwtRefreshTokenTimeToLife = nil

        UserDefaults.flushFcmRoutingData()
    }
    
    class func fcmNotificationSetRoute(action: String, id: Int) {
        UserDefaults.fcmNotificationTapAction = action
        UserDefaults.fcmNotificationTapActionId = id
    }
    
    class func flushFcmRoutingData() {
        UserDefaults.fcmNotificationTapAction = nil
        UserDefaults.fcmNotificationTapActionId = nil
    }

}
