//
//  BundleHelper.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

class BundleHelper {
    
    class func getBundleValueByKey(_ key: String) -> String {
        return Bundle.main.infoDictionary?[key] as? String ?? ""
    }
}
