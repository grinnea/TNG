//
//  LocalStorage.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

/// Coordinates the persistent stores of the application
/// Writes and reads data from the persistent store and delivers it to users (VS's, APIClient etc.)
///
/// Reason to change:
///     - new properties added
final class LocalStorage {

    // MARK: - Utility
    
    private enum PersistentStoresError: Error {
        case parsingError(_ error: Error)
        case writingError(_ error: Error)
    }
    
    // MARK: - Properties
    
    static var userDefaults = UserDefaults.standard
    
    // MARK: - Methods
    
    class func fetchObject<T: Decodable>(from: Data, to: T.Type) throws -> T? {
        var decodedObject: T?
        
        do {
            decodedObject = try JSONDecoder().decode(to, from: from)
            return decodedObject
        } catch let error {
            throw PersistentStoresError.parsingError(error)
        }
    }
    
    class func writeObject<T: Encodable>(object: T, to: String) throws {
        var encodedObject: T?
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        let key = "\(to)"
        
        do {
            encodedObject = try encoder.encode(object) as? T
            userDefaults.setValue(encodedObject, forKey: key)
        } catch let error {
            throw PersistentStoresError.writingError(error)
        }
    }
}
