//
//  Languages.swift
//  tip-n-go
//
//  Created by developer03 on 02.02.2021.
//

import Foundation
import Localize_Swift

enum Languages: String {
    case en
    case fr
}

extension Languages {
    var langcode: String {
        switch self {
        case .en:
            return "en"
        case .fr:
            return "fr-FR"
        }
    }
    
    static func getName(_ code: String) -> String {
        let locale : NSLocale = NSLocale(localeIdentifier: code)
        return locale.displayName(forKey: NSLocale.Key.identifier, value: code) ?? code
    }
    
    static func getLocalName(_ code: String) -> String {
        return Localize.displayNameForLanguage(code)
    }
}
