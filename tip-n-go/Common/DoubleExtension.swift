//
//  Double+BackendConversion.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 10.03.2022.
//

import Foundation

extension Double {
    
    func convertedFromBackendFormat(afterPoint: Int) -> Any {
        return (self / 100).removeZerosFromEnd(afterPoint: afterPoint)
    }
    
    func removeZerosFromEnd(afterPoint: Int) -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = afterPoint
        formatter.maximumFractionDigits = afterPoint
        return String(formatter.string(from: number) ?? "")
    }

}
