//
//  Error+CustomMessage.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 10.03.2022.
//

import Foundation

extension Error {
    @discardableResult func show() -> PeopleToastPanel {
        let panel = PeopleToastPanel()
        panel.title = self.localizedDescription
        panel.isError = true
        panel.show()
        return panel
    }
}
