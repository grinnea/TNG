//
//  String+DateConversion.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import Localize_Swift

// MARK: - Dates Conversion

extension String {
    
    func formatToLocale() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage())
        
        let date = dateFormatter.date(from: self)
        dateFormatter.dateStyle = .short
        
        if let date = date {
            return dateFormatter.string(from: date).uppercased()
        } else {
            return ""
        }
    }
    
    func formatToLocaleWithTime() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" // consider deleting as this is not needed
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage())
        if let formattedDate = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = DateFormatter.dateFormat(
                fromTemplate: "dd MMMM yyyy, HH:mm",
                options: 0,
                locale: Locale(identifier: Localize.currentLanguage())
            )
            return dateFormatter.string(from: formattedDate)
        } else {
            return self
        }
    }
    
    func formatToLocaleWithoutTime() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" // consider deleting as this is not needed
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage())
        if let formattedDate = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = DateFormatter.dateFormat(
                fromTemplate: "dd MMMM yyyy",
                options: 0,
                locale: Locale(identifier: Localize.currentLanguage())
            )
            return dateFormatter.string(from: formattedDate)
        } else {
            return self
        }
    }
    
    func formatToShortTime() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ" // consider deleting as this is not needed
        
        if let formattedDate = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = DateFormatter.dateFormat(
                fromTemplate: "HH:mm",
                options: 0,
                locale: Locale(identifier: Localize.currentLanguage())
            )
            return dateFormatter.string(from: formattedDate)
        } else {
            return self
        }
    }
    
    func formatToShortDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"//this your string date format
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage())
        let convertedDate = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "d MMMM"
        
        let relativeDateFormatter = DateFormatter()
        relativeDateFormatter.timeStyle = .none
        relativeDateFormatter.dateStyle = .medium
        relativeDateFormatter.locale = Locale(identifier: Localize.currentLanguage())
        relativeDateFormatter.doesRelativeDateFormatting = true
        
        let string = relativeDateFormatter.string(from: convertedDate ?? Date())
        if let _ = string.rangeOfCharacter(from: .decimalDigits) {
            return dateFormatter.string(from: convertedDate ?? Date())
        } else {
            return string
        }
    }
}

// MARK: - Crypto

extension String {
    
    // This is HMAC-signature method addition to string
    
    func signHMAC(_ algorithm: HMACAlgorithm, _ key: String) -> String {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = Int(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        let keyStr = key.cString(using: String.Encoding.utf8)
        let keyLen = Int(key.lengthOfBytes(using: String.Encoding.utf8))
        
        CCHmac(algorithm.algorithm, keyStr!, keyLen, str!, strLen, result)
        
        let a = UnsafeMutableBufferPointer(start: result, count: digestLen)
        let b = Data(a)
        
        result.deallocate()
        
        let digest = b.base64EncodedString()
        
        return digest
    }
    
}

// MARK: - To Dates Conversion

extension String {

    func toIso8601Date() -> Date {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage()) // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return dateFormatter.date(from: self) ?? Date()
    }
    
    func toShortDate() -> Date {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: Localize.currentLanguage()) // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: self) ?? Date()
    }
    
    func toFullFormatWithTime() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        let outputFormatter = DateFormatter()
        outputFormatter.locale = Locale.current
        outputFormatter.dateFormat = "dd MMMM yyyy, HH:mm"
        
        guard let date = formatter.date(from: self) else {
            let date = Date()
            return outputFormatter.string(from: date)
        }

        return outputFormatter.string(from: date)
    }
    
    func convertToTimeInterval() -> TimeInterval {
        guard self != "" else { return 0 }
        let dateFormatter = DateFormatter()
        
        return dateFormatter.date(from: self)?.timeIntervalSince1970 ?? 0.0
    }
}

// MARK: - Split

extension String {
    
    func splitAtFirst(_ delimiter: String) -> (String, String)? {
        guard
            let upperIndex = self.range(of: delimiter)?.upperBound,
            let lowerIndex = self.range(of: delimiter)?.lowerBound
        else { return nil }
        
        let firstPart = String.init(self.prefix(upTo: lowerIndex))
        let secondPart = String.init(self.suffix(from: upperIndex))
        
        return (firstPart, secondPart)
    }
}

// MARK: - Validation

extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
    }
    
    var isExclusivelyNumeric: Bool {
        return self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

// MARK: - Trimming

extension String {
    func trimHtmlTags() -> String {
        guard self != "" else { return "" }
        
        let regex = try! NSRegularExpression(
            pattern: "(\\s?(<(\\/?)(\\w+)>)\\s?)",
            options: .caseInsensitive
        )
        
        let range = NSMakeRange(0, self.count)
        
        let modifiedResult = regex.stringByReplacingMatches(
            in: self,
            options: [],
            range: range,
            withTemplate: ""
        )
        
        return modifiedResult
    }
}

// MARK: Phone Number Parsing

extension String {
    
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(
            of: "(\\d{1})(\\d{3})(\\d{3})(\\d+)",
            with: "+$1($2)$3-$4",
            options: .regularExpression,
            range: nil
        )
    }
}

// MARK: - Localizable

extension String: Localizable {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var localizedError: String {
        return self.localized()
    }
}

// MARK: - First Letter Upper Case

extension String {

    func firstUpperCase() -> String {
        guard !isEmpty else { return "" }
        return String(self.prefix(1).uppercased() + self.dropFirst().lowercased())
    }
}


// MARK: - String[]

extension String {

    var length: Int { return count }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}
