//
//  UIImage+Rescale.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 10.03.2022.
//

import UIKit

// MARK: - Resize

extension UIImage {
    func resize(_ maxDimensionSize: CGFloat) -> UIImage {
        let ratio = max(self.size.width, self.size.height) / min(self.size.width, self.size.height)
        var newSize = CGSize()
        
        if self.size.height > self.size.width {
            newSize = CGSize(width: maxDimensionSize / ratio, height: maxDimensionSize)
        } else {
            newSize = CGSize(width: maxDimensionSize, height: maxDimensionSize / ratio)
        }
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, 1)
        
        draw(in: CGRect(origin: .zero, size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndPDFContext()
        
        return newImage!
    }
    
    func resize(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        let newSize = widthRatio > heightRatio ?
            CGSize(width: size.width * heightRatio, height: size.height * heightRatio) :
            CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    class func imageWithColor(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
