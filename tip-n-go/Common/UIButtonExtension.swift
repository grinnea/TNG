//
//  UIButtonExtension.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 16.03.2022.
//

import UIKit

extension UIButton {
    func setBorder(cornerRadious: CGFloat) {
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = cornerRadious
        self.layer.borderColor = R.color.blue()?.cgColor
        self.layer.backgroundColor = R.color.blueLite()?.cgColor
        self.layer.masksToBounds = true
    }
}
