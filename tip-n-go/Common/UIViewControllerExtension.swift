//
//  SceneDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 10.03.2022.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
//        tap.cancelsTouchesInView = false
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func instantAlertShow(_ title: String, message: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok".localized(), style: .destructive, handler: handler)
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
}
