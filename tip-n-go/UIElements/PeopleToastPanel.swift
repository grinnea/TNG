//
//  PeopleToastPanel.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit
import SwiftMessages

class PeopleToastPanel: UIView {
    
    private let backView = UIView()
    private let titleLabel = UILabel()
    
    var onClose: (() -> Void)?
    
    var title: String? {
        didSet {
            self.titleLabel.text = self.title
            self.setNeedsLayout()
            self.invalidateIntrinsicContentSize()
        }
    }
    
    var isError: Bool = false {
        didSet {
            if self.isError {
                self.backView.backgroundColor = .red //.red80()
            } else {
                self.backView.backgroundColor = R.color.gray() //.green80()
            }
        }
    }
    
    var extraBottomOffset: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func onPanelClose() {
        self.onClose?()
        self.hide()
    }
    
    private func setup() {
        self.backgroundColor = UIColor.clear
        self.backView.layer.cornerRadius = 9
        self.backView.backgroundColor = R.color.gray() //.green80()
        self.addSubview(self.backView)
        
        self.titleLabel.textAlignment = .center
        self.titleLabel.font = UIFont.systemFont(ofSize: 16)
        self.titleLabel.textColor = R.color.white()
        self.titleLabel.numberOfLines = 0
        self.addSubview(titleLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let margin: CGFloat = 20
        let verticalMargin: CGFloat = 24
        self.backView.frame = CGRect(x: margin, y: verticalMargin, width: self.frame.size.width - margin * 2, height: self.frame.size.height - verticalMargin * 2 - self.extraBottomOffset)
        let labelsMaxSize = CGSize(width: self.frame.size.width - margin * 4, height: self.frame.size.height)
        let titleSize = self.titleLabel.sizeThatFits(labelsMaxSize)
        self.titleLabel.frame = CGRect(x: (self.frame.size.width - titleSize.width)/2, y: (self.frame.size.height - self.extraBottomOffset - titleSize.height)/2 - 2, width: titleSize.width, height: titleSize.height)
    }
    
    @objc private func closeAction() {
        self.hide()
        self.onClose?()
    }
    
    override var intrinsicContentSize: CGSize {
        let labelsMaxSize = CGSize(width: UIScreen.main.bounds.size.width - 80, height: UIScreen.main.bounds.size.height)
        let titleSize = self.titleLabel.sizeThatFits(labelsMaxSize)
        return CGSize(width: UIScreen.main.bounds.size.width, height: 68 + titleSize.height + self.extraBottomOffset)
    }
    
    func show() {
        let duration: Double = 3.5
        var config = SwiftMessages.Config()
        config.presentationStyle = .bottom
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.duration = .seconds(seconds: duration)
        config.dimMode = .color(color: UIColor.black.withAlphaComponent(0.5), interactive: true)
        config.eventListeners = [self.eventListener]
        
        SwiftMessages.show(config: config, view: self)
    }
    
    private func eventListener(event: SwiftMessages.Event) {
        if case .willHide = event {
            self.onPanelClose()
        }
    }
    
    class func hideAll() {
        SwiftMessages.hideAll()
    }
    
    func hide() {
        SwiftMessages.hide()
    }
}
