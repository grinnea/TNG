//
//  File.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import Alamofire

class ApiRequestAdapter: RequestAdapter {
    
    var isJwt: Bool
    
    init(isJwt :Bool) {
        self.isJwt = isJwt
    }
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var adaptedRequest = urlRequest
        
        adaptedRequest.headers.add(
            HTTPHeader(
                name: "Api-Version",
                value: BundleHelper.getBundleValueByKey("API_VERSION")
            )
        )
        
        if isJwt {
            let token = UserDefaults.jwtAccessToken ?? ""
            adaptedRequest.headers.update(.authorization(bearerToken: token))
        } else {
            let token = HMACService
                .sendWithHmacAuthHeader(
                    requestMethod: adaptedRequest.httpMethod!,
                    requestURI: adaptedRequest.url?.absoluteString ?? "",
                    content: String(
                        data: adaptedRequest.httpBody ?? Data(),
                        encoding: .utf8
                    ) ?? ""
                )
            adaptedRequest.headers.update(.authorization(token))
        }
        
        adaptedRequest.timeoutInterval = 5.0
        completion(.success(adaptedRequest))
    }
}
