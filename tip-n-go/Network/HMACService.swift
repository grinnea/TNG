//
//  HMAC.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import CryptoKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

/// Placeholder for vairious algorithms for HMAC signing for scalability purposes.
/// Works with CommonCrypto / CommonHMAC library

final class HMACService {
    
    static var clientId: String {
        return EnvironmentHelper.clientId
    }
    
    static var clientSecret: String {
        return EnvironmentHelper.clientSecret
    }
    
    // MARK: - HMAC Signing
    
    /// Returns the authorization header that is used by the interceptor
    ///
    /// - Parameters:
    ///   - requestMethod: RESTful methds: "GET" / "POST" / "PUT" / "DELETE" in String format.
    ///   - requestURI: URL on which the request should be performed. It is taken to the algorithm and then processed to become a part of header.
    ///   - content: Suitable for POST or PUT requests - as they accompany the main request with some content to be passed to the server.
    /// - Returns: Complete HMAC-signed header in String format.
    /// - Flow:
    ///     - Create content base64 string.
    ///     - Encode Requestr URI (% encoding).
    ///     - Calculate timestamp.
    ///     - Create random nonce for each request.
    ///     - Optionally hash the content if the request is POST or PUT.
    ///     - Create the raw signature string by combining APPId, request Http Method, request Uri, request TimeStamp, nonce, request Content Base64 String.
    ///     - Generate the hmac signature and set it in the Authorization header.
    
    class func sendWithHmacAuthHeader(requestMethod: String, requestURI: String, content: String = "") -> String {
        var requestContentBase64String = ""
        let customAllowedSet = CharacterSet(charactersIn: "&:=\"#%/<>?@\\^`{|}").inverted
        let requestURI = requestURI
            .addingPercentEncoding(withAllowedCharacters: customAllowedSet)?
            .lowercased()
        let requestHTTPMethod = requestMethod
        let requestTimeStamp = String(Int(Date().timeIntervalSince1970))
        let nonce = UUID().uuidString
            .replacingOccurrences(of: "-", with: "")
            .lowercased()
        
        if !content.isEmpty {
            requestContentBase64String = content.signHMAC(.sha256, self.clientSecret)
        }
        
        guard let _requestURI = requestURI else { return "" }
        let signatureRawData = [
            clientId,
            requestHTTPMethod,
            _requestURI,
            nonce,
            requestTimeStamp,
            requestContentBase64String
        ]
        .map { $0 }.joined()
        let requestSignatureBase64String = signatureRawData.signHMAC(.sha256, self.clientSecret)
        let headers =
            "amx "
            + self.clientId
            + ":"
            + requestSignatureBase64String
            + ":"
            + nonce
            + ":"
            + requestTimeStamp
        
        return headers
    }
    
}
