
//
//  API.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import Alamofire
import SwiftyJSON
import PKHUD
import CryptoSwift

final class Api {
    
    // MARK: - AF Properties
    
    static let adapterHmac = ApiRequestAdapter(isJwt: false)
    static let adapterJwt = ApiRequestAdapter(isJwt: true)
    static let retrier = ApiRequestRetrier()
    static let interceptorHmac = Interceptor(adapters: [adapterHmac], retriers: [retrier])
    static let interceptorJwt = Interceptor(adapters: [adapterJwt], retriers: [retrier])

    // MARK: - HMAC Tokens
    
    class func getJwtToken(phone: String, code: String, email: String, password: String, completion: @escaping (Bool, Data?) -> Void) {
        AF.request(
            ApiHelper.getHmacUrl("Account/GetJwtToken"),
            method: .post,
            parameters: ["phone": phone, "code": code, "email": email, "password": password],
            encoding: JSONEncoding.prettyPrinted,
            interceptor: self.interceptorHmac
        ).responseJSON { response in
            ApiHelper.processing2(response: response, important: false, completion)
            ApiData.getJwtTokenResponse = response.debugDescription
        }
    }
    
    class func refreshJwtToken(_ completion: @escaping (Bool, Data?) -> Void) {
        AF.request(
            ApiHelper.getHmacUrl("Account/RefreshJwtToken"),
            method: .post,
            parameters: ["refreshToken": UserDefaults.jwtRefreshToken ?? ""],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorHmac
        ).validate().response { response in
            ApiHelper.processing2(response: response, important: false, completion)
            ApiData.refreshJwtTokenResponse = response.debugDescription
        }
    }
    
    class func registrationUser(phone: String, email: String, firstName: String, countryId: Int, code: String, password: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getHmacUrl("Registration/RegisterUser"),
             method: .post,
             parameters: ["phone": phone, "email": email, "firstName": firstName, "countryId": countryId, "code": code, "password": password, "language": Locale.current.languageCode!],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorHmac
         ).validate().response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func registrationCheckPhone(phone: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getHmacUrl("Registration/CheckPhone"),
             method: .post,
             parameters: ["phone": phone],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorHmac
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func registrationCheckEmail(email: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getHmacUrl("Registration/CheckEmail"),
             method: .post,
             parameters: ["email": email],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorHmac
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func accountCheckPhone(phone: String, fcmToken: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getHmacUrl("Account/CheckPhone"),
             method: .post,
             parameters: ["phone": phone, "fcmToken": fcmToken],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorHmac
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func settingsGetCountries(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Settings/GetCountries"),
            method: .get,
            interceptor: self.interceptorHmac
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: HMAC Recover password
    
    class func recoverPasswordByPhone(email: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getHmacUrl("Account/RecoverPasswordByEmail"),
             method: .post,
             parameters: ["email": email],
             encoding: JSONEncoding.prettyPrinted,
             interceptor: self.interceptorHmac
         ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    // MARK: JWT Clients

    class func userGetCurrentUser(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Users"),
            method: .get,
            headers: [.authorization(bearerToken: UserDefaults.jwtAccessToken ?? "")],
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func userUpdateUser(lastName: String, firstName: String, phone: String, email: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Users/UpdateUser"),
            method: .put,
            parameters: ["phone": phone, "email": email, "lastName": lastName, "firstName": firstName],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT Organizations
    
    class func registrationUserToOrganization(phone: String, email: String, lastName: String, firstName: String,
                                              organizationHash: String, position: String, roleId: Int,
                                              _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Registration/RegisterUserToOrganization"),
             method: .post,
             parameters: ["phone": phone, "email": email, "lastName": lastName,
                          "firstName": firstName, "organizationHash": organizationHash,
                          "position": position, "roleId": roleId],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }

    class func addOrganization(brandHash: String, name: String, legalName: String, phone: String,
                               website: String, email: String, address: String, countryId: Int,
                                              _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Organizations"),
             method: .post,
             parameters: ["brandHash": brandHash, "name": name, "legalName": legalName,
                          "phone": phone, "website": website, "email": email, "address": address, "countryId": countryId],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func updateOrganization(hash: String, brandHash: String, name: String, legalName: String, phone: String,
                               website: String, email: String, address: String,
                                              _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Organizations"),
             method: .put,
             parameters: ["hash": hash, "brandHash": brandHash, "name": name, "legalName": legalName, "phone": phone,
                          "website": website, "email": email, "address": address],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func organizationAvailableRoles(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Organizations/AvailableRoles/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func organizationUsersByOrganization(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("OrganizationUsers/ByOrganization/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
 
    class func organizationUpdateUser(userHash: String, organizationHash: String, roleId: Int, position: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("OrganizationUsers"),
            method: .patch,
            parameters: ["userHash": userHash, "organizationHash": organizationHash, "roleId": roleId, "position": position],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorJwt
        ).validate().response { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func organizationGetSettings(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Organizations/Settings/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func organizationPutSettings(organizationHash: String, settings: [[String: Any]], _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Organizations/Settings/" + organizationHash),
             method: .put,
             parameters: ["settings": settings],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    // MARK: JWT Ecommpay
    
    class func ecommpayGetEcommpayConfigJson(language: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Ecommpay/GetEcommpayRequestConfig/" + language),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT Tips
    
    class func userTips(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Tips"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func userTipsByOrganization(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Tips/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func tipsByHashAndOrganizationHash(userHash: String, organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Tips/"  + userHash + "/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT Reviews
    
    class func userReviews(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Reviews"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func userReviewsByOrganization(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Reviews/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func reviewsByHashAndOrganizationHash(userHash: String, organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Reviews/"  + userHash + "/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT Finance

    class func financeBalance(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/Balance"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func financeBalanceByOrganization(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/OrganizationBalance/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
   
    class func financeBankCard(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/BankCards"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func financePayOut(amount: Int, bankCardHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Finance/PayOut"),
             method: .post,
             parameters: ["amount": amount, "bankCardHash": bankCardHash],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    class func financePayouts(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/Payouts"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func financeOrganizationUndistributedTips(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/OrganizationUndistributedTips/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func financeTipDistributions(organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Finance/TipDistributions/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func financeDistributeTips(organizationHash: String, tipHashes: [String], userHashes: [String], amount: Int, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Finance/DistributeTips"),
             method: .post,
             parameters: ["organizationHash": organizationHash, "tipHashes": tipHashes, "userHashes": userHashes, "amount": amount],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    // MARK: JWT Client images
    
    class func clientsSetAvatar(picture: UIImage, _ completion: @escaping (Bool, Data?) -> Void) {
        guard let pictureData = picture.resize(512).jpegData(compressionQuality: 0.5) else { return }
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(
                    pictureData,
                    withName: "Photo",
                    fileName: "image_" + String(arc4random()) + ".jpeg",
                    mimeType: "image/jpeg"
                )
            },
            to: ApiHelper.getJwtUrl("Users/UploadAvatar"),
            method: .post,
            headers: [.authorization(bearerToken: UserDefaults.jwtAccessToken ?? ""),
                      .contentType("multipart/form-data"),
                      .contentDisposition("form-data")]
        ).validate().responseJSON { response in ApiHelper.processing2(response: response, important: false, completion)}
    }
    
    // MARK: JWT Client by Hash
    
    class func usersByHashAndOrganizationHash(userHash: String, organizationHash: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Users/" + userHash + "/" + organizationHash),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT QR Code
    
    class func getQRCode(employeeHash: String, organizationHash: String, tableNo: Int, invoiceTotal: Int, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("QR/GetQRCode"),
             method: .post,
             parameters: ["employeeHash": employeeHash, "organizationHash": organizationHash, "tableNo": tableNo, "invoiceTotal": invoiceTotal],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).response { response in ApiHelper.processing(response: response, important: true, completion)}
     }
    
    // MARK: JWT Brands
    
    class func getBrands(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Brands"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func createBrand(brandName: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Brands"),
            method: .post,
            parameters: ["brandName": brandName],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func updateBrand(brandHash: String, brandName: String, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Brands"),
            method: .put,
            parameters: ["brandHash": brandHash, "brandName": brandName],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    class func brandsUploadLogo(brandHash: String, picture: UIImage, _ completion: @escaping (Bool, Data?) -> Void) {
        guard let pictureData = picture.resize(512).jpegData(compressionQuality: 0.5) else { return }
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(
                    pictureData,
                    withName: "Logo",
                    fileName: "image_" + String(arc4random()) + ".jpeg",
                    mimeType: "image/jpeg"
                )
                multipartFormData.append(Data(brandHash.utf8), withName: "brandHash")
            },
            to: ApiHelper.getJwtUrl("Brands/UploadLogo"),
            method: .post,
            headers: [.authorization(bearerToken: UserDefaults.jwtAccessToken ?? ""),
                      .contentType("multipart/form-data"),
                      .contentDisposition("form-data")]
        ).validate().responseJSON { response in ApiHelper.processing2(response: response, important: false, completion)}
    }
    
    // MARK: JWT Reports
    
    class func getReportsData(charts: [[String : String]], _ completion: @escaping (Bool, Data?, Error?) -> Void) {
         AF.request(
             ApiHelper.getJwtUrl("Reports/Data"),
             method: .post,
             parameters: ["charts": charts],
             encoding: JSONEncoding.default,
             interceptor: self.interceptorJwt
         ).validate().response { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: JWT Notifications
    
    class func getNotifications(_ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Notifications"),
            method: .get,
            interceptor: self.interceptorJwt
        ).responseJSON { response in ApiHelper.processing(response: response, important: true, completion)}
    }

    class func notificationMarkAsRead(notificationId: Int, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Notifications/\(notificationId)/MarkAsRead"),
            method: .patch,
            interceptor: self.interceptorJwt
        ).validate().response { response in ApiHelper.processing(response: response, important: true, completion)}
    }
   
    class func notificationDelete(notificationId: Int, _ completion: @escaping (Bool, Data?, Error?) -> Void) {
        AF.request(
            ApiHelper.getJwtUrl("Notifications/\(notificationId)/DeleteNotification"),
            method: .delete,
            interceptor: self.interceptorJwt
        ).validate().response { response in ApiHelper.processing(response: response, important: true, completion)}
    }
    
    // MARK: FCM Tokens
    
    class func registerFcmToken(fcmToken: String, vendor: String = "Apple", model: String = UIDevice.current.name, appVersion: String = EnvironmentHelper.version,
        os: String = "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)", name: String = UIDevice.current.model,
        completion: @escaping (Bool, Data?) -> Void
    )
    {
        AF.request(
            ApiHelper.getJwtUrl("MobileDevices"),
            method: .post,
            parameters: ["fcmToken": fcmToken, "vendor": vendor, "model": model, "appVersion": appVersion, "os": os, "name": name],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorJwt
        ).validate().response { response in ApiHelper.processing2(response: response, important: true, completion)}
    }
    
    class func removeFcmToken(fcmToken: String = UserDefaults.fcmToken ?? "", completion: @escaping (Bool, Data?) -> Void) {
        AF.request(
            ApiHelper.getHmacUrl("MobileDevices"),
            method: .delete,
            parameters: ["fcmToken": fcmToken],
            encoding: JSONEncoding.default,
            interceptor: self.interceptorHmac
        ).validate().response { response in ApiHelper.processing2(response: response, important: false, completion)}
    }
}

