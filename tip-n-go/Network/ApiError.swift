//
//  PeopleError.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import UIKit

class ApiError: LocalizedError {
    
    private let description: String
    private let message: String?
    let code: Int
    
    init(description: String, code: Int = 0, message: String? = nil) {
        self.description = description
        self.message = message
        self.code = code
    }
    
    var errorDescription: String? {
        return self.description
    }

    var errorMessage: String? {
        return self.message
    }

    func showErrorMessage() {
        let panel = PeopleToastPanel()
        panel.title = errorMessage
        panel.isError = true
        panel.show()
    }
}
