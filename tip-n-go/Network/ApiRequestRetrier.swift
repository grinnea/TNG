//
//  PFRequestRetrier.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import Alamofire

final class ApiRequestRetrier: RequestRetrier {
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard
            request.retryCount <= 1,
            let statusCode = request.response?.statusCode,
            let response = request.response,
            let isRefreshTokensRequest = request.request?.url?.absoluteString.contains("Account/RefreshJwtToken")
        else {
            completion(.doNotRetry)
            return
        }
        
        if !isRefreshTokensRequest && statusCode == 401 {
            self.refreshJwtToken(completion)
        } else if !isRefreshTokensRequest && statusCode != 200 {
            completion(.doNotRetryWithError(error))
            debugPrint(response)
        } else if isRefreshTokensRequest, statusCode != 200 {
            completion(.doNotRetryWithError(error))
            self.presentLoginVC()
        }
    }
    
    private func presentLoginVC() {
        DispatchQueue.main.async {
            let presentingVC = UIApplication.topViewController()
            let loginVC = R.storyboard.main.loginVC()
            loginVC?.modalTransitionStyle = .coverVertical
            loginVC?.modalPresentationStyle = .fullScreen

            if let loginVC = loginVC {
                presentingVC?.present(loginVC, animated: true, completion: nil)
            }
        }
    }
    
    private func refreshJwtToken(_ completion: @escaping (RetryResult) -> Void) {
        Api.refreshJwtToken({ success, data in
            if success, let response = data {
                do {
                    let responseObject = try JSONDecoder().decode(JwtToken.self, from: response)
                    UserDefaults.tokensReceivedTime = Date().timeIntervalSince1970
                    UserDefaults.jwtAccessToken = responseObject.accessToken
                    UserDefaults.jwtAccessTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject.accessTokenLifetime)
                    UserDefaults.jwtRefreshToken = responseObject.refreshToken
                    UserDefaults.jwtRefreshTokenTimeToLife = (UserDefaults.tokensReceivedTime ?? 0.0) + Double(responseObject.refreshTokenLifetime)
                    completion(.retry)
                } catch {
                    completion(.doNotRetry)
                    self.presentLoginVC()
                }
            }
        })
    }
}
