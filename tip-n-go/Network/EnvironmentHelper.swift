//
//  EnvironmentHelper.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

final class EnvironmentHelper {
    
    // MARK: - Properties
    
    public static var mode: EnvironmentMode = .staging
    
    // URL Constituents
    
    private static var prefix: String?
    private static var domain: String?
    private static var path: String?
    
    // HMAC Service signature elements
    
    static var clientId: String {
        return BundleHelper.getBundleValueByKey("CLIENT_ID")
    }
    
    static var clientSecret: String {
        return BundleHelper.getBundleValueByKey("CLIENT_SECRET")
    }
    
    // App version
    
    static var version: String {
        let info = Bundle.main.infoDictionary
        let appVersion = info?["CFBundleShortVersionString"] as? String ?? "0.0.0"
        let appBuild = info?["CFBundleVersion"] as? String ?? "0"
        return "\(appVersion) (\(appBuild))"
    }
    
    // MARK: - Methods
    
    static func getURL(type: EnvironmentPathMode) -> String {
        clearURLConstituents()
        
        getURLConstituents(type: type)
        getDomain()
        
        var url = "https://"
        
        if let prefix = prefix, let domain = domain, let path = path {
            url += prefix + "." + domain + "/" + path
        } else if let prefix = prefix, let domain = domain {
            url += prefix + "." + domain + "/"
        }
        
        return url
    }
    
    private static func getURLConstituents(type: EnvironmentPathMode) {
        switch type {
        case .base:
            prefix = BundleHelper.getBundleValueByKey("PREF_API")
        case .signup:
            prefix = BundleHelper.getBundleValueByKey("PREF_SIGNUP")
            path = BundleHelper.getBundleValueByKey("SIGNUP_PATH")
        }
    }
    
    private static func getDomain() {
        domain = BundleHelper.getBundleValueByKey("DOMAIN")
    }
    
    private static func clearURLConstituents() {
        prefix = ""
        domain = ""
        path = ""
    }
}
