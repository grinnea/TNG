//
//  ApiHelper.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
import Alamofire
import SwiftyJSON
import PKHUD

// MARK: - Helpers

class ApiHelper {
    
    class func getUrl(_ url: String) -> String {
        return EnvironmentHelper.getURL(type: .base) + url
    }

    class func getHmacUrl(_ name: String) -> String {
        return getUrl(name)
    }

    class func getJwtUrl(_ name: String) -> String {
        return getUrl(name)
    }

    class func processing(response: AFDataResponse<Any>, important: Bool, _ completion: @escaping (Bool, Data?, Error?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data, nil)
                } else {
                    completion(true, nil, nil)
                }
            } else if let data = response.data {
                completion(false, data, response.error)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil, response.error)
                }
            }
        }
        debugPrint(response)
    }

    class func processing(response: AFDataResponse<Data>, important: Bool, _ completion: @escaping (Bool, Data?, Error?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data, nil)
                } else {
                    completion(true, nil, nil)
                }
            } else if let data = response.data {
                completion(false, data, response.error)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil, response.error)
                }
            }
        }
        debugPrint(response)
    }

    class func processing(response: AFDataResponse<Data?>, important: Bool, _ completion: @escaping (Bool, Data?, Error?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data, nil)
                } else {
                    completion(true, nil, nil)
                }
            } else if let data = response.data {
                completion(false, data, response.error)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil, response.error)
                }
            }
        }
        debugPrint(response)
    }

    class func processing2(response: AFDataResponse<Any>, important: Bool, _ completion: @escaping (Bool, Data?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data)
                } else {
                    completion(true, nil)
                }
            } else if let data = response.data {
                completion(false, data)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil)
                }
            }
        }
        debugPrint(response)
    }

    class func processing2(response: AFDataResponse<Data>, important: Bool, _ completion: @escaping (Bool, Data?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data)
                } else {
                    completion(true, nil)
                }
            } else if let data = response.data {
                completion(false, data)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil)
                }
            }
        }
        debugPrint(response)
    }

    class func processing2(response: AFDataResponse<Data?>, important: Bool, _ completion: @escaping (Bool, Data?) -> Void)  -> Void {
        if let statusCode = response.response?.statusCode {
            if (200...299).contains(statusCode) {
                if let data = response.data {
                    completion(true, data)
                } else {
                    completion(true, nil)
                }
            } else if let data = response.data {
                completion(false, data)
            } else {
                if important {
                    ApiError(description: "error_api_try_again".localized()).show()
                } else {
                    completion(false, nil)
                }
            }
        }
        debugPrint(response)
    }
}
