//
//  QRCode.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 18.04.2022.
//

import Foundation

struct QRCode: Codable {
    var data: String?
}
