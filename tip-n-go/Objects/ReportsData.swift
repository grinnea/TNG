//
//  ReportsData.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 25.04.2022.
//

import Foundation

typealias ReportsData = [ReportData]

struct ReportData: Decodable {
    var title: Title?
    var type: String?
    var yAxis: yAxis?
    var xAxis: xAxis?
    var series: Series?
}

struct Title: Decodable {
    var text: String?
}

struct yAxis: Decodable {
    var title: Title?
}

struct xAxis: Decodable {
    var categories: [String]?
}

typealias Series = [Serie]

struct Serie: Decodable {
    var name: String?
    var dataPie: [Pie]?
    var type: String?
    var color: String?
    var dataLine: [Double]?
    
    private enum CodingKeys : String, CodingKey { case name, data, type, color }
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
        if type == "column" {
            self.color = try container.decode(String.self, forKey: .color)
            self.name = try container.decode(String.self, forKey: .name)
            self.dataLine = try container.decode([Double].self, forKey: .data)
        } else {
            self.name = try container.decode(String.self, forKey: .name)
            self.dataPie = try container.decode([Pie].self, forKey: .data)
        }
    }
}

enum DataType {
    case line([Double]), pie([Pie])
}

struct Pie : Decodable {
    var name: String?
    var y: Double?
    var color: String?
}
