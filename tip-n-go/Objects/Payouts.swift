//
//  Payouts.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 12.04.2022.
//

import Foundation

typealias Payouts = [Payout]

struct Payout: Decodable {
    var date: String?
    var amount: Int?
    var currency: Currency?
    var userBankCard: UserBankCard?
}
