//
//  UserOrganizations.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.03.2022.
//

import Foundation

typealias UserOrganizations = [UserOrganization]

struct UserOrganization: Decodable {
    var organizationRole: Role?
    var organization: Organization?
    var position: String?
}
