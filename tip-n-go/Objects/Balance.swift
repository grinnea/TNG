//
//  Balance.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.04.2022.
//

import Foundation

struct Balance: Decodable {
    var balance: Int?
    var currency: Currency?
}
