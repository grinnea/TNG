//
//  Currency.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 31.03.2022.
//

import Foundation

struct Currency: Codable {
    var id: Int?
    var name: String?
    var code: String?
    var sign: String?
}
