//
//  NotificationMessages.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.05.2022.
//

import Foundation

typealias NotificationMessages = [NotificationMessage]

struct NotificationMessage: Codable {
    var id: Int?
    var date: String?
    var text: String?
    var isRead: Bool?
}
