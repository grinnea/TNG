//
//  Organization.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.04.2022.
//

import Foundation

struct Organization: Decodable {
    var brand: Brand?
    var name: String?
    var legalName: String?
    var phone: String?
    var website: String?
    var email: String?
    var address: String?
    var hash: String?
    var tipDistributionMethodId: Int?
    var rating: Int?
    var country: Country?
    var currency: Currency?
}
