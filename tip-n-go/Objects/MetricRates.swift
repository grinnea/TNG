//
//  MetricRates.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 13.04.2022.
//

import Foundation

typealias MetricRates = [MetricRate]

struct MetricRate: Codable {
    var name: String?
    var rate: Int?
}
