//
//  Reviews.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 13.04.2022.
//

import Foundation

typealias Reviews = [Review]

struct Review: Decodable {
    var date: String?
    var comments: String?
    var rate: Int?
    var metricRates: MetricRates?
    var user: User?
    var organization: Organization?
    var tip: TipShort?
}
