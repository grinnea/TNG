//
//  TipShort.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 14.04.2022.
//

import Foundation

struct TipShort: Decodable {
    var date: String?
    var tipRate: Int?
    var amount: Int?
    var currency: Currency?
}
