//
//  SceneDelegate.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 10.03.2022.
//

import Foundation

struct UserCurrent: Decodable {
    var hash: String?
    var lastName: String?
    var firstName: String?
    var phone: String?
    var email: String?
    var language: String?
    var avatar: String?
    var rating: Int?
    var userRoles: Roles?
    var userOrganizations: UserOrganizations?
    var currency: Currency?
}
