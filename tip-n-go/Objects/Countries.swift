//
//  Countries.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 31.03.2022.
//

import Foundation

typealias Countries = [Country]

struct Country: Decodable {
    var id: Int?
    var name: String?
    var code: String?
    var phoneCountryCode: Int?
    var currency: Currency?
}
