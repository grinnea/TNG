//
//  OrganizationSettings.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 28.04.2022.
//

import Foundation

typealias OrganizationSettings = [OrganizationSetting]

struct OrganizationSetting: Decodable {
    var key: String?
    var name: String?
    var description: String?
    var options: [Option]?
}

struct Option: Decodable {
    var id: Int?
    var name: String?
    var description: String?
    var checked: Bool?
}
