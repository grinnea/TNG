//
//  TipDistributions.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.04.2022.
//

import Foundation

typealias TipDistributions = [TipDistribution]

struct TipDistribution: Decodable {
    var date: String?
    var user: User?
    var amount: Int?
    var currency: Currency?
    var done: Bool?
    var sources: Tips?
    var destinations: [Destination]?
}

struct Destination: Decodable {
    var amount: Int?
    var currency: Currency?
    var user: UserTip?
}

struct UserTip: Decodable {
    var hash: String?
    var lastName: String?
    var firstName: String?
    var phone: String?
    var email: String?
    var emailConfirmed: Bool?
    var language: String?
    var avatar: String?
    var rating: Int?
    var position: String?
}
