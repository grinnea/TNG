//
//  EnvironmentType.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

enum EnvironmentMode: String  {
    case staging
    case production
}
