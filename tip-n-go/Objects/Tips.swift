//
//  Tips.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.04.2022.
//

import Foundation

typealias Tips = [Tip]

struct Tip: Decodable {
    var hash: String?
    var date: String?
    var tipRate: Int?
    var amount: Int?
    var user: User?
    var currency: Currency?
    var organization: Organization?
}
