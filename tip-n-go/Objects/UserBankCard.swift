//
//  UserBankCard.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.04.2022.
//

import Foundation

typealias UserBankCards = [UserBankCard]

struct UserBankCard: Codable {
    var hash: String?
    var cardType: String?
    var expMonth: Int?
    var expYear: Int?
    var cardNumberMasked: String?
    var cardHolderName: String?
}
