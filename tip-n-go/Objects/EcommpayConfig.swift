//
//  EcommpayConfig.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 24.03.2022.
//

import Foundation

struct EcommpayConfig: Codable {
    var project_id: Int?
    var payment_id: String?
    var payment_amount: Int?
    var payment_currency: String?
    var customer_id: String?
    var payment_description: String?
    var region_code: String?
    var signature: String?
}
