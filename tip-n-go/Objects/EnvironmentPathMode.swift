//
//  EnvironmentPathType.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

enum EnvironmentPathMode {
    case base, signup
}
