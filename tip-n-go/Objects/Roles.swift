//
//  Roles.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.04.2022.
//

import Foundation

typealias Roles = [Role]

struct Role: Decodable {
    var id: Int?
    var name: String?
}
