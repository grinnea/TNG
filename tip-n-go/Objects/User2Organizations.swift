//
//  User2Organizations.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 22.03.2022.
//

import Foundation

typealias User2Organizations = [User2Organization]

struct User2Organization: Decodable {
    var hash: String?
    var lastName: String?
    var firstName: String?
    var phone: String?
    var email: String?
    var language: String?
    var avatar: String?
    var rating: Int?
    var organizationRole: Role?
    var position: String?
}
