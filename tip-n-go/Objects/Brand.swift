//
//  Brand.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 05.04.2022.
//

import Foundation

typealias Brands = [Brand]

struct Brand: Codable {
    var hash: String?
    var name: String?
    var logo: String?
}
