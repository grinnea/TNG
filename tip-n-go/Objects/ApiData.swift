//
//  ApiData.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

public struct ApiData: Codable {
    static var hmacEndpoints: String?
    static var getJwtTokenResponse: String?
    static var hmacErrorResponse: String?
    static var localErrorCaughtOnHmacRouteRequest: String?
    static var hasJwtToken: String?
    static var JwtToken: String?
    static var hasRefreshToken: String?
    static var refreshToken: String?
    static var jwtTokenAlive: String?
    static var refreshTokenAlive: String?
    static var refreshJwtTokenResponse: String?
    static var allJwttokensNotAlive: String?
    static var needsLogin: String?
    static var getLegalDocsResponse: String?
}
