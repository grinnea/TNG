//
//  User.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 08.04.2022.
//

import Foundation

typealias Users = [User]

struct User: Decodable {
    var hash: String?
    var lastName: String?
    var firstName: String?
    var phone: String?
    var email: String?
    var language: String?
    var avatar: String?
    var rating: Int?
}
