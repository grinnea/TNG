//
//  GetJwtTokenResponse.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation

struct JwtToken: Decodable {
    var accessToken: String
    var accessTokenLifetime: Int
    var refreshToken: String
    var refreshTokenLifetime: Int
}
