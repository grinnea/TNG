//
//  ErrorResponse.swift
//  tip-n-go
//
//  Created by Григорий Ищенко on 11.03.2022.
//

import Foundation
    
struct ServerError: Codable {
    var error: String?
    var text: String?
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case text = "text"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let error = try values.decodeIfPresent(String.self, forKey: .error) {
            self.error = error
        }
        if let text = try values.decodeIfPresent(String.self, forKey: .text) {
            self.text = text
        } else if let message = try values.decodeIfPresent(String.self, forKey: .message) {
            self.text = message
        }
    }
}
